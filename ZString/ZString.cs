#region Usings

using System;
using System.Collections.Generic;
using System.Diagnostics;

#endregion

namespace ZFormat
{
    /// <summary>
    /// Mutable, growable string with API that is loosely based on <see cref="System.Text.StringBuilder"/>.
    /// </summary>
    public sealed class ZString
    {
        private int _capacity;
        private string _str;

        /// <summary>
        /// Initializes a new instance of the ZString class using the specified optional capacity.
        /// </summary>
        /// <param name="maxCapacity">The suggested starting size of this instance.</param>
        public ZString(int maxCapacity = 16)
        {
            _capacity = maxCapacity;
            _str = new string('\0', maxCapacity);
            Clear();
        }

        /// <summary>
        /// Gets or sets the maximum number of characters that can be contained 
        /// in the memory allocated by the current instance.
        /// Capacity will be set to the next power of 2 of the value.
        /// </summary>
        public int Capacity
        {
            get { return _capacity; }
            set
            {
                if (value < 0) { return; }
                EnsureCapacity(value);
            }
        }

        /// <summary>
        /// Gets or sets the length of the current ZString object.
        /// </summary>
        public int Length
        {
            get { return _str.Length; }
            set
            {
                if (value < 0) { return; }
                EnsureCapacity(value);
                SetLength(value);
            }
        }

        /// <summary>
        /// Gets or sets the character at the specified character position in this instance.
        /// </summary>
        /// <exception cref="IndexOutOfRangeException">
        /// index is outside the bounds (length) of this instance while setting a character.</exception>
        public unsafe char this[int index]
        {
            get { return _str[index]; }
            set
            {
                if (index >= _str.Length)
                {
                    throw new IndexOutOfRangeException();
                }
                fixed (char* src = _str)
                {
                    src[index] = value;
                }
            }
        }

        /// <summary>
        /// Appends the string representation of a specified <see cref="Char"/> object to this instance.
        /// </summary>
        /// <returns>this</returns>
        public unsafe ZString Append(char ch)
        {
            EnsureCapacity(Length + 1);
            fixed (char* src = _str)
            {
                var pi = (int*) src;
                var length = pi[-1];
                src[length++] = ch;
                src[length] = '\0';
                pi[-1] = length;
            }
            return this;
        }

        /// <summary>
        /// Appends array of fragments to this instance that are one of the: 
        /// string, char[], List&lt;Char&gt; or ZString; ignore all other objects.
        /// </summary>
        /// <returns>this</returns>
        public ZString Append(object[] fragments)
        {
            if (fragments == null)
            {
                return this;
            }
            var count = LengthOfFrags(fragments);
            EnsureCapacity(Length + count);
            for (var i = 0; i < fragments.Length; i++)
            {
                var frag = fragments[i];
                var s = frag as string;
                if (s != null)
                {
                    Append(s);
                    continue;
                }
                var chars = frag as char[];
                if (chars != null)
                {
                    Append(chars);
                    continue;
                }
                var zstr = frag as ZString;
                if (zstr != null)
                {
                    Append(zstr.ToString());
                    continue;
                }
                var lst = frag as List<char>;
                if (lst != null)
                {
                    Append(lst);
                }
            }
            return this;
        }

        /// <summary>
        /// Appends a copy of the specified string to this instance.
        /// </summary>
        /// <returns>this</returns>
        public unsafe ZString Append(string s)
        {
            EnsureCapacity(Length + s.Length);
            fixed (char* src = s)
            {
                return Append(src, s.Length);
            }
        }

        /// <summary>
        /// Appends a copy of the specified ZString to this instance.
        /// </summary>
        /// <returns>this</returns>
        public ZString Append(ZString other)
        {
            return Append(other.ToString());
        }

        /// <summary>
        /// Appends the string representation of the Unicode characters in a specified list to this instance.
        /// </summary>
        /// <returns>this</returns>
        public unsafe ZString Append(List<char> chars)
        {
            if (chars == null || chars.Count == 0)
            {
                return this;
            }
            var count = chars.Count;
            EnsureCapacity(Length + count);
            fixed (char* dest = _str)
            {
                var pi = (int*) dest;
                var len = pi[-1];
                var i = 0;
                while (i < count)
                {
                    dest[len + i] = chars[i];
                    ++i;
                }
                dest[len + count] = '\0';
                pi[-1] = len + count;
            }
            return this;
        }

        /// <summary>
        /// Appends the string representation of the Unicode characters in a specified array to this instance.
        /// </summary>
        /// <returns>this</returns>
        public ZString Append(char[] chars)
        {
            if (chars == null || chars.Length == 0)
            {
                return this;
            }
            return Append(chars, chars.Length);
        }

        /// <summary>
        /// Appends the string representation of a specified array of Unicode characters to this instance.
        /// </summary>
        /// <returns>this</returns>
        /// <exception cref="ArgumentNullException">'chars' is null</exception>
        /// <exception cref="ArgumentException">
        /// 'count' is less then 0 or 'count' is greater than the length of 'chars' </exception>
        public unsafe ZString Append(char[] chars, int count)
        {
            if (chars == null) { throw new ArgumentNullException("chars"); }
            if (count < 0 || count > chars.Length) { throw new ArgumentException("count"); }
            EnsureCapacity(Length + count);
            fixed (char* src = chars)
            {
                return Append(src, count);
            }
        }

        /// <summary>
        /// Alias for 'Length = 0'
        /// </summary>
        public void Clear()
        {
            SetLength(0);
        }

        /// <summary>
        /// Calculate sum of lengths of all fragments.
        /// </summary>
        public static int LengthOfFrags(object[] fragments)
        {
            if (fragments == null)
            {
                return 0;
            }
            var length = 0;
            for (var i = 0; i < fragments.Length; i++)
            {
                var frag = fragments[i];
                var s = frag as string;
                if (s != null)
                {
                    length += s.Length;
                    continue;
                }
                var chars = frag as char[];
                if (chars != null)
                {
                    length += chars.Length;
                    continue;
                }
                var zstr = frag as ZString;
                if (zstr != null)
                {
                    length += zstr.Length;
                    continue;
                }
                var lst = frag as List<char>;
                if (lst != null)
                {
                    length += lst.Count;
                }
            }
            return length;
        }

        /// <summary>
        /// Clears and then appends a copy of the specified string to this instance
        /// </summary>
        /// <returns>this</returns>
        public ZString Reset(string str)
        {
            Clear();
            return Append(str);
        }

        /// <summary>
        /// Clears and then appends array of fragments to this instance that are one of the: 
        /// string, char[], List&lt;Char&gt; or ZString; ignore all other objects.</summary>
        /// <returns>this</returns>
        public ZString Reset(object[] frags)
        {
            Clear();
            return Append(frags);
        }

        /// <summary>
        /// Clears and then appends the string representation of a specified array of Unicode characters to this instance.
        /// </summary>
        /// <returns>this</returns>
        /// <exception cref="ArgumentNullException">'chars' is null</exception>
        /// <exception cref="ArgumentException">
        /// 'count'  is less then 0 or 'count' is greater than the length of 'chars'
        /// </exception>
        public unsafe ZString Reset(char[] chars, int count)
        {
            if (chars == null) { throw new ArgumentNullException("chars"); }
            if (count < 0 || count > chars.Length) { throw new ArgumentException("count"); }
            EnsureCapacity(count);
            Clear();
            fixed (char* src = chars)
            {
                return Append(src, count);
            }
        }

        /// <summary>
        /// NOTE: method do not allocate new string but returns reference to the internal string buffer.
        /// </summary>
        public override string ToString()
        {
            return _str;
        }

        internal unsafe ZString Append(char* src, int count)
        {
            if (src == null) { throw new ArgumentNullException("src"); }
            if (count < 0) { throw new ArgumentException("count"); }
            EnsureCapacity(Length + count);
            fixed (char* dest = _str)
            {
                var pi = (int*) dest;
                var len = pi[-1];
                char* d = &dest[len], s = src;
                var i = count;
                while (i -- > 0)
                {
                    *d++ = *s++;
                }
                *d = '\0';
                pi[-1] = len + count;
            }
            return this;
        }

        internal void EnsureCapacity(int capacity)
        {
            if (_capacity >= capacity)
            {
                return;
            }
            var newsize = capacity;
            // get next power of 2
            newsize--;
            newsize |= newsize >> 1;
            newsize |= newsize >> 2;
            newsize |= newsize >> 4;
            newsize |= newsize >> 8;
            newsize |= newsize >> 16;
            newsize++;
            var @new = new string('\0', newsize);
            StrCopy(_str, @new);
            _capacity = newsize;
            _str = @new;
        }

        internal static unsafe void StrCopy(string src, string dst)
        {
            Debug.Assert(src.Length <= dst.Length);
            var count = src.Length;
            fixed (char* sp = src, dp = dst)
            {
                var s = sp;
                var d = dp;
                while (count -- > 0)
                {
                    *d++ = *s++;
                }
                *d = '\0';
                // set new length:
                var di = (int*) dp;
                di[-1] = (int) (d - dp);
            }
        }

        private unsafe void SetLength(int length)
        {
            Debug.Assert(length <= _capacity);
            fixed (char* p = _str)
            {
                var pi = (int*) p;
                pi[-1] = length;
                p[length] = '\0';
            }
        }
    }
}
