#region Usings

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;

#endregion

namespace ZFormat
{
    /// <summary>
    ///     Provides culture-specific information for formatting numeric values.
    ///     Simplified, non-allocating version of <see cref="NumberFormatInfo" />
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public sealed class ZNumberFormatInfo : IFormatProvider
    {
        private int currencyDecimalDigits = 2;
        private string currencyDecimalSeparator = ".";
        private string currencyGroupSeparator = ",";
        private int[] currencyGroupSizes = { 3 };
        private int currencyNegativePattern = 0;
        private int currencyPositivePattern = 0;
        private string currencySymbol = "$";
        private string nanSymbol = "NaN";
        private string negativeInfinitySymbol = "-Infinity";
        private string positiveInfinitySymbol = "Infinity";
        private string negativeSign = "-";
        private string positiveSign = "+";
        private int numberDecimalDigits = 2;
        private string numberDecimalSeparator = ".";
        private string numberGroupSeparator = ",";
        private int[] numberGroupSizes = { 3 };
        private int numberNegativePattern = 1;
        private int percentDecimalDigits = 2;
        private string percentDecimalSeparator = ".";
        private string percentGroupSeparator = ",";
        private int[] percentGroupSizes = { 3 };
        private int percentNegativePattern = 0;
        private int percentPositivePattern = 0;
        private string percentSymbol = "%";
        private string perMilleSymbol = "\u2030";

        /// <summary>
        ///     Initializes a new writable instance of the ZNumberFormatInfo class that is culture-independent (invariant)
        /// </summary>
        public ZNumberFormatInfo()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the ZNumberFormatInfo class based on the culture specified by name.
        /// </summary>
        public ZNumberFormatInfo(string culture)
            : this(new CultureInfo(culture).NumberFormat)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the ZNumberFormatInfo class based on <see cref="NumberFormatInfo" />
        /// </summary>
        public ZNumberFormatInfo(NumberFormatInfo nfi)
        {
            if (nfi == null)
            {
                return;
            }

            currencyDecimalDigits    = nfi.CurrencyDecimalDigits;
            currencyDecimalSeparator = nfi.CurrencyDecimalSeparator;
            currencyGroupSeparator   = nfi.CurrencyGroupSeparator;
            currencyGroupSizes       = nfi.CurrencyGroupSizes;
            currencyNegativePattern  = nfi.CurrencyNegativePattern;
            currencyPositivePattern  = nfi.CurrencyPositivePattern;
            currencySymbol           = nfi.CurrencySymbol;
            nanSymbol                = nfi.NaNSymbol;
            negativeInfinitySymbol   = nfi.NegativeInfinitySymbol;
            negativeSign             = nfi.NegativeSign;
            numberDecimalDigits      = nfi.NumberDecimalDigits;
            numberDecimalSeparator   = nfi.NumberDecimalSeparator;
            numberGroupSeparator     = nfi.NumberGroupSeparator;
            numberGroupSizes         = nfi.NumberGroupSizes;
            numberNegativePattern    = nfi.NumberNegativePattern;
            perMilleSymbol           = nfi.PerMilleSymbol;
            percentDecimalDigits     = nfi.PercentDecimalDigits;
            percentDecimalSeparator  = nfi.PercentDecimalSeparator;
            percentGroupSeparator    = nfi.PercentGroupSeparator;
            percentGroupSizes        = nfi.PercentGroupSizes;
            percentNegativePattern   = nfi.PercentNegativePattern;
            percentPositivePattern   = nfi.PercentPositivePattern;
            percentSymbol            = nfi.PercentSymbol;
            positiveInfinitySymbol   = nfi.PositiveInfinitySymbol;
            positiveSign             = nfi.PositiveSign;
        }

        /// <summary>
        ///     Gets or sets the number of decimal places to use in currency values.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The property is being set to a value that is less than 0 or greater than 99
        /// </exception>
        public int CurrencyDecimalDigits
        {
            get { return currencyDecimalDigits; }
            set
            {
                if (value < 0 || value > 99)
                {
                    throw new ArgumentOutOfRangeException("Argument out of range [0, 99]");
                }
                currencyDecimalDigits = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string to use as the decimal separator in currency values.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null or an empty string.</exception>
        public string CurrencyDecimalSeparator
        {
            get { return currencyDecimalSeparator; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException("CurrencyDecimalSeparator");
                }
                currencyDecimalSeparator = value;
            }
        }

        /// <summary>
        ///     Gets or sets the number of digits in each group to the left of the decimal in currency values.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        /// <exception cref="ArgumentException">
        ///     The property is being set and the array contains an entry that is less than 0 or greater than 9.
        /// </exception>
        public int[] CurrencyGroupSizes
        {
            get { return currencyGroupSizes; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("CurrencyGroupSizes");
                }
                CheckGroupSize(value);
                currencyGroupSizes = value;
            }
        }

        /// <summary>
        ///     Gets or sets the number of digits in each group to the left of the decimal in numeric values.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        /// <exception cref="ArgumentException">
        ///     The property is being set and the array contains an entry that is less than 0 or greater than 9.
        /// </exception>
        public int[] NumberGroupSizes
        {
            get { return numberGroupSizes; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("NumberGroupSizes");
                }
                CheckGroupSize(value);
                numberGroupSizes = value;
            }
        }

        /// <summary>
        ///     Gets or sets the number of digits in each group to the left of the decimal in percent values.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        /// <exception cref="ArgumentException">
        ///     The property is being set and the array contains an entry that is less than 0 or greater than 9.
        /// </exception>
        public int[] PercentGroupSizes
        {
            get { return percentGroupSizes; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("PercentGroupSizes");
                }
                CheckGroupSize(value);
                percentGroupSizes = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string that separates groups of digits to the left of the decimal in currency values.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        public string CurrencyGroupSeparator
        {
            get { return currencyGroupSeparator; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("CurrencyGroupSeparator");
                }
                currencyGroupSeparator = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string to use as the currency symbol.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        public string CurrencySymbol
        {
            get { return currencySymbol; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("CurrencySymbol");
                }
                currencySymbol = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string that represents the IEEE NaN (not a number) value.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        public string NaNSymbol
        {
            get { return nanSymbol; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("NaNSymbol");
                }
                nanSymbol = value;
            }
        }

        /// <summary>
        ///     Gets or sets the format pattern for negative currency values.
        ///     00: ($n)
        ///     01: -$n
        ///     02: $-n
        ///     03: $n-
        ///     04: (n$)
        ///     05: -n$
        ///     06: n-$
        ///     07: n$-
        ///     08: -n $
        ///     09: -$ n
        ///     10: n $-
        ///     11: $ n-
        ///     12: $ -n
        ///     13: n- $
        ///     14: ($ n)
        ///     15: (n $)
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The property is being set to a value that is less than 0 or greater than 15.
        /// </exception>
        public int CurrencyNegativePattern
        {
            get { return currencyNegativePattern; }
            set
            {
                if (value < 0 || value > 15)
                {
                    throw new ArgumentOutOfRangeException("Argument out of range [0, 15]");
                }
                currencyNegativePattern = value;
            }
        }

        /// <summary>
        ///     Gets or sets the format pattern for negative numeric values.
        ///     0: (n)
        ///     1: -n
        ///     2: - n
        ///     3: n-
        ///     4: n -
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The property is being set to a value that is less than 0 or greater than 4.
        /// </exception>
        public int NumberNegativePattern
        {
            get { return numberNegativePattern; }
            set
            {
                if (value < 0 || value > 4)
                {
                    throw new ArgumentOutOfRangeException("Argument out of range [0, 4]");
                }
                numberNegativePattern = value;
            }
        }

        /// <summary>
        ///     Gets or sets the format pattern for positive percent values.
        ///     0: n %
        ///     1: n%
        ///     2: %n
        ///     3: % n
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The property is being set to a value that is less than 0 or greater than 3.
        /// </exception>
        public int PercentPositivePattern
        {
            get { return percentPositivePattern; }
            set
            {
                if (value < 0 || value > 3)
                {
                    throw new ArgumentOutOfRangeException("Argument out of range [0, 3]");
                }
                percentPositivePattern = value;
            }
        }

        /// <summary>
        ///     Gets or sets the format pattern for negative percent values.
        ///     0: -n %
        ///     1: -n%
        ///     2: -%n
        ///     3: %-n
        ///     4: %n-
        ///     5: n-%
        ///     6: n%-
        ///     7: -% n
        ///     8: n %-
        ///     9: % n-
        ///     10: % -n
        ///     11: n- %
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The property is being set to a value that is less than 0 or greater than 11.
        /// </exception>
        public int PercentNegativePattern
        {
            get { return percentNegativePattern; }
            set
            {
                if (value < 0 || value > 11)
                {
                    throw new ArgumentOutOfRangeException("Argument out of range [0, 11]");
                }
                percentNegativePattern = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string that represents negative infinity.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        public string NegativeInfinitySymbol
        {
            get { return negativeInfinitySymbol; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("NegativeInfinitySymbol");
                }
                negativeInfinitySymbol = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string that denotes that the associated number is negative.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        public string NegativeSign
        {
            get { return negativeSign; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("NegativeSign");
                }
                negativeSign = value;
            }
        }

        /// <summary>
        ///     Gets or sets the number of decimal places to use in numeric values.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The property is being set to a value that is less than 0 or greater than 99.
        /// </exception>
        public int NumberDecimalDigits
        {
            get { return numberDecimalDigits; }
            set
            {
                if (value < 0 || value > 99)
                {
                    throw new ArgumentOutOfRangeException("Argument out of range [0, 99]");
                }
                numberDecimalDigits = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string to use as the decimal separator in percent values.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        public string NumberDecimalSeparator
        {
            get { return numberDecimalSeparator; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("NumberDecimalSeparator");
                }
                numberDecimalSeparator = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string that separates groups of digits to the left of the decimal in numeric values.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        public string NumberGroupSeparator
        {
            get { return numberGroupSeparator; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("NumberGroupSeparator");
                }
                numberGroupSeparator = value;
            }
        }

        /// <summary>
        ///     Gets or sets the format pattern for positive currency values.
        ///     0: $n
        ///     1: n$
        ///     2: $ n
        ///     3: n $
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The property is being set to a value that is less than 0 or greater than 3.
        /// </exception>
        public int CurrencyPositivePattern
        {
            get { return currencyPositivePattern; }
            set
            {
                if (value < 0 || value > 3)
                {
                    throw new ArgumentOutOfRangeException("Argument out of range [0, 3]");
                }
                currencyPositivePattern = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string that represents positive infinity.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        public string PositiveInfinitySymbol
        {
            get { return positiveInfinitySymbol; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("PositiveInfinitySymbol");
                }
                positiveInfinitySymbol = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string that denotes that the associated number is positive.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        public string PositiveSign
        {
            get { return positiveSign; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("PositiveSign");
                }
                positiveSign = value;
            }
        }

        /// <summary>
        ///     Gets or sets the number of decimal places to use in percent values.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The property is being set to a value that is less than 0 or greater than 99.
        /// </exception>
        public int PercentDecimalDigits
        {
            get { return percentDecimalDigits; }
            set
            {
                if (value < 0 || value > 99)
                {
                    throw new ArgumentOutOfRangeException("Argument out of range [0, 99]");
                }
                percentDecimalDigits = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string to use as the decimal separator in percent values.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        public string PercentDecimalSeparator
        {
            get { return percentDecimalSeparator; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("PercentDecimalSeparator");
                }
                percentDecimalSeparator = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string that separates groups of digits to the left of the decimal in percent values.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        public string PercentGroupSeparator
        {
            get { return percentGroupSeparator; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("PercentDecimalSeparator");
                }
                percentGroupSeparator = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string to use as the percent symbol.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        public string PercentSymbol
        {
            get { return percentSymbol; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("PercentSymbol");
                }
                percentSymbol = value;
            }
        }

        /// <summary>
        ///     Gets or sets the string to use as the per mille symbol.
        /// </summary>
        /// <exception cref="ArgumentNullException">The property is being set to null.</exception>
        public string PerMilleSymbol
        {
            get { return perMilleSymbol; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("PerMilleSymbol");
                }
                perMilleSymbol = value;
            }
        }

        #region IFormatProvider Members

        /// <summary>
        ///     Returns object that provides formatting services for the specified type.
        /// </summary>
        public object GetFormat(Type formatType)
        {
            return formatType == typeof (ZNumberFormatInfo) ? this : null;
        }

        #endregion

        /// <summary>
        ///     Returns a new writable instance of the ZNumberFormatInfo class that is culture-independent (invariant)
        /// </summary>
        public static ZNumberFormatInfo NewInvariantInfo()
        {
            return new ZNumberFormatInfo();
        }

        private static void CheckGroupSize(int[] groupSize)
        {
            for (var i = 0; i < groupSize.Length; i++)
            {
                if (groupSize[i] < 1)
                {
                    if (i == groupSize.Length - 1 && groupSize[i] == 0)
                    {
                        return;
                    }
                    throw new ArgumentException("Invalid group size");
                }
                else if (groupSize[i] > 9)
                {
                    throw new ArgumentException("Invalid group size");
                }
            }
        }
    }
}
