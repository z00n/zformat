#region Usings

using System;
using JetBrains.Annotations;

#endregion

namespace ZFormat
{
    internal unsafe struct StackBuffer
    {
        internal const int STACK_BUFFER_CHARS = 512;
        private readonly char* _chars;
        private char* _end;
        private char* _begin;
        private readonly char* _limit;

        internal int PrependCapacity
        {
            get { return (int) (_begin - _chars); }
        }

        internal int AppendCapacity
        {
            get { return (int) (_limit - _end); }
        }

        internal char* Begin
        {
            get { return _begin; }
        }

        internal char* End
        {
            get { return _end; }
        }

        internal void TrimLeft(int count)
        {
            if (_begin + count > _end)
            {
                throw new ArgumentOutOfRangeException("count");
            }
            _begin += count;
        }

        internal void TrimRight(int count)
        {
            Length -= count;
        }

        internal int Length
        {
            get { return (int) (_end - _begin); }
            set
            {
                if (value < 0 || _begin + value > _limit)
                {
                    throw new ArgumentOutOfRangeException("value");
                }
                _end += value - Length;
            }
        }

        internal StackBuffer(char* buffer, int capacity) : this(buffer, capacity, -1)
        {
        }

        internal StackBuffer(char* buffer, int capacity, int prependReserve)
        {
            var ppreserve = prependReserve < 0 ? (capacity >> 2) : prependReserve;
            _chars = buffer;
            _limit = &_chars[capacity];
            _begin = _chars + ppreserve; // can prepend some chars later
            _end = _begin;
        }

        internal char this[int idx]
        {
            get
            {
                var p = (_begin + idx);
                CheckRange(p);
                return *p;
            }
            set
            {
                var p = (_begin + idx);
                CheckRange(p);
                *p = value;
            }
        }

        [UsedImplicitly]
        private void CheckRange(char* p)
        {
            if (p < _chars || p >= _limit)
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        internal void Append(ref StackBuffer other)
        {
            AppendChars(other.Begin, other.Length);
        }

        internal void Append(string s)
        {
            fixed (char* cs = s)
            {
                AppendChars(cs, s.Length);
            }
        }

        internal void Prepend(string s)
        {
            fixed (char* cs = s)
            {
                Prepend(cs, s.Length);
            }
        }

        private void AppendChars(char* cs, int length)
        {
            if (_end + length > _limit)
            {
                throw new Exception("Append reserve depleted");
            }
            for (var i = 0; i < length; i++)
            {
                *_end++ = cs[i];
            }
        }

        private void Prepend(char* cs, int length)
        {
            if (_begin - length < _chars)
            {
                throw new Exception("Prepend reserve depleted");
            }
            for (var i = length - 1; i >= 0; i--)
            {
                *--_begin = cs[i];
            }
        }

        internal void Append(char[] cs)
        {
            fixed (char* csp = cs)
            {
                AppendChars(csp, cs.Length);
            }
        }

        internal void Append(char c, int count)
        {
            if (_end + count > _limit)
            {
                throw new Exception("Append reserve depleted");
            }
            for (var i = 0; i < count; i++)
            {
                *_end++ = c;
            }
        }

        internal void Append(char c)
        {
            // Debug.Assert(_end + 1 < _chars + _size, "Append reserve depleted");
            if (_end + 1 > _limit)
            {
                throw new Exception("Append reserve depleted");
            }
            *_end++ = c;
        }

        internal void Prepend(char c)
        {
            // Debug.Assert(_begin - 1 >= _chars, "Prepend reserve depleted");
            if (_begin - 1 < _chars)
            {
                throw new Exception("Prepend reserve depleted");
            }
            *--_begin = c;
        }

        internal void Prepend(char c, int count)
        {
            if (_begin - count < _chars)
            {
                throw new Exception("Prepend reserve depleted");
            }
            for (var i = 0; i < count; i++)
            {
                *--_begin = c;
            }
        }

        internal string DebugDump()
        {
            var begin = _begin - _chars;
            return new string(_chars, (int) begin, Length);
        }
    }
}
