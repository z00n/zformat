//
// System.NumberFormatter.cs
//
// Author:
//   Kazuki Oikawa (kazuki@panicode.com)
//   Eyal Alaluf (eyala@mainsoft.com)
//   Andrew Zhilin (andrew_zhilin@yahoo.com)
//
// Copyright (C) 2004 Novell, Inc (http://www.novell.com)
// Copyright (C) 2008 Mainsoft Co. (http://www.mainsoft.com)
// Copyright (C) 2015 Andrew Zhilin (andrew_zhilin@yahoo.com)
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#region Usings

using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Threading;
using JetBrains.Annotations;

#endregion

namespace ZFormat
{
    /// <summary>
    ///     The ZNumberFormatter class implements methods for formatting numeric values
    ///     and returns result of formatting in the two instance properties:
    ///     <see cref="Chars" /> and <see cref="Count" />.
    ///     To format numeric values, applications should use the 'NumberToChars' methods.
    /// </summary>
    /// <remarks>
    ///     Class supports all standard and user-defined format strings:
    ///     <list type="bullet">
    ///         <item>
    ///             <description>
    ///                 Standard Numeric Format Strings: https://msdn.microsoft.com/en-us/library/dwhawy9k(v=vs.110).aspx
    ///             </description>
    ///         </item>
    ///         <item>
    ///             <description>
    ///                 Custom Numeric Format Strings: https://msdn.microsoft.com/en-us/library/0c899ak8(v=vs.110).aspx
    ///             </description>
    ///         </item>
    ///     </list>
    /// </remarks>
    public sealed partial class ZNumberFormatter
    {
        private const int DEFAULT_EXP_PRECISION = 6;
        private const int HUNDRED_MILLION = 100000000;
        private const long SEVENTEEN_DIGITS_THRESHOLD = 10000000000000000;
        private const ulong ULONG_DIV_HUNDRED_MILLION = ulong.MaxValue/HUNDRED_MILLION;
        private const ulong ULONG_MOD_HUNDRED_MILLION = 1 + ulong.MaxValue%HUNDRED_MILLION;

        private const int DOUBLE_BITS_EXPONENT_SHIFT = 52;
        private const int DOUBLE_BITS_EXPONENT_MASK = 0x7ff;
        private const long DOUBLE_BITS_MANTISSA_MASK = 0xfffffffffffff;
        private const int DECIMAL_BITS_SCALE_MASK = 0x1f0000;

        private const int SINGLE_DEF_PRECISION = 7;
        private const int DOUBLE_DEF_PRECISION = 15;
        private const int INT8_DEF_PRECISION = 3;
        private const int UINT8_DEF_PRECISION = 3;
        private const int INT16_DEF_PRECISION = 5;
        private const int UINT16_DEF_PRECISION = 5;
        private const int INT32_DEF_PRECISION = 10;
        private const int UINT32_DEF_PRECISION = 10;
        private const int INT64_DEF_PRECISION = 19;
        private const int UINT64_DEF_PRECISION = 20;
        private const int DECIMAL_DEF_PRECISION = 100;
        private const int TEN_POWERS_LIST_LENGTH = 19;
        private const int ROUNDTRIP_BUFFER_SIZE = 24;
        private const double MIN_ROUNDTRIP_VAL = -1.79769313486231E+308;
        private const double MAX_ROUNDTRIP_VAL = 1.79769313486231E+308;

        [ThreadStatic] private static ZNumberFormatter _instance;

        private bool _infinity;
        private bool _isCustomFormat;

        private bool _NaN;
        private ZNumberFormatInfo _znfi;

        private bool _positive;
        private int _precision;
        private char _specifier;
        private bool _specifierIsUpper;
        private State _state;

        private ZNumberFormatter(Thread current)
        {
            CurrentCulture = (current != null)
                ? current.CurrentCulture
                : null;
        }

        /// <summary>
        ///     Per-thread instance of formatter.
        /// </summary>
        public static ZNumberFormatter Instance
        {
            get { return _instance ?? (_instance = new ZNumberFormatter(Thread.CurrentThread)); }
        }

        /// <summary>
        ///     Sets internal NumberFormatInfo for the current thread according to the <see cref="CultureInfo" /> value.
        /// </summary>
        public CultureInfo CurrentCulture
        {
            set
            {
                _znfi = (value != null)
                    ? new ZNumberFormatInfo(value.NumberFormat)
                    : ZNumberFormatInfo.NewInvariantInfo();
            }
        }

        // ReSharper disable once UnusedMember.Global
        /// <summary>
        ///     Sets internal NumberFormatInfo for the current thread according to the <see cref="CultureInfo" /> value.
        /// </summary>
        /// <param name="culture"></param>
        public static void SetThreadCurrentCulture(CultureInfo culture)
        {
            if (_instance != null)
            {
                _instance.CurrentCulture = culture;
            }
        }

        private ZNumberFormatInfo GetNumberFormatInstance(ZNumberFormatInfo znfi)
        {
            return znfi ?? _znfi;
        }

        #region StringBufferHelpers

        private unsafe void StringBufferReplace(char[] chs, int count)
        {
            Debug.Assert(count < ROUNDTRIP_BUFFER_SIZE);
            fixed (char* dest = RtStrBuffer)
            {
                var i = 0;
                while (i < count)
                {
                    dest[i] = chs[i];
                    ++i;
                }
                dest[count] = '\0';
                var pi = (int*) dest;
                pi[-1] = count;
            }
        }

        #endregion

        #region Nested type: CustomInfo

        internal struct CustomInfo
        {
            internal int DecimalDigits;
            internal int DecimalTailSharpDigits;
            internal int DividePlaces;
            internal int ExponentDigits;
            internal bool ExponentPositiveSignOnly;
            internal int ExponentTailSharpDigits;
            internal int IntegerDigits;
            private int IntegerHeadPos;
            internal int IntegerHeadSharpDigits;
            internal int Percents;
            internal int Permilles;
            internal bool UseExponent;
            internal bool UseGroup;

            public static int ParseCustomFormat(ref CustomInfo info, string format, int offset, int length)
            {
                var literal = '\0';
                var integerArea = true;
                var decimalArea = false;
                var exponentArea = false;
                var sharpContinues = true;
                var decimalPointPos = -1;
                var groupSeparatorCounter = 0;

                for (var i = offset; i - offset < length; i++)
                {
                    var c = format[i];

                    if (c == literal && c != '\0')
                    {
                        literal = '\0';
                        continue;
                    }
                    if (literal != '\0')
                    {
                        continue;
                    }

                    if (exponentArea && (c != '\0' && c != '0' && c != '#'))
                    {
                        exponentArea = false;
                        integerArea = (decimalPointPos < 0);
                        decimalArea = !integerArea;
                        i--;
                        continue;
                    }

                    switch (c)
                    {
                        case '\\':
                            i++;
                            continue;
                        case '\'':
                        case '\"':
                            if (c == '\"' || c == '\'')
                            {
                                literal = c;
                            }
                            continue;
                        case '#':
                            if (sharpContinues && integerArea)
                            {
                                info.IntegerHeadSharpDigits++;
                            }
                            else if (decimalArea)
                            {
                                info.DecimalTailSharpDigits++;
                            }
                            else if (exponentArea)
                            {
                                info.ExponentTailSharpDigits++;
                            }

                            goto case '0';
                        case '0':
                            if (c != '#')
                            {
                                sharpContinues = false;
                                if (decimalArea)
                                {
                                    info.DecimalTailSharpDigits = 0;
                                }
                                else if (exponentArea)
                                {
                                    info.ExponentTailSharpDigits = 0;
                                }
                            }
                            if (info.IntegerHeadPos == -1)
                            {
                                info.IntegerHeadPos = i;
                            }

                            if (integerArea)
                            {
                                info.IntegerDigits++;
                                if (groupSeparatorCounter > 0)
                                {
                                    info.UseGroup = true;
                                }
                                groupSeparatorCounter = 0;
                            }
                            else if (decimalArea)
                            {
                                info.DecimalDigits++;
                            }
                            else
                            {
                                info.ExponentDigits++;
                            }
                            break;
                        case 'e':
                        case 'E':
                            if (info.UseExponent)
                            {
                                break;
                            }

                            info.UseExponent = true;
                            integerArea = false;
                            decimalArea = false;
                            exponentArea = true;
                            if (i + 1 - offset < length)
                            {
                                var nc = format[i + 1];
                                if (nc == '+')
                                {
                                    info.ExponentPositiveSignOnly = true;
                                }
                                if (nc == '+' || nc == '-')
                                {
                                    i++;
                                }
                                else if (nc != '0' && nc != '#')
                                {
                                    info.UseExponent = false;
                                    if (decimalPointPos < 0)
                                    {
                                        integerArea = true;
                                    }
                                }
                            }

                            break;
                        case '.':
                            integerArea = false;
                            decimalArea = true;
                            exponentArea = false;
                            if (decimalPointPos == -1)
                            {
                                decimalPointPos = i;
                            }
                            break;
                        case '%':
                            info.Percents++;
                            break;
                        case '\u2030':
                            info.Permilles++;
                            break;
                        case ',':
                            if (integerArea && info.IntegerDigits > 0)
                            {
                                groupSeparatorCounter++;
                            }
                            break;
                    }
                }

                if (info.ExponentDigits == 0)
                {
                    info.UseExponent = false;
                }
                else
                {
                    info.IntegerHeadSharpDigits = 0;
                }

                if (info.DecimalDigits == 0)
                {
                    decimalPointPos = -1;
                }

                info.DividePlaces += groupSeparatorCounter*3;

                // return info;
                return decimalPointPos;
            }
        }

        #endregion

        #region Nested type: State

        private struct State
        {
            internal int _digitsLen;
            internal int _offset;
            internal int _defPrecision;
            internal int _decPointPos;
            // The following fields are a hexadecimal representation of the digits.
            // For instance _val = 0x234 represents the digits '2', '3', '4'.
            internal uint _val1; // Digits 0 - 7.
            internal uint _val2; // Digits 8 - 15.
            internal uint _val3; // Digits 16 - 23.
            internal uint _val4; // Digits 23 - 31. Only needed for decimals.

            internal void ResetNumber()
            {
                _val1 = _val2 = _val3 = _val4 = 0;
            }

            // Count number of hexadecimal digits stored in _val1 .. _val4.
            private int DecHexLen()
            {
                if (_val4 != 0)
                {
                    return DecHexLen(_val4) + 24;
                }
                if (_val3 != 0)
                {
                    return DecHexLen(_val3) + 16;
                }
                if (_val2 != 0)
                {
                    return DecHexLen(_val2) + 8;
                }
                if (_val1 != 0)
                {
                    return DecHexLen(_val1);
                }
                return 0;
            }

            private static int DecHexLen(uint val)
            {
                if (val < 0x10000)
                {
                    return FastDecHexLen((int) val);
                }
                return 4 + FastDecHexLen((int) (val >> 16));
            }

            // Helper to count number of hexadecimal digits in a number.
            private static int FastDecHexLen(int val)
            {
                if (val < 0x100)
                {
                    return val < 0x10 ? 1 : 2;
                }
                return val < 0x1000 ? 3 : 4;
            }

            internal void ResetDigitsLength()
            {
                _decPointPos = _digitsLen = DecHexLen();
            }

            internal void RecalcDigitsLength()
            {
                var newlen = DecHexLen();
                _decPointPos = _decPointPos + newlen - _digitsLen;
                _digitsLen = newlen;
            }

            internal int IntegerDigits
            {
                get { return _decPointPos > 0 ? _decPointPos : 1; }
            }

            internal int DecimalDigits
            {
                get { return _digitsLen > _decPointPos ? _digitsLen - _decPointPos : 0; }
            }

            internal bool IsFloatingSource
            {
                get { return _defPrecision == DOUBLE_DEF_PRECISION || _defPrecision == SINGLE_DEF_PRECISION; }
            }

            internal bool IsZero
            {
                get { return _digitsLen == 0; }
            }

            internal bool IsZeroInteger
            {
                get { return _digitsLen == 0 || _decPointPos <= 0; }
            }

            internal int CountTrailingZeros()
            {
                if (_val1 != 0)
                {
                    return CountTrailingZeros(_val1);
                }
                if (_val2 != 0)
                {
                    return CountTrailingZeros(_val2) + 8;
                }
                if (_val3 != 0)
                {
                    return CountTrailingZeros(_val3) + 16;
                }
                if (_val4 != 0)
                {
                    return CountTrailingZeros(_val4) + 24;
                }
                return _digitsLen;
            }

            private static int CountTrailingZeros(uint val)
            {
                if ((val & 0xffff) == 0)
                {
                    if ((val & 0xffffff) == 0)
                    {
                        if ((val & 0xfffffff) == 0)
                        {
                            return 7;
                        }
                        else
                        {
                            return 6;
                        }
                    }
                    else if ((val & 0xfffff) == 0)
                    {
                        return 5;
                    }
                    else
                    {
                        return 4;
                    }
                }
                else if ((val & 0xff) == 0)
                {
                    if ((val & 0xfff) == 0)
                    {
                        return 3;
                    }
                    else
                    {
                        return 2;
                    }
                }
                else if ((val & 0xf) == 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }

            internal void AddOneToDecHex()
            {
                if (_val1 == 0x99999999)
                {
                    _val1 = 0;
                    if (_val2 == 0x99999999)
                    {
                        _val2 = 0;
                        if (_val3 == 0x99999999)
                        {
                            _val3 = 0;
                            _val4 = AddOneToDecHex(_val4);
                        }
                        else
                        {
                            _val3 = AddOneToDecHex(_val3);
                        }
                    }
                    else
                    {
                        _val2 = AddOneToDecHex(_val2);
                    }
                }
                else
                {
                    _val1 = AddOneToDecHex(_val1);
                }
            }

            private static uint AddOneToDecHex(uint val)
            {
                // Assume val != 0x99999999
                if ((val & 0xffff) == 0x9999)
                {
                    if ((val & 0xffffff) == 0x999999)
                    {
                        if ((val & 0xfffffff) == 0x9999999)
                        {
                            return val + 0x06666667;
                        }
                        else
                        {
                            return val + 0x00666667;
                        }
                    }
                    else if ((val & 0xfffff) == 0x99999)
                    {
                        return val + 0x00066667;
                    }
                    else
                    {
                        return val + 0x00006667;
                    }
                }
                else if ((val & 0xff) == 0x99)
                {
                    if ((val & 0xfff) == 0x999)
                    {
                        return val + 0x00000667;
                    }
                    else
                    {
                        return val + 0x00000067;
                    }
                }
                else if ((val & 0xf) == 0x9)
                {
                    return val + 0x00000007;
                }
                else
                {
                    return val + 1;
                }
            }

            // Translate an unsigned int to hexadecimal digits.
            // i.e. 123456789 is represented by _val1 = 0x23456789 and _val2 = 0x1
            internal void InitDecHexDigits(uint value)
            {
                if (value >= HUNDRED_MILLION)
                {
                    var div1 = (int) (value/HUNDRED_MILLION);
                    value -= HUNDRED_MILLION*(uint) div1;
                    _val2 = FastToDecHex(div1);
                }
                _val1 = ToDecHex((int) value);
            }

            // Translate an unsigned long to hexadecimal digits.
            internal void InitDecHexDigits(ulong value)
            {
                if (value >= HUNDRED_MILLION)
                {
                    var div1 = (long) (value/HUNDRED_MILLION);
                    value -= HUNDRED_MILLION*(ulong) div1;
                    if (div1 >= HUNDRED_MILLION)
                    {
                        var div2 = (int) (div1/HUNDRED_MILLION);
                        div1 = div1 - div2*(long) HUNDRED_MILLION;
                        _val3 = ToDecHex(div2);
                    }
                    if (div1 != 0)
                    {
                        _val2 = ToDecHex((int) (div1));
                    }
                }
                if (value != 0)
                {
                    _val1 = ToDecHex((int) value);
                }
            }

            // Translate a decimal integer to hexadecimal digits.
            // The decimal integer is 96 digits and its value is hi * 2^64 + lo.
            // is the lower 64 bits.
            internal void InitDecHexDigits(uint hi, ulong lo)
            {
                if (hi == 0)
                {
                    InitDecHexDigits(lo); // Only the lower 64 bits matter.
                    return;
                }

                // Compute (hi, lo) = (hi , lo) / HundredMillion.
                var divhi = hi/HUNDRED_MILLION;
                ulong remhi = hi - divhi*HUNDRED_MILLION;
                var divlo = lo/HUNDRED_MILLION;
                var remlo = lo - divlo*HUNDRED_MILLION + remhi*ULONG_MOD_HUNDRED_MILLION;
                hi = divhi;
                lo = divlo + remhi*ULONG_DIV_HUNDRED_MILLION;
                divlo = remlo/HUNDRED_MILLION;
                remlo -= divlo*HUNDRED_MILLION;
                lo += divlo;
                _val1 = ToDecHex((int) remlo);

                // Divide hi * 2 ^ 64 + lo by HundredMillion using the fact that
                // hi < HundredMillion.
                divlo = lo/HUNDRED_MILLION;
                remlo = lo - divlo*HUNDRED_MILLION;
                lo = divlo;
                if (hi != 0)
                {
                    lo += hi*ULONG_DIV_HUNDRED_MILLION;
                    remlo += hi*ULONG_MOD_HUNDRED_MILLION;
                    divlo = remlo/HUNDRED_MILLION;
                    lo += divlo;
                    remlo -= divlo*HUNDRED_MILLION;
                }
                _val2 = ToDecHex((int) remlo);

                // Now we are left with 64 bits store in lo.
                if (lo >= HUNDRED_MILLION)
                {
                    divlo = lo/HUNDRED_MILLION;
                    lo -= divlo*HUNDRED_MILLION;
                    _val4 = ToDecHex((int) divlo);
                }
                _val3 = ToDecHex((int) lo);
            }

            internal void InitHex(ulong value)
            {
                switch (_defPrecision)
                {
                    case INT8_DEF_PRECISION:
                        value = (byte) value;
                        break;
                    case INT16_DEF_PRECISION:
                        value = (ushort) value;
                        break;
                    case INT32_DEF_PRECISION:
                        value = (uint) value;
                        break;
                }
                _val1 = (uint) value;
                _val2 = (uint) (value >> 32);
                ResetDigitsLength();
                if (value == 0)
                {
                    _decPointPos = 1;
                }
            }

            internal void RemoveTrailingZeros()
            {
                _digitsLen -= (_offset = CountTrailingZeros());
            }
        }

        #endregion

        #region Inits

        // Parse the given format and initialize the following fields:
        //   _isCustomFormat, _specifierIsUpper, _specifier & _precision.
        private void Init(string format)
        {
            _state = new State();
            _NaN = _infinity = false;
            _isCustomFormat = false;
            _specifierIsUpper = true;
            _precision = -1;

            if (format == null || format.Length == 0)
            {
                _specifier = 'G';
                return;
            }

            var specifier = format[0];
            if (specifier >= 'a' && specifier <= 'z')
            {
                specifier = (char) (specifier - 'a' + 'A');
                _specifierIsUpper = false;
            }
            else if (specifier < 'A' || specifier > 'Z')
            {
                _isCustomFormat = true;
                _specifier = '0';
                return;
            }
            _specifier = specifier;
            if (format.Length > 1)
            {
                _precision = 0;
                for (var i = 1; i < format.Length; i++)
                {
                    var val = format[i] - '0';
                    _precision = _precision*10 + val;
                    if (val < 0 || val > 9 || _precision > 99)
                    {
                        // Is it a custom format?
                        _isCustomFormat = true;
                        _specifier = '0';
                        _precision = -1;
                        break;
                    }
                }
            }
        }

        private void Init(string format, int value, int defPrecision)
        {
            Init(format);
            _state._defPrecision = defPrecision;
            _positive = value >= 0;

            if (value == 0 || _specifier == 'X')
            {
                _state.InitHex((ulong) value);
                return;
            }

            if (value < 0)
            {
                value = -value;
            }
            _state.InitDecHexDigits((uint) value);
            _state.ResetDigitsLength();
        }

        private void Init(string format, uint value, int defPrecision)
        {
            Init(format);
            _state._defPrecision = defPrecision;
            _positive = true;

            if (value == 0 || _specifier == 'X')
            {
                _state.InitHex(value);
                return;
            }

            _state.InitDecHexDigits(value);
            _state.ResetDigitsLength();
        }

        private void Init(string format, long value)
        {
            Init(format);
            _state._defPrecision = INT64_DEF_PRECISION;
            _positive = value >= 0;

            if (value == 0 || _specifier == 'X')
            {
                _state.InitHex((ulong) value);
                return;
            }

            if (value < 0)
            {
                value = -value;
            }
            _state.InitDecHexDigits((ulong) value);
            _state.ResetDigitsLength();
        }

        private void Init(string format, ulong value)
        {
            Init(format);
            _state._defPrecision = UINT64_DEF_PRECISION;
            _positive = true;

            if (value == 0 || _specifier == 'X')
            {
                _state.InitHex((ulong) value);
                return;
            }

            _state.InitDecHexDigits(value);
            _state.ResetDigitsLength();
        }

        private void Init(string format, double value, int defPrecision)
        {
            Init(format);

            _state._defPrecision = defPrecision;
            var bits = DoubleToInt64Bits(value);
            _positive = bits >= 0;
            bits &= long.MaxValue;
            if (bits == 0)
            {
                _state._decPointPos = 1;
                _state._digitsLen = 0;
                _positive = true;
                return;
            }

            var e = (int) (bits >> DOUBLE_BITS_EXPONENT_SHIFT);
            var m = bits & DOUBLE_BITS_MANTISSA_MASK;
            if (e == DOUBLE_BITS_EXPONENT_MASK)
            {
                _NaN = m != 0;
                _infinity = m == 0;
                return;
            }

            var expAdjust = 0;
            if (e == 0)
            {
                // We need 'm' to be large enough so we won't lose precision.
                e = 1;
                var scale = ScaleOrder(m);
                if (scale < DOUBLE_DEF_PRECISION)
                {
                    expAdjust = scale - DOUBLE_DEF_PRECISION;
                    m *= GetTenPowerOf(-expAdjust);
                }
            }
            else
            {
                m = (m + DOUBLE_BITS_MANTISSA_MASK + 1)*10;
                expAdjust = -1;
            }

            // multiply the mantissa by 10 ^ N
            ulong lo = (uint) m;
            var hi = (ulong) m >> 32;
            var lo2 = MantissaBitsTable[e];
            var hi2 = lo2 >> 32;
            lo2 = (uint) lo2;
            var mm = hi*lo2 + lo*hi2 + ((lo*lo2) >> 32);
            var res = (long) (hi*hi2 + (mm >> 32));
            while (res < SEVENTEEN_DIGITS_THRESHOLD)
            {
                mm = (mm & uint.MaxValue)*10;
                res = res*10 + (long) (mm >> 32);
                expAdjust--;
            }
            if ((mm & 0x80000000) != 0)
            {
                res++;
            }

            var order = DOUBLE_DEF_PRECISION + 2;
            _state._decPointPos = TensExponentTable[e] + expAdjust + order;

            // Rescale 'res' to the initial precision (15-17 for doubles).
            var initialPrecision = InitialFloatingPrecision();
            if (order > initialPrecision)
            {
                var val = GetTenPowerOf(order - initialPrecision);
                res = (res + (val >> 1))/val;
                order = initialPrecision;
            }
            if (res >= GetTenPowerOf(order))
            {
                order++;
                _state._decPointPos++;
            }

            _state.InitDecHexDigits((ulong) res);

            _state._offset = _state.CountTrailingZeros();
            _state._digitsLen = order - _state._offset;
        }

        private void Init(string format, decimal value)
        {
            Init(format);
            _state._defPrecision = DECIMAL_DEF_PRECISION;

            var bits = decimal.GetBits(value);
            var scale = (bits[3] & DECIMAL_BITS_SCALE_MASK) >> 16;
            _positive = bits[3] >= 0;
            if (bits[0] == 0 && bits[1] == 0 && bits[2] == 0)
            {
                _state._decPointPos = -scale;
                _positive = true;
                _state._digitsLen = 0;
                return;
            }

            _state.InitDecHexDigits((uint) bits[2], ((ulong) bits[1] << 32) | (uint) bits[0]);
            _state.ResetDigitsLength();
            _state._decPointPos = _state._digitsLen - scale;
            if (_precision != -1 || _specifier != 'G')
            {
                _state.RemoveTrailingZeros();
            }
        }

        #endregion

        #region Helpers

        private static unsafe void GetFormatActiveSection(string format, bool zero, ref bool positive, ref int offset,
            ref int length)
        {
            var lens = stackalloc int[3];
            var index = 0;
            var lastPos = 0;
            var literal = '\0';
            for (var i = 0; i < format.Length; i++)
            {
                var c = format[i];

                if (c == literal || (literal == '\0' && (c == '\"' || c == '\'')))
                {
                    literal = (literal == '\0') ? c : '\0';
                    continue;
                }

                if (literal == '\0' && format[i] == ';' && (i == 0 || format[i - 1] != '\\'))
                {
                    lens[index++] = i - lastPos;
                    lastPos = i + 1;
                    if (index == 3)
                    {
                        break;
                    }
                }
            }

            switch (index)
            {
                case 0:
                    offset = 0;
                    length = format.Length;
                    return;
                case 1:
                    if (positive || zero)
                    {
                        offset = 0;
                        length = lens[0];
                        return;
                    }
                    if (lens[0] + 1 < format.Length)
                    {
                        positive = true;
                        offset = lens[0] + 1;
                        length = format.Length - offset;
                        return;
                    }
                    else
                    {
                        offset = 0;
                        length = lens[0];
                        return;
                    }
                case 2:
                    if (zero)
                    {
                        offset = lens[0] + lens[1] + 2;
                        length = format.Length - offset;
                        return;
                    }
                    if (positive)
                    {
                        offset = 0;
                        length = lens[0];
                        return;
                    }
                    if (lens[1] > 0)
                    {
                        positive = true;
                        offset = lens[0] + 1;
                        length = lens[1];
                        return;
                    }
                    else
                    {
                        offset = 0;
                        length = lens[0];
                        return;
                    }
                case 3:
                    if (zero)
                    {
                        offset = lens[0] + lens[1] + 2;
                        length = lens[2];
                        return;
                    }
                    if (positive)
                    {
                        offset = 0;
                        length = lens[0];
                        return;
                    }
                    if (lens[1] > 0)
                    {
                        positive = true;
                        offset = lens[0] + 1;
                        length = lens[1];
                        return;
                    }
                    else
                    {
                        offset = 0;
                        length = lens[0];
                        return;
                    }
            }
            throw new ArgumentException();
        }

        private static unsafe long DoubleToInt64Bits(double value)
        {
            return *((long*) &value);
        }

        private void Divide10(int count)
        {
            if (count <= 0 || _state.IsZero)
            {
                return;
            }

            _state._decPointPos = _state._decPointPos - count;
        }

        // Helper to translate an int in the range 0 .. 9999 to its
        // Hexadecimal digits representation.
        private static uint FastToDecHex(int val)
        {
            if (val < 100)
            {
                return (uint) DecHexDigits[val];
            }

            // Uses 2^19 (524288) to compute val / 100 for val < 10000.
            var v = (val*5243) >> 19;
            return (uint) ((DecHexDigits[v] << 8) | DecHexDigits[val - v*100]);
        }

        private static long GetTenPowerOf(int i)
        {
            return TenPowersList[i];
        }

        // Compute the initial precision for rounding a floating number
        // according to the used format.
        private int InitialFloatingPrecision()
        {
            if (_specifier == 'R')
            {
                return _state._defPrecision + 2;
            }
            if (_precision < _state._defPrecision)
            {
                return _state._defPrecision;
            }
            if (_specifier == 'G')
            {
                return Math.Min(_state._defPrecision + 2, _precision);
            }
            if (_specifier == 'E')
            {
                return Math.Min(_state._defPrecision + 2, _precision + 1);
            }
            return _state._defPrecision;
        }

        private void Multiply10(int count)
        {
            if (count <= 0 || _state.IsZero)
            {
                return;
            }

            _state._decPointPos = _state._decPointPos + count;
        }

        private void RoundBits(int shift)
        {
            if (shift <= 0)
            {
                return;
            }

            if (shift > _state._digitsLen)
            {
                _state._digitsLen = 0;
                _state._decPointPos = 1;
                _state.ResetNumber();
                _positive = true;
                return;
            }
            shift += _state._offset;
            _state._digitsLen = _state._digitsLen + _state._offset;
            while (shift > 8)
            {
                _state._val1 = _state._val2;
                _state._val2 = _state._val3;
                _state._val3 = _state._val4;
                _state._val4 = 0;
                _state._digitsLen = _state._digitsLen - 8;
                shift -= 8;
            }
            shift = (shift - 1) << 2;
            var v = _state._val1 >> shift;
            var rem16 = v & 0xf;
            _state._val1 = (v ^ rem16) << shift;
            if (rem16 >= 0x5)
            {
                _state._val1 = _state._val1 | 0x99999999 >> (28 - shift);
                _state.AddOneToDecHex();
                _state.RecalcDigitsLength();
            }
            _state.RemoveTrailingZeros();
            if (_state._digitsLen == 0)
            {
                _state._offset = 0;
                _state._decPointPos = 1;
                _positive = true;
            }
        }

        private void RoundDecimal(int decimals)
        {
            RoundBits(_state._digitsLen - _state._decPointPos - decimals);
        }

        private void RoundPos(int pos)
        {
            RoundBits(_state._digitsLen - pos);
        }

        // Helper to count the 10th scale (number of digits) in a number
        private static int ScaleOrder(long hi)
        {
            for (var i = TEN_POWERS_LIST_LENGTH - 1; i >= 0; i--)
            {
                if (hi >= GetTenPowerOf(i))
                {
                    return i + 1;
                }
            }
            return 1;
        }

        // Helper to translate an int in the range 0 .. 99999999 to its
        // Hexadecimal digits representation.
        private static uint ToDecHex(int val)
        {
            uint res = 0;
            if (val >= 10000)
            {
                var v = val/10000;
                val -= v*10000;
                res = FastToDecHex(v) << 16;
            }
            return res | FastToDecHex(val);
        }

        #endregion

        #region Inner String Buffer

        private string _rtStrBuffer;

        private string RtStrBuffer
        {
            get { return _rtStrBuffer ?? (_rtStrBuffer = new string('\0', ROUNDTRIP_BUFFER_SIZE)); }
        }

        private char[] _cbuf = new char[64];
        private int _ind;

        private void ResetCharBuf()
        {
            _ind = 0;
        }

        private void Resize(int len)
        {
            if (len <= _cbuf.Length)
            {
                return;
            }
            var newsize = len;
            // get next power of 2
            newsize--;
            newsize |= newsize >> 1;
            newsize |= newsize >> 2;
            newsize |= newsize >> 4;
            newsize |= newsize >> 8;
            newsize |= newsize >> 16;
            newsize++;
            var newBuf = new char[newsize];
            Array.Copy(_cbuf, newBuf, _ind);
            _cbuf = newBuf;
        }

        private void Append(char c)
        {
            Resize(_ind + 1);
            _cbuf[_ind++] = c;
        }

        private void Append(char c, int cnt)
        {
            Resize(_ind + cnt);
            while (cnt-- > 0)
            {
                _cbuf[_ind++] = c;
            }
        }

        private void Append(string s)
        {
            var slen = s.Length;
            Resize(_ind + slen);
            for (var i = 0; i < slen; i++)
            {
                _cbuf[_ind++] = s[i];
            }
        }

        #endregion Inner String Buffer

        #region public number formatting methods

        // ReSharper disable UnusedMember.Global

        /// <summary>
        ///     Returns result of formatting. Use together with <see cref="Count" /> property.
        /// </summary>
        public char[] Chars
        {
            get { return _cbuf; }
        }

        /// <summary>
        ///     Returns the number of characters in the result. Use together with <see cref="Chars" /> property.
        /// </summary>
        public int Count
        {
            get { return _ind; }
        }

        #region NumberToChars : (Format, Number, ZNumberFormatInfo) => void

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">culture-specific format information</param>
        public void NumberToChars(string format, sbyte value, ZNumberFormatInfo znfi)
        {
            var nfi = GetNumberFormatInstance(znfi);
            Init(format, value, INT8_DEF_PRECISION);
            IntegerToString(format, nfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">culture-specific format information</param>
        public void NumberToChars(string format, byte value, ZNumberFormatInfo znfi)
        {
            var nfi = GetNumberFormatInstance(znfi);
            Init(format, value, UINT8_DEF_PRECISION);
            IntegerToString(format, nfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">culture-specific format information</param>
        public void NumberToChars(string format, ushort value, ZNumberFormatInfo znfi)
        {
            var nfi = GetNumberFormatInstance(znfi);
            Init(format, value, INT16_DEF_PRECISION);
            IntegerToString(format, nfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">culture-specific format information</param>
        public void NumberToChars(string format, short value, ZNumberFormatInfo znfi)
        {
            var nfi = GetNumberFormatInstance(znfi);
            Init(format, value, UINT16_DEF_PRECISION);
            IntegerToString(format, nfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">culture-specific format information</param>
        public void NumberToChars(string format, uint value, ZNumberFormatInfo znfi)
        {
            var nfi = GetNumberFormatInstance(znfi);
            Init(format, value, INT32_DEF_PRECISION);
            IntegerToString(format, nfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">culture-specific format information</param>
        public void NumberToChars(string format, int value, ZNumberFormatInfo znfi)
        {
            var nfi = GetNumberFormatInstance(znfi);
            Init(format, value, UINT32_DEF_PRECISION);
            IntegerToString(format, nfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">culture-specific format information</param>
        public void NumberToChars(string format, ulong value, ZNumberFormatInfo znfi)
        {
            var nfi = GetNumberFormatInstance(znfi);
            Init(format, value);
            IntegerToString(format, nfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">culture-specific format information</param>
        public void NumberToChars(string format, long value, ZNumberFormatInfo znfi)
        {
            var nfi = GetNumberFormatInstance(znfi);
            Init(format, value);
            IntegerToString(format, nfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">culture-specific format information</param>
        public void NumberToChars(string format, float value, ZNumberFormatInfo znfi)
        {
            var nfi = GetNumberFormatInstance(znfi);
            Init(format, value, SINGLE_DEF_PRECISION);
            if (_NaN)
            {
                ResetCharBuf();
                Append(nfi.NaNSymbol);
                return;
            }
            if (_infinity)
            {
                ResetCharBuf();
                Append(_positive ? nfi.PositiveInfinitySymbol : nfi.NegativeInfinitySymbol);
                return;
            }
            if (_specifier == 'R')
            {
                FormatRoundtrip(value, nfi);
                return;
            }
            NumberToChars(format, nfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">culture-specific format information</param>
        public void NumberToChars(string format, double value, ZNumberFormatInfo znfi)
        {
            var nfi = GetNumberFormatInstance(znfi);
            Init(format, value, DOUBLE_DEF_PRECISION);
            if (_NaN)
            {
                ResetCharBuf();
                Append(nfi.NaNSymbol);
                return;
            }
            if (_infinity)
            {
                ResetCharBuf();
                Append(_positive ? nfi.PositiveInfinitySymbol : nfi.NegativeInfinitySymbol);
                return;
            }
            if (_specifier == 'R')
            {
                FormatRoundtrip(value, nfi);
                return;
            }
            NumberToChars(format, nfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">culture-specific format information</param>
        public void NumberToChars(string format, decimal value, ZNumberFormatInfo znfi)
        {
            var nfi = GetNumberFormatInstance(znfi);
            Init(format, value);
            NumberToChars(format, nfi);
        }

        #endregion

        #region NumberToChars : (Number, ZNumberFormatInfo) => void 

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the optional culture-specific format information.
        /// </summary>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">optional culture-specific format information</param>
        public void NumberToChars(uint value, ZNumberFormatInfo znfi)
        {
            if (value >= HUNDRED_MILLION)
            {
                NumberToChars(null, value, znfi);
            }
            else
            {
                FastIntegerToString((int) value, znfi);
            }
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the optional culture-specific format information.
        /// </summary>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">optional culture-specific format information</param>
        public void NumberToChars(int value, ZNumberFormatInfo znfi)
        {
            if (value >= HUNDRED_MILLION || value <= -HUNDRED_MILLION)
            {
                NumberToChars(null, value, znfi);
            }
            else
            {
                FastIntegerToString(value, znfi);
            }
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the optional culture-specific format information.
        /// </summary>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">optional culture-specific format information</param>
        public void NumberToChars(ulong value, ZNumberFormatInfo znfi)
        {
            if (value >= HUNDRED_MILLION)
            {
                NumberToChars(null, value, znfi);
            }
            else
            {
                FastIntegerToString((int) value, znfi);
            }
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the optional culture-specific format information.
        /// </summary>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">optional culture-specific format information</param>
        public void NumberToChars(long value, ZNumberFormatInfo znfi)
        {
            if (value >= HUNDRED_MILLION || value <= -HUNDRED_MILLION)
            {
                NumberToChars(null, value, znfi);
            }
            else
            {
                FastIntegerToString((int) value, znfi);
            }
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the optional culture-specific format information.
        /// </summary>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">optional culture-specific format information</param>
        public void NumberToChars(float value, ZNumberFormatInfo znfi)
        {
            Init(null, value, SINGLE_DEF_PRECISION);
            var nfi = GetNumberFormatInstance(znfi);
            if (_NaN)
            {
                ResetCharBuf();
                Append(nfi.NaNSymbol);
                return;
            }
            if (_infinity)
            {
                ResetCharBuf();
                Append(_positive ? nfi.PositiveInfinitySymbol : nfi.NegativeInfinitySymbol);
                return;
            }
            FormatGeneral(-1, nfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the optional culture-specific format information.
        /// </summary>
        /// <param name="value">numeric value</param>
        /// <param name="znfi">optional culture-specific format information</param>
        public void NumberToChars(double value, ZNumberFormatInfo znfi)
        {
            var nfi = GetNumberFormatInstance(znfi);
            Init(null, value, DOUBLE_DEF_PRECISION);
            if (_NaN)
            {
                ResetCharBuf();
                Append(nfi.NaNSymbol);
                return;
            }
            if (_infinity)
            {
                ResetCharBuf();
                Append(_positive ? nfi.PositiveInfinitySymbol : nfi.NegativeInfinitySymbol);
                return;
            }
            FormatGeneral(-1, nfi);
        }

        #endregion

        #region NumberToChars : (Format, Number) => void

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        public void NumberToChars(string format, uint value)
        {
            Init(format, value, INT32_DEF_PRECISION);
            IntegerToString(format, _znfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        public void NumberToChars(string format, int value)
        {
            Init(format, value, UINT32_DEF_PRECISION);
            IntegerToString(format, _znfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        public void NumberToChars(string format, ulong value)
        {
            Init(format, value);
            IntegerToString(format, _znfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        public void NumberToChars(string format, long value)
        {
            Init(format, value);
            IntegerToString(format, _znfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        public void NumberToChars(string format, float value)
        {
            Init(format, value, SINGLE_DEF_PRECISION);
            if (_NaN)
            {
                ResetCharBuf();
                Append(_znfi.NaNSymbol);
                return;
            }
            if (_infinity)
            {
                ResetCharBuf();
                Append(_positive ? _znfi.PositiveInfinitySymbol : _znfi.NegativeInfinitySymbol);
                return;
            }
            if (_specifier == 'R')
            {
                FormatRoundtrip(value, _znfi);
                return;
            }
            NumberToChars(format, _znfi);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count
        ///     using the specified format.
        /// </summary>
        /// <param name="format">standard formatting string</param>
        /// <param name="value">numeric value</param>
        public void NumberToChars(string format, double value)
        {
            Init(format, value, DOUBLE_DEF_PRECISION);
            if (_NaN)
            {
                ResetCharBuf();
                Append(_znfi.NaNSymbol);
                return;
            }
            if (_infinity)
            {
                ResetCharBuf();
                Append(_positive ? _znfi.PositiveInfinitySymbol : _znfi.NegativeInfinitySymbol);
                return;
            }
            if (_specifier == 'R')
            {
                FormatRoundtrip(value, _znfi);
                return;
            }
            NumberToChars(format, _znfi);
        }

        #endregion

        #region NumberToChars : (Number) => void 

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count.
        /// </summary>
        /// <param name="value">numeric value</param>
        public void NumberToChars(uint value)
        {
            NumberToChars(value, null);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count.
        /// </summary>
        /// <param name="value">numeric value</param>
        public void NumberToChars(int value)
        {
            NumberToChars(value, null);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count.
        /// </summary>
        /// <param name="value">numeric value</param>
        public void NumberToChars(ulong value)
        {
            NumberToChars(value, null);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count.
        /// </summary>
        /// <param name="value">numeric value</param>
        public void NumberToChars(long value)
        {
            NumberToChars(value, null);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count.
        /// </summary>
        /// <param name="value">numeric value</param>
        public void NumberToChars(float value)
        {
            NumberToChars(value, null);
        }

        /// <summary>
        ///     Converts the numeric value to the reference to the character buffer and characters count.
        /// </summary>
        /// <param name="value">numeric value</param>
        public void NumberToChars(double value)
        {
            NumberToChars(value, null);
        }

        #endregion

        // ReSharper restore UnusedMember.Global

        #endregion public number formatting methods

        #region private number formatting methods

        private void FastIntegerToString(int value, ZNumberFormatInfo fp)
        {
            ResetCharBuf();
            if (value < 0)
            {
                var sign = GetNumberFormatInstance(fp).NegativeSign;
                value = -value;
                Append(sign);
            }
            if (value >= 10000)
            {
                var v = value/10000;
                FastAppendDigits(v, false);
                FastAppendDigits(value - v*10000, true);
            }
            else
            {
                FastAppendDigits(value, false);
            }
        }

        private void IntegerToString(string format, [NotNull] ZNumberFormatInfo nfi)
        {
            switch (_specifier)
            {
                case 'C':
                    FormatCurrency(_precision, nfi);
                    break;
                case 'D':
                    FormatDecimal(_precision, nfi);
                    break;
                case 'E':
                    FormatExponential(_precision, nfi);
                    break;
                case 'F':
                    FormatFixedPoint(_precision, nfi);
                    break;
                case 'G':
                    if (_precision <= 0)
                    {
                        FormatDecimal(-1, nfi);
                    }
                    else
                    {
                        FormatGeneral(_precision, nfi);
                    }
                    break;
                case 'N':
                    FormatNumber(_precision, nfi);
                    break;
                case 'P':
                    FormatPercent(_precision, nfi);
                    break;
                case 'X':
                    FormatHexadecimal(_precision);
                    break;
                default:
                    if (_isCustomFormat)
                    {
                        FormatCustom(format, nfi);
                    }
                    else
                    {
                        throw new FormatException("The specified format '" + format + "' is invalid");
                    }
                    break;
            }
        }

        private void NumberToChars(string format, ZNumberFormatInfo nfi)
        {
            switch (_specifier)
            {
                case 'C':
                    FormatCurrency(_precision, nfi);
                    break;
                case 'E':
                    FormatExponential(_precision, nfi);
                    break;
                case 'F':
                    FormatFixedPoint(_precision, nfi);
                    break;
                case 'G':
                    FormatGeneral(_precision, nfi);
                    break;
                case 'N':
                    FormatNumber(_precision, nfi);
                    break;
                case 'P':
                    FormatPercent(_precision, nfi);
                    break;
                // ReSharper disable once RedundantCaseLabel
                case 'X':
                default:
                    if (_isCustomFormat)
                    {
                        FormatCustom(format, nfi);
                    }
                    else
                    {
                        throw new FormatException("The specified format '" + format + "' is invalid");
                    }
                    break;
            }
        }

        private void FormatCurrency(int precision, ZNumberFormatInfo nfi)
        {
            precision = (precision >= 0 ? precision : nfi.CurrencyDecimalDigits);
            RoundDecimal(precision);
            ResetCharBuf();

            if (_positive)
            {
                switch (nfi.CurrencyPositivePattern)
                {
                    case 0:
                        Append(nfi.CurrencySymbol);
                        break;
                    case 2:
                        Append(nfi.CurrencySymbol);
                        Append(' ');
                        break;
                }
            }
            else
            {
                switch (nfi.CurrencyNegativePattern)
                {
                    case 0:
                        Append('(');
                        Append(nfi.CurrencySymbol);
                        break;
                    case 1:
                        Append(nfi.NegativeSign);
                        Append(nfi.CurrencySymbol);
                        break;
                    case 2:
                        Append(nfi.CurrencySymbol);
                        Append(nfi.NegativeSign);
                        break;
                    case 3:
                        Append(nfi.CurrencySymbol);
                        break;
                    case 4:
                        Append('(');
                        break;
                    case 5:
                        Append(nfi.NegativeSign);
                        break;
                    case 8:
                        Append(nfi.NegativeSign);
                        break;
                    case 9:
                        Append(nfi.NegativeSign);
                        Append(nfi.CurrencySymbol);
                        Append(' ');
                        break;
                    case 11:
                        Append(nfi.CurrencySymbol);
                        Append(' ');
                        break;
                    case 12:
                        Append(nfi.CurrencySymbol);
                        Append(' ');
                        Append(nfi.NegativeSign);
                        break;
                    case 14:
                        Append('(');
                        Append(nfi.CurrencySymbol);
                        Append(' ');
                        break;
                    case 15:
                        Append('(');
                        break;
                }
            }

            AppendIntegerStringWithGroupSeparator(nfi.CurrencyGroupSizes, nfi.CurrencyGroupSeparator);

            if (precision > 0)
            {
                Append(nfi.CurrencyDecimalSeparator);
                AppendDecimalString(precision);
            }

            if (_positive)
            {
                switch (nfi.CurrencyPositivePattern)
                {
                    case 1:
                        Append(nfi.CurrencySymbol);
                        break;
                    case 3:
                        Append(' ');
                        Append(nfi.CurrencySymbol);
                        break;
                }
            }
            else
            {
                switch (nfi.CurrencyNegativePattern)
                {
                    case 0:
                        Append(')');
                        break;
                    case 3:
                        Append(nfi.NegativeSign);
                        break;
                    case 4:
                        Append(nfi.CurrencySymbol);
                        Append(')');
                        break;
                    case 5:
                        Append(nfi.CurrencySymbol);
                        break;
                    case 6:
                        Append(nfi.NegativeSign);
                        Append(nfi.CurrencySymbol);
                        break;
                    case 7:
                        Append(nfi.CurrencySymbol);
                        Append(nfi.NegativeSign);
                        break;
                    case 8:
                        Append(' ');
                        Append(nfi.CurrencySymbol);
                        break;
                    case 10:
                        Append(' ');
                        Append(nfi.CurrencySymbol);
                        Append(nfi.NegativeSign);
                        break;
                    case 11:
                        Append(nfi.NegativeSign);
                        break;
                    case 13:
                        Append(nfi.NegativeSign);
                        Append(' ');
                        Append(nfi.CurrencySymbol);
                        break;
                    case 14:
                        Append(')');
                        break;
                    case 15:
                        Append(' ');
                        Append(nfi.CurrencySymbol);
                        Append(')');
                        break;
                }
            }
        }

        private void FormatDecimal(int precision, ZNumberFormatInfo nfi)
        {
            if (precision < _state._digitsLen)
            {
                precision = _state._digitsLen;
            }
            if (precision == 0)
            {
                ResetCharBuf();
                _cbuf[_ind++] = '0';
                return;
            }

            ResetCharBuf();

            if (!_positive)
            {
                Append(nfi.NegativeSign);
            }
            AppendDigits(0, precision);
        }

        private void FormatHexadecimal(int precision)
        {
            var size = Math.Max(precision, _state._decPointPos);
            var digits = _specifierIsUpper ? DigitUpperTable : DigitLowerTable;
            ResetCharBuf();
            Resize(size);
            _ind = size;
            var val = _state._val1 | ((ulong) _state._val2 << 32);
            while (size > 0)
            {
                _cbuf[--size] = digits[val & 0xf];
                val >>= 4;
            }
        }

        private void FormatFixedPoint(int precision, ZNumberFormatInfo nfi)
        {
            if (precision == -1)
            {
                precision = nfi.NumberDecimalDigits;
            }

            RoundDecimal(precision);

            ResetCharBuf();

            if (!_positive)
            {
                Append(nfi.NegativeSign);
            }

            AppendIntegerString(_state.IntegerDigits);

            if (precision > 0)
            {
                Append(nfi.NumberDecimalSeparator);
                AppendDecimalString(precision);
            }
        }

        [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
        private void FormatRoundtrip(double origval, ZNumberFormatInfo nfi)
        {
            var state = _state;
            if (origval >= MIN_ROUNDTRIP_VAL && origval <= MAX_ROUNDTRIP_VAL)
            {
                FormatGeneral(_state._defPrecision, nfi);
                StringBufferReplace(Chars, Count);
                if (origval == double.Parse(RtStrBuffer, nfi))
                {
                    return;
                }
            }
            ResetCharBuf();
            _state = state;
            FormatGeneral(_state._defPrecision + 2, nfi);
        }

        [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
        private void FormatRoundtrip(float origval, ZNumberFormatInfo nfi)
        {
            var state = _state;
            FormatGeneral(_state._defPrecision, nfi);
            StringBufferReplace(Chars, Count);
            // Check roundtrip only for "normal" double values.
            if (origval == float.Parse(RtStrBuffer, nfi))
            {
                return;
            }
            ResetCharBuf();
            _state = state;
            FormatGeneral(_state._defPrecision + 2, nfi);
        }

        private void FormatGeneral(int precision, ZNumberFormatInfo nfi)
        {
            bool enableExp;
            if (precision == -1)
            {
                enableExp = _state.IsFloatingSource;
                precision = _state._defPrecision;
            }
            else
            {
                enableExp = true;
                if (precision == 0)
                {
                    precision = _state._defPrecision;
                }
                RoundPos(precision);
            }

            var intDigits = _state._decPointPos;
            var digits = _state._digitsLen;
            var decDigits = digits - intDigits;

            if ((intDigits > precision || intDigits <= -4) && enableExp)
            {
                FormatExponential(digits - 1, nfi, 2);
                return;
            }

            if (decDigits < 0)
            {
                decDigits = 0;
            }
            if (intDigits < 0)
            {
                intDigits = 0;
            }
            ResetCharBuf();

            if (!_positive)
            {
                Append(nfi.NegativeSign);
            }

            if (intDigits == 0)
            {
                Append('0');
            }
            else
            {
                AppendDigits(digits - intDigits, digits);
            }

            if (decDigits > 0)
            {
                Append(nfi.NumberDecimalSeparator);
                AppendDigits(0, decDigits);
            }
        }

        private void FormatNumber(int precision, ZNumberFormatInfo nfi)
        {
            precision = (precision >= 0 ? precision : nfi.NumberDecimalDigits);
            ResetCharBuf();
            RoundDecimal(precision);

            if (!_positive)
            {
                switch (nfi.NumberNegativePattern)
                {
                    case 0:
                        Append('(');
                        break;
                    case 1:
                        Append(nfi.NegativeSign);
                        break;
                    case 2:
                        Append(nfi.NegativeSign);
                        Append(' ');
                        break;
                }
            }

            AppendIntegerStringWithGroupSeparator(nfi.NumberGroupSizes, nfi.NumberGroupSeparator);

            if (precision > 0)
            {
                Append(nfi.NumberDecimalSeparator);
                AppendDecimalString(precision);
            }

            if (!_positive)
            {
                switch (nfi.NumberNegativePattern)
                {
                    case 0:
                        Append(')');
                        break;
                    case 3:
                        Append(nfi.NegativeSign);
                        break;
                    case 4:
                        Append(' ');
                        Append(nfi.NegativeSign);
                        break;
                }
            }
        }

        private void FormatPercent(int precision, ZNumberFormatInfo nfi)
        {
            precision = (precision >= 0 ? precision : nfi.PercentDecimalDigits);
            Multiply10(2);
            RoundDecimal(precision);
            ResetCharBuf();

            if (_positive)
            {
                if (nfi.PercentPositivePattern == 2)
                {
                    Append(nfi.PercentSymbol);
                }
            }
            else
            {
                switch (nfi.PercentNegativePattern)
                {
                    case 0:
                        Append(nfi.NegativeSign);
                        break;
                    case 1:
                        Append(nfi.NegativeSign);
                        break;
                    case 2:
                        Append(nfi.NegativeSign);
                        Append(nfi.PercentSymbol);
                        break;
                }
            }

            AppendIntegerStringWithGroupSeparator(nfi.PercentGroupSizes, nfi.PercentGroupSeparator);

            if (precision > 0)
            {
                Append(nfi.PercentDecimalSeparator);
                AppendDecimalString(precision);
            }

            if (_positive)
            {
                switch (nfi.PercentPositivePattern)
                {
                    case 0:
                        Append(' ');
                        Append(nfi.PercentSymbol);
                        break;
                    case 1:
                        Append(nfi.PercentSymbol);
                        break;
                }
            }
            else
            {
                switch (nfi.PercentNegativePattern)
                {
                    case 0:
                        Append(' ');
                        Append(nfi.PercentSymbol);
                        break;
                    case 1:
                        Append(nfi.PercentSymbol);
                        break;
                }
            }
        }

        private void FormatExponential(int precision, ZNumberFormatInfo nfi)
        {
            if (precision == -1)
            {
                precision = DEFAULT_EXP_PRECISION;
            }

            RoundPos(precision + 1);
            FormatExponential(precision, nfi, 3);
        }

        private void FormatExponential(int precision, ZNumberFormatInfo nfi, int expDigits)
        {
            var digits = _state._digitsLen;
            var exponent = _state._decPointPos - 1;
            _state._decPointPos = 1;

            ResetCharBuf();

            if (!_positive)
            {
                Append(nfi.NegativeSign);
            }

            AppendOneDigit(digits - 1);

            if (precision > 0)
            {
                Append(nfi.NumberDecimalSeparator);
                AppendDigits(digits - precision - 1, digits - _state._decPointPos);
            }

            AppendExponent(nfi, exponent, expDigits);
        }

        private unsafe void FormatCustom(string format, ZNumberFormatInfo nfi)
        {
            var positive = _positive;
            var offset = 0;
            var length = 0;
            GetFormatActiveSection(format, _state.IsZero, ref positive, ref offset, ref length);
            if (length == 0)
            {
                ResetCharBuf();
                Append(_positive ? string.Empty : nfi.NegativeSign);
            }
            _positive = positive;

            var info = new CustomInfo();
            var decimalPointPos = CustomInfo.ParseCustomFormat(ref info, format, offset, length);
            if (info.Percents > 0)
            {
                Multiply10(2*info.Percents);
            }
            if (info.Permilles > 0)
            {
                Multiply10(3*info.Permilles);
            }
            if (info.DividePlaces > 0)
            {
                Divide10(info.DividePlaces);
            }

            // allocate all helper buffers on stack, 3*512 chars
            var sb_int_b = stackalloc char[StackBuffer.STACK_BUFFER_CHARS];
            var sb_int = new StackBuffer(sb_int_b, StackBuffer.STACK_BUFFER_CHARS);

            var sb_dec_b = stackalloc char[StackBuffer.STACK_BUFFER_CHARS];
            var sb_dec = new StackBuffer(sb_dec_b, StackBuffer.STACK_BUFFER_CHARS);

            var sb_exp_b = stackalloc char[StackBuffer.STACK_BUFFER_CHARS];
            var sb_exp = new StackBuffer(sb_exp_b, StackBuffer.STACK_BUFFER_CHARS);

            var diff = 0;
            var expPositive = true;
            if (info.UseExponent && (info.DecimalDigits > 0 || info.IntegerDigits > 0))
            {
                if (!_state.IsZero)
                {
                    RoundPos(info.DecimalDigits + info.IntegerDigits);
                    diff -= _state._decPointPos - info.IntegerDigits;
                    _state._decPointPos = info.IntegerDigits;
                }

                expPositive = diff <= 0;
                AppendNonNegativeNumber(ref sb_exp, diff < 0 ? -diff : diff);
            }
            else
            {
                RoundDecimal(info.DecimalDigits);
            }

            if (info.IntegerDigits != 0 || !_state.IsZeroInteger)
            {
                AppendIntegerString(_state.IntegerDigits, ref sb_int);
            }

            AppendDecimalString(_state.DecimalDigits, ref sb_dec);

            if (info.UseExponent)
            {
                if (info.DecimalDigits <= 0 && info.IntegerDigits <= 0)
                {
                    _positive = true;
                }

                if (sb_int.Length < info.IntegerDigits)
                {
                    sb_int.Prepend('0', info.IntegerDigits - sb_int.Length);
                }

                while (sb_exp.Length < info.ExponentDigits - info.ExponentTailSharpDigits)
                {
                    sb_exp.Prepend('0');
                }

                if (expPositive && info.ExponentPositiveSignOnly)
                {
                    sb_exp.Prepend(nfi.PositiveSign);
                }
                else if (!expPositive)
                {
                    sb_exp.Prepend(nfi.NegativeSign);
                }
            }
            else
            {
                if (sb_int.Length < info.IntegerDigits - info.IntegerHeadSharpDigits)
                {
                    sb_int.Prepend('0', info.IntegerDigits - info.IntegerHeadSharpDigits - sb_int.Length);
                }
                if (info.IntegerDigits == info.IntegerHeadSharpDigits && IsZeroOnly(ref sb_int))
                {
                    sb_int.TrimLeft(sb_int.Length);
                }
            }

            ZeroTrimEnd(ref sb_dec, true);
            while (sb_dec.Length < info.DecimalDigits - info.DecimalTailSharpDigits)
            {
                sb_dec.Append('0');
            }
            if (sb_dec.Length > info.DecimalDigits)
            {
                sb_dec.TrimRight(info.DecimalDigits);
            }

            var sb_b = stackalloc char[StackBuffer.STACK_BUFFER_CHARS];
            var sb = new StackBuffer(sb_b, StackBuffer.STACK_BUFFER_CHARS, nfi.NegativeSign.Length);
            var literal = '\0';
            var integerArea = true;
            var intSharpCounter = 0;
            var sb_int_index = 0;
            var sb_dec_index = 0;

            var groups = nfi.NumberGroupSizes;
            var groupSeparator = nfi.NumberGroupSeparator;
            int intLen = 0, total = 0, groupIndex = 0, counter = 0, groupSize = 0;
            if (info.UseGroup && groups.Length > 0)
            {
                intLen = sb_int.Length;
                for (var i = 0; i < groups.Length; i++)
                {
                    total += groups[i];
                    if (total <= intLen)
                    {
                        groupIndex = i;
                    }
                }
                groupSize = groups[groupIndex];
                var fraction = intLen > total ? intLen - total : 0;
                if (groupSize == 0)
                {
                    while (groupIndex >= 0 && groups[groupIndex] == 0)
                    {
                        groupIndex--;
                    }

                    groupSize = fraction > 0 ? fraction : groups[groupIndex];
                }
                if (fraction == 0)
                {
                    counter = groupSize;
                }
                else
                {
                    groupIndex += fraction/groupSize;
                    counter = fraction%groupSize;
                    if (counter == 0)
                    {
                        counter = groupSize;
                    }
                    else
                    {
                        groupIndex++;
                    }
                }
            }
            else
            {
                info.UseGroup = false;
            }

            for (var i = offset; i - offset < length; i++)
            {
                var c = format[i];

                if (c == literal && c != '\0')
                {
                    literal = '\0';
                    continue;
                }
                if (literal != '\0')
                {
                    sb.Append(c);
                    continue;
                }

                switch (c)
                {
                    case '\\':
                        i++;
                        if (i - offset < length)
                        {
                            sb.Append(format[i]);
                        }
                        continue;
                    case '\'':
                    case '\"':
                        if (c == '\"' || c == '\'')
                        {
                            literal = c;
                        }
                        continue;
                    case '#':
                        goto case '0';
                    case '0':
                        if (integerArea)
                        {
                            intSharpCounter++;
                            if (info.IntegerDigits - intSharpCounter < sb_int.Length + sb_int_index || c == '0')
                            {
                                while (info.IntegerDigits - intSharpCounter + sb_int_index < sb_int.Length)
                                {
                                    sb.Append(sb_int[sb_int_index++]);
                                    if (info.UseGroup && --intLen > 0 && --counter == 0)
                                    {
                                        sb.Append(groupSeparator);
                                        if (--groupIndex < groups.Length && groupIndex >= 0)
                                        {
                                            groupSize = groups[groupIndex];
                                        }
                                        counter = groupSize;
                                    }
                                }
                            }
                            break;
                        }
                        if (sb_dec_index < sb_dec.Length)
                        {
                            sb.Append(sb_dec[sb_dec_index++]);
                        }
                        break;
                    case 'e':
                    case 'E':
                        if (!info.UseExponent)
                        {
                            sb.Append(c);
                            break;
                        }

                        var flag1 = true;
                        var flag2 = false;

                        int q;
                        for (q = i + 1; q - offset < length; q++)
                        {
                            if (format[q] == '0')
                            {
                                flag2 = true;
                                continue;
                            }
                            if (q == i + 1 && (format[q] == '+' || format[q] == '-'))
                            {
                                continue;
                            }
                            if (!flag2)
                            {
                                flag1 = false;
                            }
                            break;
                        }

                        if (flag1)
                        {
                            i = q - 1;
                            integerArea = (decimalPointPos < 0);

                            sb.Append(c);
                            sb.Append(ref sb_exp);
                        }
                        else
                        {
                            sb.Append(c);
                        }

                        break;
                    case '.':
                        if (decimalPointPos == i)
                        {
                            if (info.DecimalDigits > 0)
                            {
                                while (sb_int_index < sb_int.Length)
                                {
                                    sb.Append(sb_int[sb_int_index++]);
                                }
                            }
                            if (sb_dec.Length > 0)
                            {
                                sb.Append(nfi.NumberDecimalSeparator);
                            }
                        }
                        integerArea = false;
                        break;
                    case ',':
                        break;
                    case '%':
                        sb.Append(nfi.PercentSymbol);
                        break;
                    case '\u2030':
                        sb.Append(nfi.PerMilleSymbol);
                        break;
                    default:
                        sb.Append(c);
                        break;
                }
            }

            if (!_positive)
            {
                sb.Prepend(nfi.NegativeSign);
            }
            ResetCharBuf();
            Resize(sb.Length);
            // unsafe:
            for (var p = sb.Begin; p < sb.End; p++)
            {
                _cbuf[_ind++] = *p;
            }
        }

        #endregion private number formatting methods

        #region StackBuffer formatting helpers

        private static void ZeroTrimEnd(ref StackBuffer sb, bool canEmpty)
        {
            var len = 0;
            for (var i = sb.Length - 1; (canEmpty ? i >= 0 : i > 0); i--)
            {
                if (sb[i] != '0')
                {
                    break;
                }
                len++;
            }

            if (len > 0)
            {
                sb.TrimRight(len);
            }
        }

        private static bool IsZeroOnly(ref StackBuffer sb)
        {
            for (var i = 0; i < sb.Length; i++)
            {
                if (char.IsDigit(sb[i]) && sb[i] != '0')
                {
                    return false;
                }
            }
            return true;
        }

        private static void AppendNonNegativeNumber(ref StackBuffer sb, int v)
        {
            if (v < 0)
            {
                throw new ArgumentException();
            }

            var i = ScaleOrder(v) - 1;
            do
            {
                var n = v/(int) GetTenPowerOf(i);
                sb.Append((char) ('0' | n));
                v -= (int) GetTenPowerOf(i--)*n;
            } while (i >= 0);
        }

        #endregion StackBuffer formatting helpers

        #region Append helpers

        private void AppendIntegerString(int minLength, ref StackBuffer sb)
        {
            if (_state._decPointPos <= 0)
            {
                sb.Append('0', minLength);
                return;
            }

            if (_state._decPointPos < minLength)
            {
                sb.Append('0', minLength - _state._decPointPos);
            }

            AppendDigits(_state._digitsLen - _state._decPointPos, _state._digitsLen, ref sb);
        }

        private void AppendIntegerString(int minLength)
        {
            if (_state._decPointPos <= 0)
            {
                Append('0', minLength);
                return;
            }

            if (_state._decPointPos < minLength)
            {
                Append('0', minLength - _state._decPointPos);
            }

            AppendDigits(_state._digitsLen - _state._decPointPos, _state._digitsLen);
        }

        private void AppendDecimalString(int precision, ref StackBuffer sb)
        {
            var start = _state._digitsLen - precision - _state._decPointPos;
            var end = _state._digitsLen - _state._decPointPos;
            AppendDigits(start, end, ref sb);
        }

        private void AppendDecimalString(int precision)
        {
            AppendDigits(_state._digitsLen - precision - _state._decPointPos, _state._digitsLen - _state._decPointPos);
        }

        private void AppendIntegerStringWithGroupSeparator(int[] groups, string groupSeparator)
        {
            if (_state.IsZeroInteger)
            {
                Append('0');
                return;
            }

            var total = 0;
            var groupIndex = 0;
            for (var i = 0; i < groups.Length; i++)
            {
                total += groups[i];
                if (total <= _state._decPointPos)
                {
                    groupIndex = i;
                }
                else
                {
                    break;
                }
            }

            if (groups.Length > 0 && total > 0)
            {
                int counter;
                var groupSize = groups[groupIndex];
                var fraction = _state._decPointPos > total ? _state._decPointPos - total : 0;
                if (groupSize == 0)
                {
                    while (groupIndex >= 0 && groups[groupIndex] == 0)
                    {
                        groupIndex--;
                    }

                    groupSize = fraction > 0 ? fraction : groups[groupIndex];
                }
                if (fraction == 0)
                {
                    counter = groupSize;
                }
                else
                {
                    groupIndex += fraction/groupSize;
                    counter = fraction%groupSize;
                    if (counter == 0)
                    {
                        counter = groupSize;
                    }
                    else
                    {
                        groupIndex++;
                    }
                }

                for (var i = 0;;)
                {
                    if ((_state._decPointPos - i) <= counter || counter == 0)
                    {
                        AppendDigits(_state._digitsLen - _state._decPointPos, _state._digitsLen - i);
                        break;
                    }
                    AppendDigits(_state._digitsLen - i - counter, _state._digitsLen - i);
                    i += counter;
                    Append(groupSeparator);
                    if (--groupIndex < groups.Length && groupIndex >= 0)
                    {
                        groupSize = groups[groupIndex];
                    }
                    counter = groupSize;
                }
            }
            else
            {
                AppendDigits(_state._digitsLen - _state._decPointPos, _state._digitsLen);
            }
        }

        // minDigits is in the range 1..3
        private void AppendExponent(ZNumberFormatInfo nfi, int exponent, int minDigits)
        {
            if (_specifierIsUpper || _specifier == 'R')
            {
                Append('E');
            }
            else
            {
                Append('e');
            }

            if (exponent >= 0)
            {
                Append(nfi.PositiveSign);
            }
            else
            {
                Append(nfi.NegativeSign);
                exponent = -exponent;
            }

            if (exponent == 0)
            {
                Append('0', minDigits);
            }
            else if (exponent < 10)
            {
                Append('0', minDigits - 1);
                Append((char) ('0' | exponent));
            }
            else
            {
                var hexDigit = FastToDecHex(exponent);
                if (exponent >= 100 || minDigits == 3)
                {
                    Append((char) ('0' | (hexDigit >> 8)));
                }
                Append((char) ('0' | ((hexDigit >> 4) & 0xf)));
                Append((char) ('0' | (hexDigit & 0xf)));
            }
        }

        private void AppendOneDigit(int start)
        {
            Resize(_ind + 10);
            start += _state._offset;
            uint v;
            if (start < 0)
            {
                v = 0;
            }
            else if (start < 8)
            {
                v = _state._val1;
            }
            else if (start < 16)
            {
                v = _state._val2;
            }
            else if (start < 24)
            {
                v = _state._val3;
            }
            else if (start < 32)
            {
                v = _state._val4;
            }
            else
            {
                v = 0;
            }
            v >>= (start & 0x7) << 2;
            _cbuf[_ind++] = (char) ('0' | v & 0xf);
        }

        private void FastAppendDigits(int val, bool force)
        {
            Resize(_ind + 4);
            var i = _ind;
            int digits;
            if (force || val >= 100)
            {
                var v = (val*5243) >> 19;
                digits = DecHexDigits[v];
                if (force || val >= 1000)
                {
                    _cbuf[i++] = (char) ('0' | digits >> 4);
                }
                _cbuf[i++] = (char) ('0' | (digits & 0xf));
                digits = DecHexDigits[val - v*100];
            }
            else
            {
                digits = DecHexDigits[val];
            }

            if (force || val >= 10)
            {
                _cbuf[i++] = (char) ('0' | digits >> 4);
            }
            _cbuf[i++] = (char) ('0' | (digits & 0xf));
            _ind = i;
        }

        private void AppendDigits(int start, int end)
        {
            if (start >= end)
            {
                return;
            }

            var i = _ind + (end - start);
            Resize(i);
            _ind = i;

            end += _state._offset;
            start += _state._offset;

            for (var next = start + 8 - (start & 0x7);; start = next, next += 8)
            {
                uint v;
                if (next == 8)
                {
                    v = _state._val1;
                }
                else if (next == 16)
                {
                    v = _state._val2;
                }
                else if (next == 24)
                {
                    v = _state._val3;
                }
                else if (next == 32)
                {
                    v = _state._val4;
                }
                else
                {
                    v = 0;
                }
                v >>= (start & 0x7) << 2;
                if (next > end)
                {
                    next = end;
                }

                _cbuf[--i] = (char) ('0' | v & 0xf);
                switch (next - start)
                {
                    case 8:
                        _cbuf[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 7;
                    case 7:
                        _cbuf[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 6;
                    case 6:
                        _cbuf[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 5;
                    case 5:
                        _cbuf[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 4;
                    case 4:
                        _cbuf[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 3;
                    case 3:
                        _cbuf[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 2;
                    case 2:
                        _cbuf[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 1;
                    case 1:
                        if (next == end)
                        {
                            return;
                        }
                        continue;
                }
            }
        }

        private void AppendDigits(int start, int end, ref StackBuffer sb)
        {
            if (start >= end)
            {
                return;
            }

            var i = sb.Length + (end - start);
            sb.Length = i;

            end += _state._offset;
            start += _state._offset;

            for (var next = start + 8 - (start & 0x7);; start = next, next += 8)
            {
                uint v;
                if (next == 8)
                {
                    v = _state._val1;
                }
                else if (next == 16)
                {
                    v = _state._val2;
                }
                else if (next == 24)
                {
                    v = _state._val3;
                }
                else if (next == 32)
                {
                    v = _state._val4;
                }
                else
                {
                    v = 0;
                }
                v >>= (start & 0x7) << 2;
                if (next > end)
                {
                    next = end;
                }
                sb[--i] = (char) ('0' | v & 0xf);
                switch (next - start)
                {
                    case 8:
                        sb[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 7;
                    case 7:
                        sb[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 6;
                    case 6:
                        sb[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 5;
                    case 5:
                        sb[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 4;
                    case 4:
                        sb[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 3;
                    case 3:
                        sb[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 2;
                    case 2:
                        sb[--i] = (char) ('0' | (v >>= 4) & 0xf);
                        goto case 1;
                    case 1:
                        if (next == end)
                        {
                            return;
                        }
                        continue;
                }
            }
        }

        #endregion Append helpers
    }
}
