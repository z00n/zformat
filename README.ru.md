# ZFormat

### Unity3D .NET library for formatting numbers without memory allocation

----

#### TL;DR

The `ZFormat` library is a possibility to do all kinds of number formatting,
including Currency and Custom, and to communicate with APIs expecting
System.String, with zero dynamic memory allocation.

Библиотека `ZFormat` – это инструмент для форматирования чисел всеми
возможными способами, включая Currency и Custom, и для общения с API,
ожидающими System.String, не выделяя динамически ни байта памяти.

#### Motivation

For a long time I've been bothered by the fact that there is no standard way
to convert a number into a formatted string without memory allocation. Not
only every Format* method in .NET returns a freshly allocated string, but, in
addition, allocate memory for various inner buffers, clone the
`NumberFormatInfo` class, etc.

Меня давно  раздражал тот факт, что стандартными средствами невозможно
превратить число в отформатированную строку, не выделяя при этом память. Все
Format* методы в .NET мало того, что возвращают свежевыделенную строку, но при
этом еще и выделяют память для разных внутренних буферов, клонируют класс
`NumberFormatInfo` и.т.д.

Assume that you want to show an FPS counter on the screen, and you write
something like this:

```csharp
FPSCounter.text = string.Format("{0:F2}", fps);
```

Допустим, вы хотите показать на экране счетчик FPS и пишете что-то вроде:

```csharp
FPSCounter.text = string.Format("{0:F2}", fps);
```

This will cause memory allocation, 0.25KB - seems like no big deal, but if
there will be memory allocation, however small, for every frame, sooner or
later GC is  going to reclaim garbage, and there will be a discernible pause
in the game. Due to the fact that, historically, Unity3D uses conservative
non-generational GC, the  duration of the pause is determined by the total
size of the mono heap, that even  in a mobile game can reach tens of megabytes
(and the pause, accordingly, tens of milliseconds). Another issue is the
subsequent [memory heap fragmentation][Frag].

При этом выделяется память, 0.25KB – казалось бы, немного, но, если вы
выделяете даже немного памяти каждый кадр, рано или поздно GC соберет
неиспользованную память, и в игре произойдет ощутимая пауза. В связи с тем,
что по историческим причинам Unity3D использует консервативный
non-generational GC, продолжительность этой паузы зависит от общего размера
mono heap, который даже в мобильной игре может составлять десятки мегабайт (а
пауза, соответственно, десятки миллисекунд). Еще одной неприятной проблемой
является сопутствующая [фрагментация memory heap][Frag].

Within the past years the community have developed some guidelines for memory
management. But the situation with strings has not changed much. For the
reason  that strings in .NET are immutable, you can't use "pool-of-strings";
for the reason that many APIs are expecting strings, you can't use
`pool-of-StringBuilder` or  `pool-of-List<Char>` without the ultimate
conversion into a string. But even if this issue will be resolved, it will
still be impossible to use standard means  of formatting
(i.e.,<nobr>`xxx.ToString(String, IFormatProvider)`</nobr>),   because they,
too, allocate memory.

За все эти годы сообщество выработало некоторые правила обращения с памятью в
Unity3D; вот очень хорошая статья: ["C# Memory Management for Unity
Developers"][Reich]. Unity Technology тоже не сидело без дела: последние годы
они последовательно [улучшали API][UnityAPI] и [пытаются настроить
GC][IL2CPP_GC].

Но со строками все осталось по-прежнему. Поскольку строки в .NET immutable,
нельзя сделать "pool-of-strings"; поскольку многие API ожидают строку, нельзя
использовать `pool-of-StringBuilder` или `pool-of-List<Char>` без последующей
конвертации в строку. Но даже если эта задача будет решена, нельзя будет
пользоваться стандартными средствами форматирования (i.e.,
<nobr>`xxx.ToString(String, IFormatProvider)`</nobr>), так как они тоже
выделяют память.


#### Quick Start

 `ZFormat` API is somewhat bulky but flexible and non-coupled. Its API
 comprises some extension methods that make things sleeker, but they are  not
 shown here for the sake of clarity:

```csharp
private readonly ZString _price = new ZString();
private readonly _esFormatInfo = new ZNumberFormatInfo("es-ES");
...
...
// there are no memory allocations after this point
var formatter = ZNumberFormatter.Instance;

formatter.NumberToChars("C2", 12.345, _esFormatInfo);

_price.Reset(formatter.Chars,formater.Count);

// using it with uGUI Text
PriceLabel.text = _price.ToString();

// tell uGUI to redraw this text label (should be an extension method)
PriceLabel.SetVerticesDirty();
PriceLabel.SetLayoutDirty();
PriceLabel.cachedTextGenerator.Invalidate();

// now you get '12,12 €' on screen
```

#### Features

`ZFormat` is 100% compatible with all .NET [standard][StandardFS] and
[custom][CustomFS] format specifiers. It's based on mono's `NumberFormatter`
and correctly passes all (hundreds of) tests.

`ZFormat` is fully culture-aware and supports all culture information
available in Unity3D runtime.

`ZFormat` supports either mono runtime or IL2CPP.

`ZString` is a growable mutable string with an interface similar to
`StringBuilder`. Its ToString method returns the reference to its internal
string buffer, and can be used for talking with API, that is expecting
`System.String`.

```csharp
var zstr = new ZString(8);
zstr.Clear();
zstr.Append('H').Append("ello").Append(new[]{'!','\n'});
Debug.Log(zstr.ToString());
```

#### Performance

Performance is about the same with StringBuilder + Format combination. It
may be slightly better than `{0, XX:YY}` `String.Format` methods.

#### Limitations

- `ZFormat` extensively uses unsafe code and pointers, and therefore can't be
supported by WebPlayer (now deprecated, though).

- Round-trip formatting still allocate some bytes, because mono's Double.Parse
does it. It's not easily fixable. However, I consider its usages rather rare.

- `ZFormat` does not support (yet?) `{0,XX:YY}` formatting and paddings.

- `ZFormat` is not thread-safe. You should serialize access to the
`ZNumberFormatter.Instance` by yourself ([Unity3D does not support
`ThreadStatic`][ThreadStatic])

#### API

###### class ZNumberFormatter

###### class ZNumberFormatInfo

###### class ZString


--------------------

[Frag]: http://stackoverflow.com/questions/3770457/what-is-memory-fragmentation

[Evo]: https://evo.my.com/en/?lang=en_US

[Geldreich]: http://www.gamasutra.com/blogs/RichGeldreich/20150731/250071/

[Reich]: http://www.gamasutra.com/blogs/WendelinReich/20131109/203841/

[UnityAPI]: http://docs.unity3d.com/ScriptReference/Mesh.SetVertices.html

[IL2CPP_GC]:http://blogs.unity3d.com/2015/07/09/il2cpp-internals-garbage-collector-integration/

[StandardFS]: https://msdn.microsoft.com/en-us/library/dwhawy9k(v=vs.110).aspx

[CustomFS]: https://msdn.microsoft.com/en-us/library/0c899ak8(v=vs.110).aspx

[ThreadStatic]: http://docs.unity3d.com/Manual/Attributes.html
