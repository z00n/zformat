// FloatingPointFormatterTest.cs - NUnit Test Cases for the System.FloatingPointFormatter class
//
// Authors:
// 	Duncan Mak (duncan@ximian.com)
//	Atsushi Enomoto (atsushi@ximian.com)
//
// (C) 2003 Ximian, Inc.  http://www.ximian.com
// (C) 2004 Novell Inc.
// 

#region Usings

using System.Globalization;
using System.Threading;
using NUnit.Framework;
// ReSharper disable All

#endregion

namespace ZFormat
{
    [TestFixture]
    public class FloatingPointFormatterTest
    {
        private CultureInfo old_culture;

        [SetUp]
        public void SetUp()
        {
            old_culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);
        }

        [TearDown]
        public void TearDown()
        {
            Thread.CurrentThread.CurrentCulture = old_culture;
        }

        [Test]
        public void Format1()
        {
            Assert.AreEqual("100000000000000", 1.0e+14.Fmt(), "F1");
            Assert.AreEqual("1E+15", 1.0e+15.Fmt(), "F2");
            Assert.AreEqual("1E+16", 1.0e+16.Fmt(), "F3");
            Assert.AreEqual("1E+17", 1.0e+17.Fmt(), "F4");
        }

        [Test]
        public void FormatStartsWithDot()
        {
            var ci = new CultureInfo("en-US");
            var val = 12345.1234567890123456;
            var s = val.Fmt(".0################;-.0################;0.0", ci);
            Assert.AreEqual("12345.123456789", s, "#1");

            s = (-val).Fmt(".0################;-.0#######;#########;0.0", ci);
            Assert.AreEqual("-12345.12345679", s, "#2");

            s = 0.0.Fmt(".0################;-.0#######;+-0", ci);
            Assert.AreEqual("+-0", s, "#3");
        }

        [Test]
        public void PermillePercent()
        {
            var ci = CultureInfo.InvariantCulture;
            Assert.AreEqual("485.7\u2030", (0.4857).Fmt("###.###\u2030"), "#1");
            var nfi = new NumberFormatInfo();
            nfi.NegativeSign = "";
            nfi.PerMilleSymbol = "m";
            nfi.PercentSymbol = "percent";
            Assert.AreEqual("m485.7", 0.4857.Fmt("\u2030###.###", nfi), "#2");
            Assert.AreEqual("485.7m", 0.4857.Fmt("###.###\u2030", nfi), "#3");
            Assert.AreEqual("percent48.57", 0.4857.Fmt("%###.###", nfi), "#4");
            Assert.AreEqual("48.57percent", 0.4857.Fmt("###.###%", nfi), "#5");
        }

        [Test]
        public void LiteralMixed()
        {
            var ci = CultureInfo.InvariantCulture;
            Assert.AreEqual("test 235", 234.56.Fmt("'test' ###", ci), "#1");
            Assert.AreEqual("235 test", 234.56.Fmt("### 'test'", ci), "#2");
            Assert.AreEqual("234 test.56", 234.56.Fmt("### 'test'.###", ci), "#3");
            Assert.AreEqual("hoge 235", 234.56.Fmt("'hoge' ###", ci), "#1");
            Assert.AreEqual("235 hoge", 234.56.Fmt("### 'hoge'", ci), "#2");
            Assert.AreEqual("234 hoge.56", 234.56.Fmt("### 'hoge'.###", ci), "#3");
        }
    }
}
