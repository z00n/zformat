#region Usings

#region Usings

using System;
using System.Collections.Generic;
using System.Globalization;
using NUnit.Framework;

#endregion

// ReSharper disable All

#endregion

namespace ZFormat
{
    [TestFixture]
    public class ZStringTest
    {
        [Test]
        public void Length()
        {
            var zs = new ZString(16);
            Assert.That(zs.Length, Is.EqualTo(0));
            zs.Length = zs.Capacity;
            Assert.That(zs.Length, Is.EqualTo(zs.Capacity));
            Assert.That(zs.Length, Is.EqualTo(16));
        }

        [Test]
        public void Zeros()
        {
            var zs = new ZString();
            zs.Append("A\0B\0C");
            var i1 = zs.ToString().IndexOf('\0');
            zs[i1] = ' ';
            var i2 = zs.ToString().IndexOf('\0');
            zs[i2] = ' ';
            Assert.That(i1 == 1);
            Assert.That(i2 == 3);
            Assert.That(zs.ToString() == "A B C");
        }

        [Test]
        public void InternalGetSetAt()
        {
            var zs = new ZString();
            zs.Append("ABC");
            Assert.That(zs[0], Is.EqualTo('A'), "#1");
            zs[2] = ('Z');
            Assert.That(zs[2], Is.EqualTo('Z'), "#2");
            Assert.Throws(typeof (IndexOutOfRangeException), () => { var x = zs[zs.Length]; }, "#3");
            Assert.Throws(typeof (IndexOutOfRangeException), () => { zs[zs.Capacity] = '?'; }, "#4");
        }

        [Test]
        public void AddFrags01()
        {
            var x = (-12.34).ToString("N1", CultureInfo.CreateSpecificCulture("sv-SE"));
            var zs = new ZString();
            var frags = new object[] {
                "A", new[] {'B', 'C'}, "D", new ZString().Append("E").Append("F"), new List<char> {'a', 'b'}
            };
            var flen = ZString.LengthOfFrags(frags);
            zs.Append(frags);
            Assert.That(zs.Length, Is.EqualTo(flen));
            Assert.That(zs.ToString(), Is.EqualTo("ABCDEFab"));
        }

        [Test]
        public void AddingList()
        {
            var zs = new ZString();
            var lst = new List<char>("abc".ToCharArray());
            zs.Append(lst);
            Assert.That(zs.Length, Is.EqualTo(3));
            Assert.That(zs.ToString(), Is.EqualTo("abc"));
        }

        [Test]
        public void AddingChar()
        {
            var zs = new ZString();
            zs.Append('A');
            Assert.That(zs.Length, Is.EqualTo(1));
            Assert.That(zs.ToString(), Is.EqualTo("A"));
            zs.Append('B');
            Assert.That(zs.Length, Is.EqualTo(2));
            Assert.That(zs.ToString(), Is.EqualTo("AB"));
        }

        [Test]
        public void AddingChars()
        {
            var zs = new ZString();
            zs.Append(new[] {'A'});
            Assert.That(zs.Length, Is.EqualTo(1));
            Assert.That(zs.ToString(), Is.EqualTo("A"));
            zs.Append(new[] {'B'});
            Assert.That(zs.Length, Is.EqualTo(2));
            Assert.That(zs.ToString(), Is.EqualTo("AB"));

            zs.Append(new[] {'B', 'U', 'B', 'Y'});
            Assert.That(zs.Length, Is.EqualTo(6));
            Assert.That(zs.ToString(), Is.EqualTo("ABBUBY"));
        }

        [Test]
        public void AddingChars01()
        {
            var zs = new ZString(4);
            var chars = new[] {'A', 'B', 'C', 'D', 'E'};
            Assert.Throws<ArgumentException>(() => zs.Append(chars, 6), "#1");
        }

        [Test]
        public void ResettingChars()
        {
            var zs = new ZString(4);
            var chars = new[] {'A', 'B', 'C', 'D', 'E'};
            char[] nchars = null;
            Assert.Throws<ArgumentNullException>(() => zs.Reset(nchars, 0), "#1");
            Assert.Throws<ArgumentException>(() => zs.Reset(chars, 6), "#2");
            Assert.That(zs.Reset(chars, 4).ToString(), Is.EqualTo("ABCD"), "#4");
            Assert.That(zs.Reset(chars, 3).ToString(), Is.EqualTo("ABC"), "#5");
            Assert.That(zs.Reset(chars, 0).ToString(), Is.EqualTo(""), "#6");
            Assert.That(zs.Reset("Hello").ToString(), Is.EqualTo("Hello"));
        }

        [Test]
        public void AddingString()
        {
            var zs = new ZString();
            zs.Append("A");
            Assert.That(zs.Length, Is.EqualTo(1));
            Assert.That(zs.ToString(), Is.EqualTo("A"));
            zs.Append("B");
            Assert.That(zs.Length, Is.EqualTo(2));
            Assert.That(zs.ToString(), Is.EqualTo("AB"));
        }

        [Test]
        public void Capacity()
        {
            var zs = new ZString(3);
            zs.Append("ABC");
            Assert.That(zs.Length, Is.EqualTo(3), "#1");
            Assert.That(zs.Capacity, Is.EqualTo(3), "#2");
            zs.Capacity = 128;
            Console.WriteLine(zs.Capacity);
            Assert.That(zs.Capacity == 128, "#3");
        }

        [Test]
        public void Creation01()
        {
            var zs = new ZString();
            Assert.That(zs.Length, Is.EqualTo(0));
            Assert.That(zs.Capacity, Is.GreaterThan(0));
        }

        [Test]
        public void StrCopy()
        {
            var s1 = "Hello";
            var s2 = "1234567890";
            ZString.StrCopy(s1, s2);
            Assert.That("Hello", Is.EqualTo(s1), "#1");
            Assert.That(s1.Length == s2.Length, "#2");
            Assert.That(s1 == s2, "#3");

            var s3 = "";
            var s4 = "1234567890";
            ZString.StrCopy(s3, s4);
            Assert.That("", Is.EqualTo(s3), "#4");
            Assert.That(s3.Length == s4.Length, "#5");
            Assert.That(s3 == s4, "#6");
        }

        [Test]
        public void EnsureCapacity()
        {
            var zs = new ZString(5);
            Assert.That(zs.Capacity == 5, "#1");
            zs.EnsureCapacity(6);
            Assert.That(zs.Capacity == 8, "#2");
            zs.EnsureCapacity(10 - 1);
            Assert.That(zs.Capacity == 16, "#3");
        }

        [Test]
        public void Telescope01()
        {
            var zs = new ZString();
            zs
                .Append('H')
                .Append("el")
                .Append('l')
                .Append(new[] {'o'});
            Assert.That(zs.ToString() == "Hello", "#1");
            var zs1 = new ZString().Append(" World");
            zs
                .Append(zs1)
                .Append('!');
            Assert.That(zs.ToString() == "Hello World!", "#2");
        }

        [Test]
        public void Telescope02()
        {
            var zs = new ZString();
            zs
                .Reset(new object[] {"H", "el", "l", "o"})
                .Append(new char[] {' '})
                .Append(new List<char> {'W', 'o', 'r', 'l', 'd'})
                .Append('!');
            Assert.That(zs.ToString() == "Hello World!", "#1");
        }

        [Test]
        public void AppendRecursion()
        {
            var zs = new ZString();
            zs.Append("123").Append(zs);
            Assert.That("123123", Is.EqualTo(zs.ToString()), "#1");
            zs.Append(zs.ToString());
            Assert.That("123123123123", Is.EqualTo(zs.ToString()), "#2");
        }
    }
}
