#region Usings

using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

#endregion

namespace ZFormat
{
    [TestFixture]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public unsafe class StackBufferTest
    {
        [Test]
        public void Init01()
        {
            char* b = stackalloc char[16];
            var sb = new StackBuffer(b, 16, 1);
            Assert.That(sb.Length == 0, "#1");
            sb.Append('8');
            Assert.That(sb.Length == 1, "#2");
            sb.Prepend('7');
            Assert.That(sb.Length == 2, "#3");
            var str1 = sb.DebugDump();
            Assert.That(str1 == "78");
            Assert.Throws<Exception>(() => sb.Prepend('?'), "#4");
            var appCap = sb.AppendCapacity;
            for (var i = 0; i < appCap; i++)
            {
                sb.Append((char) ('A' + i));
            }
            var str2 = sb.DebugDump();
            Assert.That(str2, Is.EqualTo("78ABCDEFGHIJKLMN"), "#5");
            Assert.Throws<Exception>(() => sb.Append('?'), "#6");

            for (var i = 0; i < sb.Length; i++)
            {
                Assert.That(str2[i], Is.EqualTo(*(sb.Begin + i)), "iter#" + i);
            }

            var idx = 0;
            for (var p = sb.Begin; p < sb.End; ++p)
            {
                Assert.That(str2[idx++], Is.EqualTo(*p), "iter#" + idx);
            }
        }

        [Test]
        public void Appends()
        {
            char* b = stackalloc char[StackBuffer.STACK_BUFFER_CHARS];
            var sb = new StackBuffer(b, StackBuffer.STACK_BUFFER_CHARS);
            sb.Prepend('-');
            sb.Append("000");
            sb.Append('.');
            sb.Append("12345".ToCharArray());
            Assert.That(sb.Length, Is.EqualTo(10), "#1");
            Assert.That(sb.DebugDump(), Is.EqualTo("-000.12345"), "#2");
        }

        [Test]
        public void Capacity()
        {
            char* b = stackalloc char[StackBuffer.STACK_BUFFER_CHARS];
            var sb = new StackBuffer(b, StackBuffer.STACK_BUFFER_CHARS, 8);
            Assert.That(sb.PrependCapacity, Is.EqualTo(8), "#1");
            Assert.That(sb.AppendCapacity, Is.EqualTo(StackBuffer.STACK_BUFFER_CHARS - 8), "#2");
            sb.Prepend('-');
            sb.Append("123456789");
            Assert.That(sb.PrependCapacity, Is.EqualTo(8 - 1), "#3");
            Assert.That(sb.AppendCapacity, Is.EqualTo(StackBuffer.STACK_BUFFER_CHARS - 8 - 9), "#4");
        }

        [Test]
        public void Prepends()
        {
            char* b = stackalloc char[StackBuffer.STACK_BUFFER_CHARS];
            var sb = new StackBuffer(b, StackBuffer.STACK_BUFFER_CHARS, 8);
            sb.Prepend("ABC");
            sb.Prepend('-');
            Assert.That(sb.DebugDump(), Is.EqualTo("-ABC"));
            var rest = sb.PrependCapacity;
            for (var i = 0; i < rest; i++)
            {
                sb.Prepend((char) ('0' + i));
            }
            Assert.That(sb.DebugDump(), Is.EqualTo("3210-ABC"));
        }

        [Test]
        public void AppendRepeat()
        {
            char* b = stackalloc char[16];
            var sb = new StackBuffer(b, 16, 1);
            Assert.That(sb.Length == 0, "#1");
            sb.Append('0', 15);
            Assert.That(sb.Length == 15, "#1");
            Assert.That(sb.DebugDump(), Is.EqualTo("000000000000000"), "#4");
            Assert.Throws<Exception>(() => sb.Append('?'), "#3");
        }

        [Test]
        [SuppressMessage("ReSharper", "UnusedVariable")]
        public void LengthAndIndexer()
        {
            char* b = stackalloc char[16];
            var sb = new StackBuffer(b, 16, 1);
            Assert.That(sb.Length == 0, "#1");
            sb.Prepend('-');
            sb.Append("123456789012345");
            Assert.That(sb.Length, Is.EqualTo(16), "#2");
            Assert.That(sb.DebugDump(), Is.EqualTo("-123456789012345"), "#3");
            Assert.Throws<Exception>(() => sb.Append("badbadbad"), "#4");
            sb.Length = 1;
            Assert.That(sb.DebugDump(), Is.EqualTo("-"), "#5");
            sb.Length = 2;
            Assert.That(sb.DebugDump(), Is.EqualTo("-1"), "#6");
            Assert.Throws<ArgumentOutOfRangeException>(() => sb.Length = 17, "#7");
            sb.Length = 16;
            for (var i = 0; i < sb.Length; i++)
            {
                sb[i] = (char) (sb[i] + 1);
            }
            Assert.That(sb.DebugDump(), Is.EqualTo(".23456789:123456"), "#7");
            var idx = 0;
            while (idx < sb.Length)
            {
                sb[idx++] = (char) ('A' + idx);
            }
            Assert.That(sb.DebugDump(), Is.EqualTo("BCDEFGHIJKLMNOPQ"), "#8");
            Assert.Throws<ArgumentOutOfRangeException>(() => { var _ = sb[16]; }, "#9");
            Assert.Throws<ArgumentOutOfRangeException>(() => { sb[16] = '?'; }, "10");
            Assert.Throws<ArgumentOutOfRangeException>(() => { var _ = sb[-1]; }, "#11");
            Assert.Throws<ArgumentOutOfRangeException>(() => { sb[-1] = '?'; }, "12");
        }

        [Test]
        public void PrependsCount()
        {
            char* b = stackalloc char[16];
            var sb = new StackBuffer(b, 16, 8);
            sb.Append('1');
            sb.Prepend('0', 8);
            Assert.That(sb.DebugDump(), Is.EqualTo("000000001"), "#1");
            Assert.That(sb.PrependCapacity, Is.EqualTo(0), "#2");
            Assert.Throws<Exception>(() => sb.Prepend('?', 1), "#3");
        }

        [Test]
        public void Trims()
        {
            char* b = stackalloc char[16];
            var sb = new StackBuffer(b, 16, 8);
            sb.Append("12345678");
            sb.Prepend("ABCDEFGH");
            Assert.That(sb.DebugDump(), Is.EqualTo("ABCDEFGH12345678"), "#1");
            sb.TrimLeft(4);
            Assert.That(sb.DebugDump(), Is.EqualTo("EFGH12345678"), "#2");
            sb.TrimRight(4);
            Assert.That(sb.DebugDump(), Is.EqualTo("EFGH1234"), "#3");
        }
    }
}
