//
// MonoTests.System.NumberFormatterTest
//
// Authors: 
//    akiramei (mei@work.email.ne.jp)
//
// (C) 2005 akiramei
//

#region Usings

using System;
using System.Globalization;
using System.Threading;
using NUnit.Framework;

#endregion

namespace ZFormat
{
    [TestFixture]
    public class NumberFormatterTest
    {
        private CultureInfo old_culture;
        private NumberFormatInfo _nfi;

        [SetUp]
        public void SetUp()
        {
            old_culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);
            _nfi = NumberFormatInfo.InvariantInfo.Clone() as NumberFormatInfo;
        }

        [TearDown]
        public void TearDown()
        {
            Thread.CurrentThread.CurrentCulture = old_culture;
        }

        [Test]
        public void FastTrack()
        {
            Assert.AreEqual("0", 0.Fmt(_nfi), "#01");
            Assert.AreEqual("10000", 10000.Fmt(_nfi), "#02");
            Assert.AreEqual("-10000", (-10000).Fmt(_nfi), "#03");
            Assert.AreEqual("0", 0u.Fmt(_nfi), "#04");

            Assert.AreEqual("10000", 10000U.Fmt(_nfi), "#05");
            Assert.AreEqual("10000000", (10000000U).Fmt(_nfi), "#06");

            Assert.AreEqual("0", (-0.0).Fmt(_nfi), "#07");
            Assert.AreEqual("0", (0.0).Fmt(_nfi), "#08");
            Assert.AreEqual("-10.1", (-10.10).Fmt(_nfi), "#09");
            Assert.AreEqual("100.1001", (100.10010).Fmt(_nfi), "#10");

            Assert.AreEqual("0", (-0.0F).Fmt(_nfi), "#11");
            Assert.AreEqual("0", (0.0F).Fmt(_nfi), "#12");
            Assert.AreEqual("-10.1", (-10.10F).Fmt(_nfi), "#13");
            Assert.AreEqual("100.1001", (100.10010F).Fmt(_nfi), "#14");

            Assert.AreEqual("0", 0UL.Fmt(_nfi), "#15");
            Assert.AreEqual("1000", 1000UL.Fmt(_nfi), "#16");
            Assert.AreEqual("19990001", 19990001UL.Fmt(_nfi), "#17");

            Assert.AreEqual("0", 0L.Fmt(_nfi), "#18");
            Assert.AreEqual("1000", 1000L.Fmt(_nfi), "#19");
            Assert.AreEqual("19990001", 19990001L.Fmt(_nfi), "#20");
            Assert.AreEqual("-1000", (-1000L).Fmt(_nfi), "#21");
            Assert.AreEqual("-19990001", (-19990001L).Fmt(_nfi), "#22");
            Assert.AreEqual("1000", 1000L.Fmt(_nfi), "#23");
            Assert.AreEqual("19990001", 19990001L.Fmt(_nfi), "#24");
        }



        // Test00000- Int32 and D
        [Test]
        public void Test00000()
        {
            Assert.AreEqual("0", 0.Fmt("D", _nfi), "#01");
            Assert.AreEqual("0", 0.Fmt("d", _nfi), "#02");
            Assert.AreEqual("-2147483648", int.MinValue.ToString("D", _nfi), "#03");
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("d", _nfi), "#04");
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("D", _nfi), "#05");
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("d", _nfi), "#06");
        }

        [Test]
        public void Test00001()
        {
            Assert.AreEqual("D ", 0.Fmt("D ", _nfi), "#01");
            Assert.AreEqual(" D", 0.Fmt(" D", _nfi), "#02");
            Assert.AreEqual(" D ", 0.Fmt(" D ", _nfi), "#03");
        }

        [Test]
        public void Test00002()
        {
            Assert.AreEqual("-D ", (-1).Fmt("D ", _nfi), "#01");
            Assert.AreEqual("- D", (-1).Fmt(" D", _nfi), "#02");
            Assert.AreEqual("- D ", (-1).Fmt(" D ", _nfi), "#03");
        }

        [Test]
        public void Test00003()
        {
            Assert.AreEqual("0", 0.Fmt("D0", _nfi), "#01");
            Assert.AreEqual("0000000000", 0.Fmt("D10", _nfi), "#02");
            Assert.AreEqual("00000000000", 0.Fmt("D11", _nfi), "#03");
            Assert.AreEqual(
                "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                0.Fmt("D99", _nfi), "#04");
            Assert.AreEqual("D100", 0.Fmt("D100", _nfi), "#05");
        }

        [Test]
        public void Test00004()
        {
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("D0", _nfi), "#01");
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("D10", _nfi), "#02");
            Assert.AreEqual("02147483647", int.MaxValue.Fmt("D11", _nfi), "#03");
            Assert.AreEqual(
                "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002147483647",
                int.MaxValue.Fmt("D99", _nfi), "#04");
            Assert.AreEqual("D12147483647", int.MaxValue.Fmt("D100", _nfi), "#05");
        }

        [Test]
        public void Test00005()
        {
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("D0", _nfi), "#01");
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("D10", _nfi), "#02");
            Assert.AreEqual("-02147483648", int.MinValue.Fmt("D11", _nfi), "#03");
            Assert.AreEqual(
                "-000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002147483648",
                int.MinValue.Fmt("D99", _nfi), "#04");
            Assert.AreEqual("-D12147483648", int.MinValue.Fmt("D100", _nfi), "#05");
        }

        [Test]
        public void Test00006()
        {
            Assert.AreEqual("DF", 0.Fmt("DF", _nfi), "#01");
            Assert.AreEqual("D0F", 0.Fmt("D0F", _nfi), "#02");
            Assert.AreEqual("D0xF", 0.Fmt("D0xF", _nfi), "#03");
        }

        [Test]
        public void Test00007()
        {
            Assert.AreEqual("DF", int.MaxValue.Fmt("DF", _nfi), "#01");
            Assert.AreEqual("D2147483647F", int.MaxValue.Fmt("D0F", _nfi), "#02");
            Assert.AreEqual("D2147483647xF", int.MaxValue.Fmt("D0xF", _nfi), "#03");
        }

        [Test]
        public void Test00008()
        {
            Assert.AreEqual("-DF", int.MinValue.Fmt("DF", _nfi), "#01");
            Assert.AreEqual("-D2147483648F", int.MinValue.Fmt("D0F", _nfi), "#02");
            Assert.AreEqual("-D2147483648xF", int.MinValue.Fmt("D0xF", _nfi), "#03");
        }

        [Test]
        public void Test00009()
        {
            Assert.AreEqual("00000000000", 0.Fmt("D0000000000000000000000000000000000000011", _nfi), "#01");
            Assert.AreEqual("02147483647", int.MaxValue.Fmt("D0000000000000000000000000000000000000011", _nfi), "#02");
            Assert.AreEqual("-02147483648", int.MinValue.Fmt("D0000000000000000000000000000000000000011", _nfi), "#03");
        }

        [Test]
        public void Test00010()
        {
            Assert.AreEqual("+D", 0.Fmt("+D", _nfi), "#01");
            Assert.AreEqual("D+", 0.Fmt("D+", _nfi), "#02");
            Assert.AreEqual("+D+", 0.Fmt("+D+", _nfi), "#03");
        }

        [Test]
        public void Test00011()
        {
            Assert.AreEqual("+D", int.MaxValue.Fmt("+D", _nfi), "#01");
            Assert.AreEqual("D+", int.MaxValue.Fmt("D+", _nfi), "#02");
            Assert.AreEqual("+D+", int.MaxValue.Fmt("+D+", _nfi), "#03");
        }

        [Test]
        public void Test00012()
        {
            Assert.AreEqual("-+D", int.MinValue.Fmt("+D", _nfi), "#01");
            Assert.AreEqual("-D+", int.MinValue.Fmt("D+", _nfi), "#02");
            Assert.AreEqual("-+D+", int.MinValue.Fmt("+D+", _nfi), "#03");
        }

        [Test]
        public void Test00013()
        {
            Assert.AreEqual("-D", 0.Fmt("-D", _nfi), "#01");
            Assert.AreEqual("D-", 0.Fmt("D-", _nfi), "#02");
            Assert.AreEqual("-D-", 0.Fmt("-D-", _nfi), "#03");
        }

        [Test]
        public void Test00014()
        {
            Assert.AreEqual("-D", int.MaxValue.Fmt("-D", _nfi), "#01");
            Assert.AreEqual("D-", int.MaxValue.Fmt("D-", _nfi), "#02");
            Assert.AreEqual("-D-", int.MaxValue.Fmt("-D-", _nfi), "#03");
        }

        [Test]
        public void Test00015()
        {
            Assert.AreEqual("--D", int.MinValue.Fmt("-D", _nfi), "#01");
            Assert.AreEqual("-D-", int.MinValue.Fmt("D-", _nfi), "#02");
            Assert.AreEqual("--D-", int.MinValue.Fmt("-D-", _nfi), "#03");
        }

        [Test]
        public void Test00016()
        {
            Assert.AreEqual("D+0", 0.Fmt("D+0", _nfi), "#01");
            Assert.AreEqual("D+2147483647", int.MaxValue.Fmt("D+0", _nfi), "#02");
            Assert.AreEqual("-D+2147483648", int.MinValue.Fmt("D+0", _nfi), "#03");
        }

        [Test]
        public void Test00017()
        {
            Assert.AreEqual("D+9", 0.Fmt("D+9", _nfi), "#01");
            Assert.AreEqual("D+9", int.MaxValue.Fmt("D+9", _nfi), "#02");
            Assert.AreEqual("-D+9", int.MinValue.Fmt("D+9", _nfi), "#03");
        }

        [Test]
        public void Test00018()
        {
            Assert.AreEqual("D-9", 0.Fmt("D-9", _nfi), "#01");
            Assert.AreEqual("D-9", int.MaxValue.Fmt("D-9", _nfi), "#02");
            Assert.AreEqual("-D-9", int.MinValue.Fmt("D-9", _nfi), "#03");
        }

        [Test]
        public void Test00019()
        {
            Assert.AreEqual("D0", 0.Fmt("D0,", _nfi), "#01");
            Assert.AreEqual("D2147484", int.MaxValue.Fmt("D0,", _nfi), "#02");
            Assert.AreEqual("-D2147484", int.MinValue.Fmt("D0,", _nfi), "#03");
        }

        [Test]
        public void Test00020()
        {
            Assert.AreEqual("D0", 0.Fmt("D0.", _nfi), "#01");
            Assert.AreEqual("D2147483647", int.MaxValue.Fmt("D0.", _nfi), "#02");
            Assert.AreEqual("-D2147483648", int.MinValue.Fmt("D0.", _nfi), "#03");
        }

        [Test]
        public void Test00021()
        {
            Assert.AreEqual("D0.0", 0.Fmt("D0.0", _nfi), "#01");
            Assert.AreEqual("D2147483647.0", int.MaxValue.Fmt("D0.0", _nfi), "#02");
            Assert.AreEqual("-D2147483648.0", int.MinValue.Fmt("D0.0", _nfi), "#03");
        }

        [Test]
        public void Test00022()
        {
            Assert.AreEqual("D09", 0.Fmt("D0.9", _nfi), "#01");
            Assert.AreEqual("D21474836479", int.MaxValue.Fmt("D0.9", _nfi), "#02");
            Assert.AreEqual("-D21474836489", int.MinValue.Fmt("D0.9", _nfi), "#03");
        }

        // Test01000- Int32 and E
        [Test]
        public void Test01000()
        {
            Assert.AreEqual("0.000000E+000", 0.Fmt("E", _nfi), "#01");
            Assert.AreEqual("0.000000e+000", 0.Fmt("e", _nfi), "#02");
            Assert.AreEqual("-2.147484E+009", int.MinValue.Fmt("E", _nfi), "#03");
            Assert.AreEqual("-2.147484e+009", int.MinValue.Fmt("e", _nfi), "#04");
            Assert.AreEqual("2.147484E+009", int.MaxValue.Fmt("E", _nfi), "#05");
            Assert.AreEqual("2.147484e+009", int.MaxValue.Fmt("e", _nfi), "#06");
        }

        [Test]
        public void Test01001()
        {
            Assert.AreEqual("E ", 0.Fmt("E ", _nfi), "#01");
            Assert.AreEqual(" E", 0.Fmt(" E", _nfi), "#02");
            Assert.AreEqual(" E ", 0.Fmt(" E ", _nfi), "#03");
        }

        [Test]
        public void Test01002()
        {
            Assert.AreEqual("-E ", (-1).Fmt("E ", _nfi), "#01");
            Assert.AreEqual("- E", (-1).Fmt(" E", _nfi), "#02");
            Assert.AreEqual("- E ", (-1).Fmt(" E ", _nfi), "#03");
        }

        [Test]
        public void Test01003()
        {
            Assert.AreEqual("0E+000", 0.Fmt("E0", _nfi), "#01");
            Assert.AreEqual("0.000000000E+000", 0.Fmt("E9", _nfi), "#02");
            Assert.AreEqual("0.0000000000E+000", 0.Fmt("E10", _nfi), "#03");
            Assert.AreEqual(
                "0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000E+000",
                0.Fmt("E99", _nfi), "#04");
            Assert.AreEqual("E100", 0.Fmt("E100", _nfi), "#05");
        }

        [Test]
        public void Test01004()
        {
            Assert.AreEqual("2E+009", int.MaxValue.Fmt("E0", _nfi), "#01");
            Assert.AreEqual("2.147483647E+009", int.MaxValue.Fmt("E9", _nfi), "#02");
            Assert.AreEqual("2.1474836470E+009", int.MaxValue.Fmt("E10", _nfi), "#03");
            Assert.AreEqual(
                "2.147483647000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000E+009",
                int.MaxValue.Fmt("E99", _nfi), "#04");
            Assert.AreEqual("E12147483647", int.MaxValue.Fmt("E100", _nfi), "#05");
        }

        [Test]
        public void Test01005()
        {
            Assert.AreEqual("-2E+009", int.MinValue.Fmt("E0", _nfi), "#01");
            Assert.AreEqual("-2.147483648E+009", int.MinValue.Fmt("E9", _nfi), "#02");
            Assert.AreEqual("-2.1474836480E+009", int.MinValue.Fmt("E10", _nfi), "#03");
            Assert.AreEqual(
                "-2.147483648000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000E+009",
                int.MinValue.Fmt("E99", _nfi), "#04");
            Assert.AreEqual("-E12147483648", int.MinValue.Fmt("E100", _nfi), "#05");
        }

        [Test]
        public void Test01006()
        {
            Assert.AreEqual("EF", 0.Fmt("EF", _nfi), "#01");
            Assert.AreEqual("E0F", 0.Fmt("E0F", _nfi), "#02");
            Assert.AreEqual("E0xF", 0.Fmt("E0xF", _nfi), "#03");
        }

        [Test]
        public void Test01007()
        {
            Assert.AreEqual("EF", int.MaxValue.Fmt("EF", _nfi), "#01");
            Assert.AreEqual("E0F", int.MaxValue.Fmt("E0F", _nfi), "#02");
            Assert.AreEqual("E0xF", int.MaxValue.Fmt("E0xF", _nfi), "#03");
        }

        [Test]
        public void Test01008()
        {
            Assert.AreEqual("-EF", int.MinValue.Fmt("EF", _nfi), "#01");
            Assert.AreEqual("E0F", int.MinValue.Fmt("E0F", _nfi), "#02");
            Assert.AreEqual("E0xF", int.MinValue.Fmt("E0xF", _nfi), "#03");
        }

        [Test]
        public void Test01009()
        {
            Assert.AreEqual("0.0000000000E+000", 0.Fmt("E0000000000000000000000000000000000000010", _nfi), "#01");
            Assert.AreEqual("2.1474836470E+009", int.MaxValue.Fmt("E0000000000000000000000000000000000000010", _nfi),
                "#02");
            Assert.AreEqual("-2.1474836480E+009", int.MinValue.Fmt("E0000000000000000000000000000000000000010", _nfi),
                "#03");
        }

        [Test]
        public void Test01010()
        {
            Assert.AreEqual("+E", 0.Fmt("+E", _nfi), "#01");
            Assert.AreEqual("E+", 0.Fmt("E+", _nfi), "#02");
            Assert.AreEqual("+E+", 0.Fmt("+E+", _nfi), "#03");
        }

        [Test]
        public void Test01011()
        {
            Assert.AreEqual("+E", int.MaxValue.Fmt("+E", _nfi), "#01");
            Assert.AreEqual("E+", int.MaxValue.Fmt("E+", _nfi), "#02");
            Assert.AreEqual("+E+", int.MaxValue.Fmt("+E+", _nfi), "#03");
        }

        [Test]
        public void Test01012()
        {
            Assert.AreEqual("-+E", int.MinValue.Fmt("+E", _nfi), "#01");
            Assert.AreEqual("-E+", int.MinValue.Fmt("E+", _nfi), "#02");
            Assert.AreEqual("-+E+", int.MinValue.Fmt("+E+", _nfi), "#03");
        }

        [Test]
        public void Test01013()
        {
            Assert.AreEqual("-E", 0.Fmt("-E", _nfi), "#01");
            Assert.AreEqual("E-", 0.Fmt("E-", _nfi), "#02");
            Assert.AreEqual("-E-", 0.Fmt("-E-", _nfi), "#03");
        }

        [Test]
        public void Test01014()
        {
            Assert.AreEqual("-E", int.MaxValue.Fmt("-E", _nfi), "#01");
            Assert.AreEqual("E-", int.MaxValue.Fmt("E-", _nfi), "#02");
            Assert.AreEqual("-E-", int.MaxValue.Fmt("-E-", _nfi), "#03");
        }

        [Test]
        public void Test01015()
        {
            Assert.AreEqual("--E", int.MinValue.Fmt("-E", _nfi), "#01");
            Assert.AreEqual("-E-", int.MinValue.Fmt("E-", _nfi), "#02");
            Assert.AreEqual("--E-", int.MinValue.Fmt("-E-", _nfi), "#03");
        }

        [Test]
        public void Test01016()
        {
            Assert.AreEqual("E+0", 0.Fmt("E+0", _nfi), "#01");
            Assert.AreEqual("E+0", int.MaxValue.Fmt("E+0", _nfi), "#02");
            Assert.AreEqual("E+0", int.MinValue.Fmt("E+0", _nfi), "#03");
        }

        [Test]
        public void Test01017()
        {
            Assert.AreEqual("E+9", 0.Fmt("E+9", _nfi), "#01");
            Assert.AreEqual("E+9", int.MaxValue.Fmt("E+9", _nfi), "#02");
            Assert.AreEqual("-E+9", int.MinValue.Fmt("E+9", _nfi), "#03");
        }

        [Test]
        public void Test01018()
        {
            Assert.AreEqual("E-9", 0.Fmt("E-9", _nfi), "#01");
            Assert.AreEqual("E-9", int.MaxValue.Fmt("E-9", _nfi), "#02");
            Assert.AreEqual("-E-9", int.MinValue.Fmt("E-9", _nfi), "#03");
        }

        [Test]
        public void Test01019()
        {
            Assert.AreEqual("E0", 0.Fmt("E0,", _nfi), "#01");
            Assert.AreEqual("E0", int.MaxValue.Fmt("E0,", _nfi), "#02");
            Assert.AreEqual("E0", int.MinValue.Fmt("E0,", _nfi), "#03");
        }

        [Test]
        public void Test01020()
        {
            Assert.AreEqual("E0", 0.Fmt("E0.", _nfi), "#01");
            Assert.AreEqual("E0", int.MaxValue.Fmt("E0.", _nfi), "#02");
            Assert.AreEqual("E0", int.MinValue.Fmt("E0.", _nfi), "#03");
        }

        [Test]
        public void Test01021()
        {
            Assert.AreEqual("E0.0", 0.Fmt("E0.0", _nfi), "#01");
            Assert.AreEqual("E10.2", int.MaxValue.Fmt("E0.0", _nfi), "#02");
            Assert.AreEqual("-E10.2", int.MinValue.Fmt("E0.0", _nfi), "#03");
        }

        [Test]
        public void Test01022()
        {
            Assert.AreEqual("E09", 0.Fmt("E0.9", _nfi), "#01");
            Assert.AreEqual("E09", int.MaxValue.Fmt("E0.9", _nfi), "#02");
            Assert.AreEqual("E09", int.MinValue.Fmt("E0.9", _nfi), "#03");
        }

        [Test]
        public void Test01023()
        {
            Assert.AreEqual("9.999999E+007", 99999990.Fmt("E", _nfi), "#01");
            Assert.AreEqual("9.999999E+007", 99999991.Fmt("E", _nfi), "#02");
            Assert.AreEqual("9.999999E+007", 99999992.Fmt("E", _nfi), "#03");
            Assert.AreEqual("9.999999E+007", 99999993.Fmt("E", _nfi), "#04");
            Assert.AreEqual("9.999999E+007", 99999994.Fmt("E", _nfi), "#05");
            Assert.AreEqual("1.000000E+008", 99999995.Fmt("E", _nfi), "#06");
            Assert.AreEqual("1.000000E+008", 99999996.Fmt("E", _nfi), "#07");
            Assert.AreEqual("1.000000E+008", 99999997.Fmt("E", _nfi), "#08");
            Assert.AreEqual("1.000000E+008", 99999998.Fmt("E", _nfi), "#09");
            Assert.AreEqual("1.000000E+008", 99999999.Fmt("E", _nfi), "#10");
        }

        [Test]
        public void Test01024()
        {
            Assert.AreEqual("-9.999999E+007", (-99999990).Fmt("E", _nfi), "#01");
            Assert.AreEqual("-9.999999E+007", (-99999991).Fmt("E", _nfi), "#02");
            Assert.AreEqual("-9.999999E+007", (-99999992).Fmt("E", _nfi), "#03");
            Assert.AreEqual("-9.999999E+007", (-99999993).Fmt("E", _nfi), "#04");
            Assert.AreEqual("-9.999999E+007", (-99999994).Fmt("E", _nfi), "#05");
            Assert.AreEqual("-1.000000E+008", (-99999995).Fmt("E", _nfi), "#06");
            Assert.AreEqual("-1.000000E+008", (-99999996).Fmt("E", _nfi), "#07");
            Assert.AreEqual("-1.000000E+008", (-99999997).Fmt("E", _nfi), "#08");
            Assert.AreEqual("-1.000000E+008", (-99999998).Fmt("E", _nfi), "#09");
            Assert.AreEqual("-1.000000E+008", (-99999999).Fmt("E", _nfi), "#10");
        }

        [Test]
        public void Test01025()
        {
            Assert.AreEqual("9.999998E+007", 99999980.Fmt("E", _nfi), "#01");
            Assert.AreEqual("9.999998E+007", 99999981.Fmt("E", _nfi), "#02");
            Assert.AreEqual("9.999998E+007", 99999982.Fmt("E", _nfi), "#03");
            Assert.AreEqual("9.999998E+007", 99999983.Fmt("E", _nfi), "#04");
            Assert.AreEqual("9.999998E+007", 99999984.Fmt("E", _nfi), "#05");
            Assert.AreEqual("9.999999E+007", 99999985.Fmt("E", _nfi), "#06");
            Assert.AreEqual("9.999999E+007", 99999986.Fmt("E", _nfi), "#07");
            Assert.AreEqual("9.999999E+007", 99999987.Fmt("E", _nfi), "#08");
            Assert.AreEqual("9.999999E+007", 99999988.Fmt("E", _nfi), "#09");
            Assert.AreEqual("9.999999E+007", 99999989.Fmt("E", _nfi), "#10");
        }

        [Test]
        public void Test01026()
        {
            Assert.AreEqual("-9.999998E+007", (-99999980).Fmt("E", _nfi), "#01");
            Assert.AreEqual("-9.999998E+007", (-99999981).Fmt("E", _nfi), "#02");
            Assert.AreEqual("-9.999998E+007", (-99999982).Fmt("E", _nfi), "#03");
            Assert.AreEqual("-9.999998E+007", (-99999983).Fmt("E", _nfi), "#04");
            Assert.AreEqual("-9.999998E+007", (-99999984).Fmt("E", _nfi), "#05");
            Assert.AreEqual("-9.999999E+007", (-99999985).Fmt("E", _nfi), "#06");
            Assert.AreEqual("-9.999999E+007", (-99999986).Fmt("E", _nfi), "#07");
            Assert.AreEqual("-9.999999E+007", (-99999987).Fmt("E", _nfi), "#08");
            Assert.AreEqual("-9.999999E+007", (-99999988).Fmt("E", _nfi), "#09");
            Assert.AreEqual("-9.999999E+007", (-99999989).Fmt("E", _nfi), "#10");
        }

        [Test]
        public void Test01027()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberDecimalSeparator = "#";
            Assert.AreEqual("-1#000000E+008", (-99999999).Fmt("E", nfi), "#01");
        }

        [Test]
        public void Test01028()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "+";
            nfi.PositiveSign = "-";

            Assert.AreEqual("1.000000E-000", 1.Fmt("E", nfi), "#01");
            Assert.AreEqual("0.000000E-000", 0.Fmt("E", nfi), "#02");
            Assert.AreEqual("+1.000000E-000", (-1).Fmt("E", nfi), "#03");
        }

        // Test02000- Int32 and F
        [Test]
        public void Test02000()
        {
            Assert.AreEqual("0.00", 0.Fmt("F", _nfi), "#01");
            Assert.AreEqual("0.00", 0.Fmt("f", _nfi), "#02");
            Assert.AreEqual("-2147483648.00", int.MinValue.Fmt("F", _nfi), "#03");
            Assert.AreEqual("-2147483648.00", int.MinValue.Fmt("f", _nfi), "#04");
            Assert.AreEqual("2147483647.00", int.MaxValue.Fmt("F", _nfi), "#05");
            Assert.AreEqual("2147483647.00", int.MaxValue.Fmt("f", _nfi), "#06");
        }

        [Test]
        public void Test02001()
        {
            Assert.AreEqual("F ", 0.Fmt("F ", _nfi), "#01");
            Assert.AreEqual(" F", 0.Fmt(" F", _nfi), "#02");
            Assert.AreEqual(" F ", 0.Fmt(" F ", _nfi), "#03");
        }

        [Test]
        public void Test02002()
        {
            Assert.AreEqual("-F ", (-1).Fmt("F ", _nfi), "#01");
            Assert.AreEqual("- F", (-1).Fmt(" F", _nfi), "#02");
            Assert.AreEqual("- F ", (-1).Fmt(" F ", _nfi), "#03");
        }

        [Test]
        public void Test02003()
        {
            Assert.AreEqual("0", 0.Fmt("F0", _nfi), "#01");
            Assert.AreEqual("0.000000000", 0.Fmt("F9", _nfi), "#02");
            Assert.AreEqual("0.0000000000", 0.Fmt("F10", _nfi), "#03");
            Assert.AreEqual(
                "0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                0.Fmt("F99", _nfi), "#04");
            Assert.AreEqual("F100", 0.Fmt("F100", _nfi), "#05");
        }

        [Test]
        public void Test02004()
        {
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("F0", _nfi), "#01");
            Assert.AreEqual("2147483647.000000000", int.MaxValue.Fmt("F9", _nfi), "#02");
            Assert.AreEqual("2147483647.0000000000", int.MaxValue.Fmt("F10", _nfi), "#03");
            Assert.AreEqual(
                "2147483647.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                int.MaxValue.Fmt("F99", _nfi), "#04");
            Assert.AreEqual("F12147483647", int.MaxValue.Fmt("F100", _nfi), "#05");
        }

        [Test]
        public void Test02005()
        {
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("F0", _nfi), "#01");
            Assert.AreEqual("-2147483648.000000000", int.MinValue.Fmt("F9", _nfi), "#02");
            Assert.AreEqual("-2147483648.0000000000", int.MinValue.Fmt("F10", _nfi), "#03");
            Assert.AreEqual(
                "-2147483648.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                int.MinValue.Fmt("F99", _nfi), "#04");
            Assert.AreEqual("-F12147483648", int.MinValue.Fmt("F100", _nfi), "#05");
        }

        [Test]
        public void Test02006()
        {
            Assert.AreEqual("FF", 0.Fmt("FF", _nfi), "#01");
            Assert.AreEqual("F0F", 0.Fmt("F0F", _nfi), "#02");
            Assert.AreEqual("F0xF", 0.Fmt("F0xF", _nfi), "#03");
        }

        [Test]
        public void Test02007()
        {
            Assert.AreEqual("FF", int.MaxValue.Fmt("FF", _nfi), "#01");
            Assert.AreEqual("F2147483647F", int.MaxValue.Fmt("F0F", _nfi), "#02");
            Assert.AreEqual("F2147483647xF", int.MaxValue.Fmt("F0xF", _nfi), "#03");
        }

        [Test]
        public void Test02008()
        {
            Assert.AreEqual("-FF", int.MinValue.Fmt("FF", _nfi), "#01");
            Assert.AreEqual("-F2147483648F", int.MinValue.Fmt("F0F", _nfi), "#02");
            Assert.AreEqual("-F2147483648xF", int.MinValue.Fmt("F0xF", _nfi), "#03");
        }

        [Test]
        public void Test02009()
        {
            Assert.AreEqual("0.0000000000", 0.Fmt("F0000000000000000000000000000000000000010", _nfi), "#01");
            Assert.AreEqual("2147483647.0000000000", int.MaxValue.Fmt("F0000000000000000000000000000000000000010", _nfi),
                "#02");
            Assert.AreEqual("-2147483648.0000000000",
                int.MinValue.Fmt("F0000000000000000000000000000000000000010", _nfi), "#03");
        }

        [Test]
        public void Test02010()
        {
            Assert.AreEqual("+F", 0.Fmt("+F", _nfi), "#01");
            Assert.AreEqual("F+", 0.Fmt("F+", _nfi), "#02");
            Assert.AreEqual("+F+", 0.Fmt("+F+", _nfi), "#03");
        }

        [Test]
        public void Test02011()
        {
            Assert.AreEqual("+F", int.MaxValue.Fmt("+F", _nfi), "#01");
            Assert.AreEqual("F+", int.MaxValue.Fmt("F+", _nfi), "#02");
            Assert.AreEqual("+F+", int.MaxValue.Fmt("+F+", _nfi), "#03");
        }

        [Test]
        public void Test02012()
        {
            Assert.AreEqual("-+F", int.MinValue.Fmt("+F", _nfi), "#01");
            Assert.AreEqual("-F+", int.MinValue.Fmt("F+", _nfi), "#02");
            Assert.AreEqual("-+F+", int.MinValue.Fmt("+F+", _nfi), "#03");
        }

        [Test]
        public void Test02013()
        {
            Assert.AreEqual("-F", 0.Fmt("-F", _nfi), "#01");
            Assert.AreEqual("F-", 0.Fmt("F-", _nfi), "#02");
            Assert.AreEqual("-F-", 0.Fmt("-F-", _nfi), "#03");
        }

        [Test]
        public void Test02014()
        {
            Assert.AreEqual("-F", int.MaxValue.Fmt("-F", _nfi), "#01");
            Assert.AreEqual("F-", int.MaxValue.Fmt("F-", _nfi), "#02");
            Assert.AreEqual("-F-", int.MaxValue.Fmt("-F-", _nfi), "#03");
        }

        [Test]
        public void Test02015()
        {
            Assert.AreEqual("--F", int.MinValue.Fmt("-F", _nfi), "#01");
            Assert.AreEqual("-F-", int.MinValue.Fmt("F-", _nfi), "#02");
            Assert.AreEqual("--F-", int.MinValue.Fmt("-F-", _nfi), "#03");
        }

        [Test]
        public void Test02016()
        {
            Assert.AreEqual("F+0", 0.Fmt("F+0", _nfi), "#01");
            Assert.AreEqual("F+2147483647", int.MaxValue.Fmt("F+0", _nfi), "#02");
            Assert.AreEqual("-F+2147483648", int.MinValue.Fmt("F+0", _nfi), "#03");
        }

        [Test]
        public void Test02017()
        {
            Assert.AreEqual("F+9", 0.Fmt("F+9", _nfi), "#01");
            Assert.AreEqual("F+9", int.MaxValue.Fmt("F+9", _nfi), "#02");
            Assert.AreEqual("-F+9", int.MinValue.Fmt("F+9", _nfi), "#03");
        }

        [Test]
        public void Test02018()
        {
            Assert.AreEqual("F-9", 0.Fmt("F-9", _nfi), "#01");
            Assert.AreEqual("F-9", int.MaxValue.Fmt("F-9", _nfi), "#02");
            Assert.AreEqual("-F-9", int.MinValue.Fmt("F-9", _nfi), "#03");
        }

        [Test]
        public void Test02019()
        {
            Assert.AreEqual("F0", 0.Fmt("F0,", _nfi), "#01");
            Assert.AreEqual("F2147484", int.MaxValue.Fmt("F0,", _nfi), "#02");
            Assert.AreEqual("-F2147484", int.MinValue.Fmt("F0,", _nfi), "#03");
        }

        [Test]
        public void Test02020()
        {
            Assert.AreEqual("F0", 0.Fmt("F0.", _nfi), "#01");
            Assert.AreEqual("F2147483647", int.MaxValue.Fmt("F0.", _nfi), "#02");
            Assert.AreEqual("-F2147483648", int.MinValue.Fmt("F0.", _nfi), "#03");
        }

        [Test]
        public void Test02021()
        {
            Assert.AreEqual("F0.0", 0.Fmt("F0.0", _nfi), "#01");
            Assert.AreEqual("F2147483647.0", int.MaxValue.Fmt("F0.0", _nfi), "#02");
            Assert.AreEqual("-F2147483648.0", int.MinValue.Fmt("F0.0", _nfi), "#03");
        }

        [Test]
        public void Test02022()
        {
            Assert.AreEqual("F09", 0.Fmt("F0.9", _nfi), "#01");
            Assert.AreEqual("F21474836479", int.MaxValue.Fmt("F0.9", _nfi), "#02");
            Assert.AreEqual("-F21474836489", int.MinValue.Fmt("F0.9", _nfi), "#03");
        }

        [Test]
        public void Test02023()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberDecimalDigits = 0;
            Assert.AreEqual("0", 0.Fmt("F", nfi), "#01");
            nfi.NumberDecimalDigits = 1;
            Assert.AreEqual("0.0", 0.Fmt("F", nfi), "#02");
            nfi.NumberDecimalDigits = 99;
            Assert.AreEqual(
                "0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                0.Fmt("F", nfi), "#03");
        }

        [Test]
        public void Test02024()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "";
            Assert.AreEqual("2147483648.00", int.MinValue.Fmt("F", nfi), "#01");
            nfi.NegativeSign = "-";
            Assert.AreEqual("-2147483648.00", int.MinValue.Fmt("F", nfi), "#02");
            nfi.NegativeSign = "+";
            Assert.AreEqual("+2147483648.00", int.MinValue.Fmt("F", nfi), "#03");
            nfi.NegativeSign = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Assert.AreEqual("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ2147483648.00",
                int.MinValue.Fmt("F", nfi), "#04");
        }

        [Test]
        public void Test02025()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "-";
            nfi.PositiveSign = "+";
            Assert.AreEqual("-1.00", (-1).Fmt("F", nfi), "#01");
            Assert.AreEqual("0.00", 0.Fmt("F", nfi), "#02");
            Assert.AreEqual("1.00", 1.Fmt("F", nfi), "#03");
        }

        [Test]
        public void Test02026()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "+";
            nfi.PositiveSign = "-";
            Assert.AreEqual("+1.00", (-1).Fmt("F", nfi), "#01");
            Assert.AreEqual("0.00", 0.Fmt("F", nfi), "#02");
            Assert.AreEqual("1.00", 1.Fmt("F", nfi), "#03");
        }

        [Test]
        public void Test02027()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberDecimalSeparator = "#";
            Assert.AreEqual("1#00", 1.Fmt("F", nfi), "#01");
        }

        // Test03000 - Int32 and G
        [Test]
        public void Test03000()
        {
            Assert.AreEqual("0", 0.Fmt("G", _nfi), "#01");
            Assert.AreEqual("0", 0.Fmt("g", _nfi), "#02");
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("G", _nfi), "#03");
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("g", _nfi), "#04");
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("G", _nfi), "#05");
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("g", _nfi), "#06");
        }

        [Test]
        public void Test03001()
        {
            Assert.AreEqual("G ", 0.Fmt("G ", _nfi), "#01");
            Assert.AreEqual(" G", 0.Fmt(" G", _nfi), "#02");
            Assert.AreEqual(" G ", 0.Fmt(" G ", _nfi), "#03");
        }

        [Test]
        public void Test03002()
        {
            Assert.AreEqual("-G ", (-1).Fmt("G ", _nfi), "#01");
            Assert.AreEqual("- G", (-1).Fmt(" G", _nfi), "#02");
            Assert.AreEqual("- G ", (-1).Fmt(" G ", _nfi), "#03");
        }

        [Test]
        public void Test03003()
        {
            Assert.AreEqual("0", 0.Fmt("G0", _nfi), "#01");
            Assert.AreEqual("0", 0.Fmt("G9", _nfi), "#02");
            Assert.AreEqual("0", 0.Fmt("G10", _nfi), "#03");
            Assert.AreEqual("0", 0.Fmt("G99", _nfi), "#04");
            Assert.AreEqual("G100", 0.Fmt("G100", _nfi), "#05");
        }

        [Test]
        public void Test03004()
        {
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("G0", _nfi), "#01");
            Assert.AreEqual("2.14748365E+09", int.MaxValue.Fmt("G9", _nfi), "#02");
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("G10", _nfi), "#03");
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("G99", _nfi), "#04");
            Assert.AreEqual("G12147483647", int.MaxValue.Fmt("G100", _nfi), "#05");
        }

        [Test]
        public void Test03005()
        {
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("G0", _nfi), "#01");
            Assert.AreEqual("-2.14748365E+09", int.MinValue.Fmt("G9", _nfi), "#02");
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("G10", _nfi), "#03");
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("G99", _nfi), "#04");
            Assert.AreEqual("-G12147483648", int.MinValue.Fmt("G100", _nfi), "#05");
        }

        [Test]
        public void Test03006()
        {
            Assert.AreEqual("GF", 0.Fmt("GF", _nfi), "#01");
            Assert.AreEqual("G0F", 0.Fmt("G0F", _nfi), "#02");
            Assert.AreEqual("G0xF", 0.Fmt("G0xF", _nfi), "#03");
        }

        [Test]
        public void Test03007()
        {
            Assert.AreEqual("GF", int.MaxValue.Fmt("GF", _nfi), "#01");
            Assert.AreEqual("G2147483647F", int.MaxValue.Fmt("G0F", _nfi), "#02");
            Assert.AreEqual("G2147483647xF", int.MaxValue.Fmt("G0xF", _nfi), "#03");
        }

        [Test]
        public void Test03008()
        {
            Assert.AreEqual("-GF", int.MinValue.Fmt("GF", _nfi), "#01");
            Assert.AreEqual("-G2147483648F", int.MinValue.Fmt("G0F", _nfi), "#02");
            Assert.AreEqual("-G2147483648xF", int.MinValue.Fmt("G0xF", _nfi), "#03");
        }

        [Test]
        public void Test03009()
        {
            Assert.AreEqual("0", 0.Fmt("G0000000000000000000000000000000000000010", _nfi), "#01");
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("G0000000000000000000000000000000000000010", _nfi), "#02");
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("G0000000000000000000000000000000000000010", _nfi), "#03");
        }

        [Test]
        public void Test03010()
        {
            Assert.AreEqual("+G", 0.Fmt("+G", _nfi), "#01");
            Assert.AreEqual("G+", 0.Fmt("G+", _nfi), "#02");
            Assert.AreEqual("+G+", 0.Fmt("+G+", _nfi), "#03");
        }

        [Test]
        public void Test03011()
        {
            Assert.AreEqual("+G", int.MaxValue.Fmt("+G", _nfi), "#01");
            Assert.AreEqual("G+", int.MaxValue.Fmt("G+", _nfi), "#02");
            Assert.AreEqual("+G+", int.MaxValue.Fmt("+G+", _nfi), "#03");
        }

        [Test]
        public void Test03012()
        {
            Assert.AreEqual("-+G", int.MinValue.Fmt("+G", _nfi), "#01");
            Assert.AreEqual("-G+", int.MinValue.Fmt("G+", _nfi), "#02");
            Assert.AreEqual("-+G+", int.MinValue.Fmt("+G+", _nfi), "#03");
        }

        [Test]
        public void Test03013()
        {
            Assert.AreEqual("-G", 0.Fmt("-G", _nfi), "#01");
            Assert.AreEqual("G-", 0.Fmt("G-", _nfi), "#02");
            Assert.AreEqual("-G-", 0.Fmt("-G-", _nfi), "#03");
        }

        [Test]
        public void Test03014()
        {
            Assert.AreEqual("-G", int.MaxValue.Fmt("-G", _nfi), "#01");
            Assert.AreEqual("G-", int.MaxValue.Fmt("G-", _nfi), "#02");
            Assert.AreEqual("-G-", int.MaxValue.Fmt("-G-", _nfi), "#03");
        }

        [Test]
        public void Test03015()
        {
            Assert.AreEqual("--G", int.MinValue.Fmt("-G", _nfi), "#01");
            Assert.AreEqual("-G-", int.MinValue.Fmt("G-", _nfi), "#02");
            Assert.AreEqual("--G-", int.MinValue.Fmt("-G-", _nfi), "#03");
        }

        [Test]
        public void Test03016()
        {
            Assert.AreEqual("G+0", 0.Fmt("G+0", _nfi), "#01");
            Assert.AreEqual("G+2147483647", int.MaxValue.Fmt("G+0", _nfi), "#02");
            Assert.AreEqual("-G+2147483648", int.MinValue.Fmt("G+0", _nfi), "#03");
        }

        [Test]
        public void Test03017()
        {
            Assert.AreEqual("G+9", 0.Fmt("G+9", _nfi), "#01");
            Assert.AreEqual("G+9", int.MaxValue.Fmt("G+9", _nfi), "#02");
            Assert.AreEqual("-G+9", int.MinValue.Fmt("G+9", _nfi), "#03");
        }

        [Test]
        public void Test03018()
        {
            Assert.AreEqual("G-9", 0.Fmt("G-9", _nfi), "#01");
            Assert.AreEqual("G-9", int.MaxValue.Fmt("G-9", _nfi), "#02");
            Assert.AreEqual("-G-9", int.MinValue.Fmt("G-9", _nfi), "#03");
        }

        [Test]
        public void Test03019()
        {
            Assert.AreEqual("G0", 0.Fmt("G0,", _nfi), "#01");
            Assert.AreEqual("G2147484", int.MaxValue.Fmt("G0,", _nfi), "#02");
            Assert.AreEqual("-G2147484", int.MinValue.Fmt("G0,", _nfi), "#03");
        }

        [Test]
        public void Test03020()
        {
            Assert.AreEqual("G0", 0.Fmt("G0.", _nfi), "#01");
            Assert.AreEqual("G2147483647", int.MaxValue.Fmt("G0.", _nfi), "#02");
            Assert.AreEqual("-G2147483648", int.MinValue.Fmt("G0.", _nfi), "#03");
        }

        [Test]
        public void Test03021()
        {
            Assert.AreEqual("G0.0", 0.Fmt("G0.0", _nfi), "#01");
            Assert.AreEqual("G2147483647.0", int.MaxValue.Fmt("G0.0", _nfi), "#02");
            Assert.AreEqual("-G2147483648.0", int.MinValue.Fmt("G0.0", _nfi), "#03");
        }

        [Test]
        public void Test03022()
        {
            Assert.AreEqual("G09", 0.Fmt("G0.9", _nfi), "#01");
            Assert.AreEqual("G21474836479", int.MaxValue.Fmt("G0.9", _nfi), "#02");
            Assert.AreEqual("-G21474836489", int.MinValue.Fmt("G0.9", _nfi), "#03");
        }

        [Test]
        public void Test03023()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberDecimalDigits = 0;
            Assert.AreEqual("0", 0.Fmt("G", nfi), "#01");
            nfi.NumberDecimalDigits = 1;
            Assert.AreEqual("0", 0.Fmt("G", nfi), "#02");
            nfi.NumberDecimalDigits = 99;
            Assert.AreEqual("0", 0.Fmt("G", nfi), "#03");
        }

        [Test]
        public void Test03024()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "";
            Assert.AreEqual("2147483648", int.MinValue.Fmt("G", nfi), "#01");
            nfi.NegativeSign = "-";
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("G", nfi), "#02");
            nfi.NegativeSign = "+";
            Assert.AreEqual("+2147483648", int.MinValue.Fmt("G", nfi), "#03");
            nfi.NegativeSign = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Assert.AreEqual("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ2147483648", int.MinValue.Fmt("G", nfi),
                "#04");
        }

        [Test]
        public void Test03025()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "-";
            nfi.PositiveSign = "+";
            Assert.AreEqual("-1", (-1).Fmt("G", nfi), "#01");
            Assert.AreEqual("0", 0.Fmt("G", nfi), "#02");
            Assert.AreEqual("1", 1.Fmt("G", nfi), "#03");
        }

        [Test]
        public void Test03026()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "+";
            nfi.PositiveSign = "-";
            Assert.AreEqual("+1", (-1).Fmt("G", nfi), "#01");
            Assert.AreEqual("0", 0.Fmt("G", nfi), "#02");
            Assert.AreEqual("1", 1.Fmt("G", nfi), "#03");
        }

        [Test]
        public void Test03027()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberDecimalSeparator = "#";
            Assert.AreEqual("1#2E+02", 123.Fmt("G2", nfi), "#01");
        }

        // Test04000 - Int32 and N
        [Test]
        public void Test04000()
        {
            Assert.AreEqual("0.00", 0.Fmt("N", _nfi), "#01");
            Assert.AreEqual("0.00", 0.Fmt("n", _nfi), "#02");
            Assert.AreEqual("-2,147,483,648.00", int.MinValue.Fmt("N", _nfi), "#03");
            Assert.AreEqual("-2,147,483,648.00", int.MinValue.Fmt("n", _nfi), "#04");
            Assert.AreEqual("2,147,483,647.00", int.MaxValue.Fmt("N", _nfi), "#05");
            Assert.AreEqual("2,147,483,647.00", int.MaxValue.Fmt("n", _nfi), "#06");
        }

        [Test]
        public void Test04001()
        {
            Assert.AreEqual("N ", 0.Fmt("N ", _nfi), "#01");
            Assert.AreEqual(" N", 0.Fmt(" N", _nfi), "#02");
            Assert.AreEqual(" N ", 0.Fmt(" N ", _nfi), "#03");
        }

        [Test]
        public void Test04002()
        {
            Assert.AreEqual("-N ", (-1).Fmt("N ", _nfi), "#01");
            Assert.AreEqual("- N", (-1).Fmt(" N", _nfi), "#02");
            Assert.AreEqual("- N ", (-1).Fmt(" N ", _nfi), "#03");
        }

        [Test]
        public void Test04003()
        {
            Assert.AreEqual("0", 0.Fmt("N0", _nfi), "#01");
            Assert.AreEqual("0.000000000", 0.Fmt("N9", _nfi), "#02");
            Assert.AreEqual("0.0000000000", 0.Fmt("N10", _nfi), "#03");
            Assert.AreEqual(
                "0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                0.Fmt("N99", _nfi), "#04");
            Assert.AreEqual("N100", 0.Fmt("N100", _nfi), "#05");
        }

        [Test]
        public void Test04004()
        {
            Assert.AreEqual("2,147,483,647", int.MaxValue.Fmt("N0", _nfi), "#01");
            Assert.AreEqual("2,147,483,647.000000000", int.MaxValue.Fmt("N9", _nfi), "#02");
            Assert.AreEqual("2,147,483,647.0000000000", int.MaxValue.Fmt("N10", _nfi), "#03");
            Assert.AreEqual(
                "2,147,483,647.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                int.MaxValue.Fmt("N99", _nfi), "#04");
            Assert.AreEqual("N12147483647", int.MaxValue.Fmt("N100", _nfi), "#05");
        }

        [Test]
        public void Test04005()
        {
            Assert.AreEqual("-2,147,483,648", int.MinValue.Fmt("N0", _nfi), "#01");
            Assert.AreEqual("-2,147,483,648.000000000", int.MinValue.Fmt("N9", _nfi), "#02");
            Assert.AreEqual("-2,147,483,648.0000000000", int.MinValue.Fmt("N10", _nfi), "#03");
            Assert.AreEqual(
                "-2,147,483,648.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                int.MinValue.Fmt("N99", _nfi), "#04");
            Assert.AreEqual("-N12147483648", int.MinValue.Fmt("N100", _nfi), "#05");
        }

        [Test]
        public void Test04006()
        {
            Assert.AreEqual("NF", 0.Fmt("NF", _nfi), "#01");
            Assert.AreEqual("N0F", 0.Fmt("N0F", _nfi), "#02");
            Assert.AreEqual("N0xF", 0.Fmt("N0xF", _nfi), "#03");
        }

        [Test]
        public void Test04007()
        {
            Assert.AreEqual("NF", int.MaxValue.Fmt("NF", _nfi), "#01");
            Assert.AreEqual("N2147483647F", int.MaxValue.Fmt("N0F", _nfi), "#02");
            Assert.AreEqual("N2147483647xF", int.MaxValue.Fmt("N0xF", _nfi), "#03");
        }

        [Test]
        public void Test04008()
        {
            Assert.AreEqual("-NF", int.MinValue.Fmt("NF", _nfi), "#01");
            Assert.AreEqual("-N2147483648F", int.MinValue.Fmt("N0F", _nfi), "#02");
            Assert.AreEqual("-N2147483648xF", int.MinValue.Fmt("N0xF", _nfi), "#03");
        }

        [Test]
        public void Test04009()
        {
            Assert.AreEqual("0.0000000000", 0.Fmt("N0000000000000000000000000000000000000010", _nfi), "#01");
            Assert.AreEqual("2,147,483,647.0000000000",
                int.MaxValue.Fmt("N0000000000000000000000000000000000000010", _nfi), "#02");
            Assert.AreEqual("-2,147,483,648.0000000000",
                int.MinValue.Fmt("N0000000000000000000000000000000000000010", _nfi), "#03");
        }

        [Test]
        public void Test04010()
        {
            Assert.AreEqual("+N", 0.Fmt("+N", _nfi), "#01");
            Assert.AreEqual("N+", 0.Fmt("N+", _nfi), "#02");
            Assert.AreEqual("+N+", 0.Fmt("+N+", _nfi), "#03");
        }

        [Test]
        public void Test04011()
        {
            Assert.AreEqual("+N", int.MaxValue.Fmt("+N", _nfi), "#01");
            Assert.AreEqual("N+", int.MaxValue.Fmt("N+", _nfi), "#02");
            Assert.AreEqual("+N+", int.MaxValue.Fmt("+N+", _nfi), "#03");
        }

        [Test]
        public void Test04012()
        {
            Assert.AreEqual("-+N", int.MinValue.Fmt("+N", _nfi), "#01");
            Assert.AreEqual("-N+", int.MinValue.Fmt("N+", _nfi), "#02");
            Assert.AreEqual("-+N+", int.MinValue.Fmt("+N+", _nfi), "#03");
        }

        [Test]
        public void Test04013()
        {
            Assert.AreEqual("-N", 0.Fmt("-N", _nfi), "#01");
            Assert.AreEqual("N-", 0.Fmt("N-", _nfi), "#02");
            Assert.AreEqual("-N-", 0.Fmt("-N-", _nfi), "#03");
        }

        [Test]
        public void Test04014()
        {
            Assert.AreEqual("-N", int.MaxValue.Fmt("-N", _nfi), "#01");
            Assert.AreEqual("N-", int.MaxValue.Fmt("N-", _nfi), "#02");
            Assert.AreEqual("-N-", int.MaxValue.Fmt("-N-", _nfi), "#03");
        }

        [Test]
        public void Test04015()
        {
            Assert.AreEqual("--N", int.MinValue.Fmt("-N", _nfi), "#01");
            Assert.AreEqual("-N-", int.MinValue.Fmt("N-", _nfi), "#02");
            Assert.AreEqual("--N-", int.MinValue.Fmt("-N-", _nfi), "#03");
        }

        [Test]
        public void Test04016()
        {
            Assert.AreEqual("N+0", 0.Fmt("N+0", _nfi), "#01");
            Assert.AreEqual("N+2147483647", int.MaxValue.Fmt("N+0", _nfi), "#02");
            Assert.AreEqual("-N+2147483648", int.MinValue.Fmt("N+0", _nfi), "#03");
        }

        [Test]
        public void Test04017()
        {
            Assert.AreEqual("N+9", 0.Fmt("N+9", _nfi), "#01");
            Assert.AreEqual("N+9", int.MaxValue.Fmt("N+9", _nfi), "#02");
            Assert.AreEqual("-N+9", int.MinValue.Fmt("N+9", _nfi), "#03");
        }

        [Test]
        public void Test04018()
        {
            Assert.AreEqual("N-9", 0.Fmt("N-9", _nfi), "#01");
            Assert.AreEqual("N-9", int.MaxValue.Fmt("N-9", _nfi), "#02");
            Assert.AreEqual("-N-9", int.MinValue.Fmt("N-9", _nfi), "#03");
        }

        [Test]
        public void Test04019()
        {
            Assert.AreEqual("N0", 0.Fmt("N0,", _nfi), "#01");
            Assert.AreEqual("N2147484", int.MaxValue.Fmt("N0,", _nfi), "#02");
            Assert.AreEqual("-N2147484", int.MinValue.Fmt("N0,", _nfi), "#03");
        }

        [Test]
        public void Test04020()
        {
            Assert.AreEqual("N0", 0.Fmt("N0.", _nfi), "#01");
            Assert.AreEqual("N2147483647", int.MaxValue.Fmt("N0.", _nfi), "#02");
            Assert.AreEqual("-N2147483648", int.MinValue.Fmt("N0.", _nfi), "#03");
        }

        [Test]
        public void Test04021()
        {
            Assert.AreEqual("N0.0", 0.Fmt("N0.0", _nfi), "#01");
            Assert.AreEqual("N2147483647.0", int.MaxValue.Fmt("N0.0", _nfi), "#02");
            Assert.AreEqual("-N2147483648.0", int.MinValue.Fmt("N0.0", _nfi), "#03");
        }

        [Test]
        public void Test04022()
        {
            Assert.AreEqual("N09", 0.Fmt("N0.9", _nfi), "#01");
            Assert.AreEqual("N21474836479", int.MaxValue.Fmt("N0.9", _nfi), "#02");
            Assert.AreEqual("-N21474836489", int.MinValue.Fmt("N0.9", _nfi), "#03");
        }

        [Test]
        public void Test04023()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberDecimalDigits = 0;
            Assert.AreEqual("0", 0.Fmt("N", nfi), "#01");
            nfi.NumberDecimalDigits = 1;
            Assert.AreEqual("0.0", 0.Fmt("N", nfi), "#02");
            nfi.NumberDecimalDigits = 99;
            Assert.AreEqual(
                "0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                0.Fmt("N", nfi), "#03");
        }

        [Test]
        public void Test04024()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "";
            Assert.AreEqual("2,147,483,648.00", int.MinValue.Fmt("N", nfi), "#01");
            nfi.NegativeSign = "-";
            Assert.AreEqual("-2,147,483,648.00", int.MinValue.Fmt("N", nfi), "#02");
            nfi.NegativeSign = "+";
            Assert.AreEqual("+2,147,483,648.00", int.MinValue.Fmt("N", nfi), "#03");
            nfi.NegativeSign = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Assert.AreEqual("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ2,147,483,648.00",
                int.MinValue.Fmt("N", nfi), "#04");
        }

        [Test]
        public void Test04025()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "-";
            nfi.PositiveSign = "+";
            Assert.AreEqual("-1.00", (-1).Fmt("N", nfi), "#01");
            Assert.AreEqual("0.00", 0.Fmt("N", nfi), "#02");
            Assert.AreEqual("1.00", 1.Fmt("N", nfi), "#03");
        }

        [Test]
        public void Test04026()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "+";
            nfi.PositiveSign = "-";
            Assert.AreEqual("+1.00", (-1).Fmt("N", nfi), "#01");
            Assert.AreEqual("0.00", 0.Fmt("N", nfi), "#02");
            Assert.AreEqual("1.00", 1.Fmt("N", nfi), "#03");
        }

        [Test]
        public void Test04027()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberDecimalSeparator = "#";
            Assert.AreEqual("123#0", 123.Fmt("N1", nfi), "#01");
        }

        [Test]
        public void Test04028()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberGroupSeparator = "-";
            Assert.AreEqual("-2-147-483-648.0", int.MinValue.Fmt("N1", nfi), "#01");
        }

        [Test]
        public void Test04029()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberGroupSizes = new int[] {};
            Assert.AreEqual("-2147483648.0", int.MinValue.Fmt("N1", nfi), "#01");
            nfi.NumberGroupSizes = new int[] {0};
            Assert.AreEqual("-2147483648.0", int.MinValue.Fmt("N1", nfi), "#02");
            nfi.NumberGroupSizes = new int[] {1};
            Assert.AreEqual("-2,1,4,7,4,8,3,6,4,8.0", int.MinValue.Fmt("N1", nfi), "#03");
            nfi.NumberGroupSizes = new int[] {3};
            Assert.AreEqual("-2,147,483,648.0", int.MinValue.Fmt("N1", nfi), "#04");
            nfi.NumberGroupSizes = new int[] {9};
            Assert.AreEqual("-2,147483648.0", int.MinValue.Fmt("N1", nfi), "#05");
        }

        [Test]
        public void Test04030()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberGroupSizes = new int[] {1, 2};
            Assert.AreEqual("-2,14,74,83,64,8.0", int.MinValue.Fmt("N1", nfi), "#01");
            nfi.NumberGroupSizes = new int[] {1, 2, 3};
            Assert.AreEqual("-2,147,483,64,8.0", int.MinValue.Fmt("N1", nfi), "#02");
            nfi.NumberGroupSizes = new int[] {1, 2, 3, 4};
            Assert.AreEqual("-2147,483,64,8.0", int.MinValue.Fmt("N1", nfi), "#03");
            nfi.NumberGroupSizes = new int[] {1, 2, 1, 2, 1, 2, 1};
            Assert.AreEqual("-2,14,7,48,3,64,8.0", int.MinValue.Fmt("N1", nfi), "#04");
            nfi.NumberGroupSizes = new int[] {1, 0};
            Assert.AreEqual("-214748364,8.0", int.MinValue.Fmt("N1", nfi), "#05");
            nfi.NumberGroupSizes = new int[] {1, 2, 0};
            Assert.AreEqual("-2147483,64,8.0", int.MinValue.Fmt("N1", nfi), "#06");
            nfi.NumberGroupSizes = new int[] {1, 2, 3, 0};
            Assert.AreEqual("-2147,483,64,8.0", int.MinValue.Fmt("N1", nfi), "#07");
            nfi.NumberGroupSizes = new int[] {1, 2, 3, 4, 0};
            Assert.AreEqual("-2147,483,64,8.0", int.MinValue.Fmt("N1", nfi), "#08");
        }

        [Test]
        public void Test04031()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "1234567890";
            Assert.AreEqual("12345678902,147,483,648.00", int.MinValue.Fmt("N", nfi), "#01");
        }

        // Test05000 - Int32 and P
        [Test]
        public void Test05000()
        {
            Assert.AreEqual("0.00 %", 0.Fmt("P", _nfi), "#01");
            Assert.AreEqual("0.00 %", 0.Fmt("p", _nfi), "#02");
            Assert.AreEqual("-214,748,364,800.00 %", int.MinValue.Fmt("P", _nfi), "#03");
            Assert.AreEqual("-214,748,364,800.00 %", int.MinValue.Fmt("p", _nfi), "#04");
            Assert.AreEqual("214,748,364,700.00 %", int.MaxValue.Fmt("P", _nfi), "#05");
            Assert.AreEqual("214,748,364,700.00 %", int.MaxValue.Fmt("p", _nfi), "#06");
        }

        [Test]
        public void Test05001()
        {
            Assert.AreEqual("P ", 0.Fmt("P ", _nfi), "#01");
            Assert.AreEqual(" P", 0.Fmt(" P", _nfi), "#02");
            Assert.AreEqual(" P ", 0.Fmt(" P ", _nfi), "#03");
        }

        [Test]
        public void Test05002()
        {
            Assert.AreEqual("-P ", (-1).Fmt("P ", _nfi), "#01");
            Assert.AreEqual("- P", (-1).Fmt(" P", _nfi), "#02");
            Assert.AreEqual("- P ", (-1).Fmt(" P ", _nfi), "#03");
        }

        [Test]
        public void Test05003()
        {
            Assert.AreEqual("0 %", 0.Fmt("P0", _nfi), "#01");
            Assert.AreEqual("0.000000000 %", 0.Fmt("P9", _nfi), "#02");
            Assert.AreEqual("0.0000000000 %", 0.Fmt("P10", _nfi), "#03");
            Assert.AreEqual(
                "0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 %",
                0.Fmt("P99", _nfi), "#04");
            Assert.AreEqual("P100", 0.Fmt("P100", _nfi), "#05");
        }

        [Test]
        public void Test05004()
        {
            Assert.AreEqual("214,748,364,700 %", int.MaxValue.Fmt("P0", _nfi), "#01");
            Assert.AreEqual("214,748,364,700.000000000 %", int.MaxValue.Fmt("P9", _nfi), "#02");
            Assert.AreEqual("214,748,364,700.0000000000 %", int.MaxValue.Fmt("P10", _nfi), "#03");
            Assert.AreEqual(
                "214,748,364,700.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 %",
                int.MaxValue.Fmt("P99", _nfi), "#04");
            Assert.AreEqual("P12147483647", int.MaxValue.Fmt("P100", _nfi), "#05");
        }

        [Test]
        public void Test05005()
        {
            Assert.AreEqual("-214,748,364,800 %", int.MinValue.Fmt("P0", _nfi), "#01");
            Assert.AreEqual("-214,748,364,800.000000000 %", int.MinValue.Fmt("P9", _nfi), "#02");
            Assert.AreEqual("-214,748,364,800.0000000000 %", int.MinValue.Fmt("P10", _nfi), "#03");
            Assert.AreEqual(
                "-214,748,364,800.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 %",
                int.MinValue.Fmt("P99", _nfi), "#04");
            Assert.AreEqual("-P12147483648", int.MinValue.Fmt("P100", _nfi), "#05");
        }

        [Test]
        public void Test05006()
        {
            Assert.AreEqual("PF", 0.Fmt("PF", _nfi), "#01");
            Assert.AreEqual("P0F", 0.Fmt("P0F", _nfi), "#02");
            Assert.AreEqual("P0xF", 0.Fmt("P0xF", _nfi), "#03");
        }

        [Test]
        public void Test05007()
        {
            Assert.AreEqual("PF", int.MaxValue.Fmt("PF", _nfi), "#01");
            Assert.AreEqual("P2147483647F", int.MaxValue.Fmt("P0F", _nfi), "#02");
            Assert.AreEqual("P2147483647xF", int.MaxValue.Fmt("P0xF", _nfi), "#03");
        }

        [Test]
        public void Test05008()
        {
            Assert.AreEqual("-PF", int.MinValue.Fmt("PF", _nfi), "#01");
            Assert.AreEqual("-P2147483648F", int.MinValue.Fmt("P0F", _nfi), "#02");
            Assert.AreEqual("-P2147483648xF", int.MinValue.Fmt("P0xF", _nfi), "#03");
        }

        [Test]
        public void Test05009()
        {
            Assert.AreEqual("0.0000000000 %", 0.Fmt("P0000000000000000000000000000000000000010", _nfi), "#01");
            Assert.AreEqual("214,748,364,700.0000000000 %",
                int.MaxValue.Fmt("P0000000000000000000000000000000000000010", _nfi), "#02");
            Assert.AreEqual("-214,748,364,800.0000000000 %",
                int.MinValue.Fmt("P0000000000000000000000000000000000000010", _nfi), "#03");
        }

        [Test]
        public void Test05010()
        {
            Assert.AreEqual("+P", 0.Fmt("+P", _nfi), "#01");
            Assert.AreEqual("P+", 0.Fmt("P+", _nfi), "#02");
            Assert.AreEqual("+P+", 0.Fmt("+P+", _nfi), "#03");
        }

        [Test]
        public void Test05011()
        {
            Assert.AreEqual("+P", int.MaxValue.Fmt("+P", _nfi), "#01");
            Assert.AreEqual("P+", int.MaxValue.Fmt("P+", _nfi), "#02");
            Assert.AreEqual("+P+", int.MaxValue.Fmt("+P+", _nfi), "#03");
        }

        [Test]
        public void Test05012()
        {
            Assert.AreEqual("-+P", int.MinValue.Fmt("+P", _nfi), "#01");
            Assert.AreEqual("-P+", int.MinValue.Fmt("P+", _nfi), "#02");
            Assert.AreEqual("-+P+", int.MinValue.Fmt("+P+", _nfi), "#03");
        }

        [Test]
        public void Test05013()
        {
            Assert.AreEqual("-P", 0.Fmt("-P", _nfi), "#01");
            Assert.AreEqual("P-", 0.Fmt("P-", _nfi), "#02");
            Assert.AreEqual("-P-", 0.Fmt("-P-", _nfi), "#03");
        }

        [Test]
        public void Test05014()
        {
            Assert.AreEqual("-P", int.MaxValue.Fmt("-P", _nfi), "#01");
            Assert.AreEqual("P-", int.MaxValue.Fmt("P-", _nfi), "#02");
            Assert.AreEqual("-P-", int.MaxValue.Fmt("-P-", _nfi), "#03");
        }

        [Test]
        public void Test05015()
        {
            Assert.AreEqual("--P", int.MinValue.Fmt("-P", _nfi), "#01");
            Assert.AreEqual("-P-", int.MinValue.Fmt("P-", _nfi), "#02");
            Assert.AreEqual("--P-", int.MinValue.Fmt("-P-", _nfi), "#03");
        }

        [Test]
        public void Test05016()
        {
            Assert.AreEqual("P+0", 0.Fmt("P+0", _nfi), "#01");
            Assert.AreEqual("P+2147483647", int.MaxValue.Fmt("P+0", _nfi), "#02");
            Assert.AreEqual("-P+2147483648", int.MinValue.Fmt("P+0", _nfi), "#03");
        }

        [Test]
        public void Test05017()
        {
            Assert.AreEqual("P+9", 0.Fmt("P+9", _nfi), "#01");
            Assert.AreEqual("P+9", int.MaxValue.Fmt("P+9", _nfi), "#02");
            Assert.AreEqual("-P+9", int.MinValue.Fmt("P+9", _nfi), "#03");
        }

        [Test]
        public void Test05018()
        {
            Assert.AreEqual("P-9", 0.Fmt("P-9", _nfi), "#01");
            Assert.AreEqual("P-9", int.MaxValue.Fmt("P-9", _nfi), "#02");
            Assert.AreEqual("-P-9", int.MinValue.Fmt("P-9", _nfi), "#03");
        }

        [Test]
        public void Test05019()
        {
            Assert.AreEqual("P0", 0.Fmt("P0,", _nfi), "#01");
            Assert.AreEqual("P2147484", int.MaxValue.Fmt("P0,", _nfi), "#02");
            Assert.AreEqual("-P2147484", int.MinValue.Fmt("P0,", _nfi), "#03");
        }

        [Test]
        public void Test05020()
        {
            Assert.AreEqual("P0", 0.Fmt("P0.", _nfi), "#01");
            Assert.AreEqual("P2147483647", int.MaxValue.Fmt("P0.", _nfi), "#02");
            Assert.AreEqual("-P2147483648", int.MinValue.Fmt("P0.", _nfi), "#03");
        }

        [Test]
        public void Test05021()
        {
            Assert.AreEqual("P0.0", 0.Fmt("P0.0", _nfi), "#01");
            Assert.AreEqual("P2147483647.0", int.MaxValue.Fmt("P0.0", _nfi), "#02");
            Assert.AreEqual("-P2147483648.0", int.MinValue.Fmt("P0.0", _nfi), "#03");
        }

        [Test]
        public void Test05022()
        {
            Assert.AreEqual("P09", 0.Fmt("P0.9", _nfi), "#01");
            Assert.AreEqual("P21474836479", int.MaxValue.Fmt("P0.9", _nfi), "#02");
            Assert.AreEqual("-P21474836489", int.MinValue.Fmt("P0.9", _nfi), "#03");
        }

        [Test]
        public void Test05023()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.PercentDecimalDigits = 0;
            Assert.AreEqual("0 %", 0.Fmt("P", nfi), "#01");
            nfi.PercentDecimalDigits = 1;
            Assert.AreEqual("0.0 %", 0.Fmt("P", nfi), "#02");
            nfi.PercentDecimalDigits = 99;
            Assert.AreEqual(
                "0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 %",
                0.Fmt("P", nfi), "#03");
        }

        [Test]
        public void Test05024()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "";
            Assert.AreEqual("214,748,364,800.00 %", int.MinValue.Fmt("P", nfi), "#01");
            nfi.NegativeSign = "-";
            Assert.AreEqual("-214,748,364,800.00 %", int.MinValue.Fmt("P", nfi), "#02");
            nfi.NegativeSign = "+";
            Assert.AreEqual("+214,748,364,800.00 %", int.MinValue.Fmt("P", nfi), "#03");
            nfi.NegativeSign = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMPOPQRSTUVWXYZ";
            Assert.AreEqual("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMPOPQRSTUVWXYZ214,748,364,800.00 %",
                int.MinValue.Fmt("P", nfi), "#04");
        }

        [Test]
        public void Test05025()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "-";
            nfi.PositiveSign = "+";
            Assert.AreEqual("-100.00 %", (-1).Fmt("P", nfi), "#01");
            Assert.AreEqual("0.00 %", 0.Fmt("P", nfi), "#02");
            Assert.AreEqual("100.00 %", 1.Fmt("P", nfi), "#03");
        }

        [Test]
        public void Test05026()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "+";
            nfi.PositiveSign = "-";
            Assert.AreEqual("+100.00 %", (-1).Fmt("P", nfi), "#01");
            Assert.AreEqual("0.00 %", 0.Fmt("P", nfi), "#02");
            Assert.AreEqual("100.00 %", 1.Fmt("P", nfi), "#03");
        }

        [Test]
        public void Test05027()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.PercentDecimalSeparator = "#";
            Assert.AreEqual("12,300#0 %", 123.Fmt("P1", nfi), "#01");
        }

        [Test]
        public void Test05028()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.PercentGroupSeparator = "-";
            Assert.AreEqual("-214-748-364-800.0 %", int.MinValue.Fmt("P1", nfi), "#01");
        }

        [Test]
        public void Test05029()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.PercentGroupSizes = new int[] {};
            Assert.AreEqual("-214748364800.0 %", int.MinValue.Fmt("P1", nfi), "#01");
            nfi.PercentGroupSizes = new int[] {0};
            Assert.AreEqual("-214748364800.0 %", int.MinValue.Fmt("P1", nfi), "#02");
            nfi.PercentGroupSizes = new int[] {1};
            Assert.AreEqual("-2,1,4,7,4,8,3,6,4,8,0,0.0 %", int.MinValue.Fmt("P1", nfi), "#03");
            nfi.PercentGroupSizes = new int[] {3};
            Assert.AreEqual("-214,748,364,800.0 %", int.MinValue.Fmt("P1", nfi), "#04");
            nfi.PercentGroupSizes = new int[] {9};
            Assert.AreEqual("-214,748364800.0 %", int.MinValue.Fmt("P1", nfi), "#05");
        }

        [Test]
        public void Test05030()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.PercentGroupSizes = new int[] {1, 2};
            Assert.AreEqual("-2,14,74,83,64,80,0.0 %", int.MinValue.Fmt("P1", nfi), "#01");
            nfi.PercentGroupSizes = new int[] {1, 2, 3};
            Assert.AreEqual("-214,748,364,80,0.0 %", int.MinValue.Fmt("P1", nfi), "#02");
            nfi.PercentGroupSizes = new int[] {1, 2, 3, 4};
            Assert.AreEqual("-21,4748,364,80,0.0 %", int.MinValue.Fmt("P1", nfi), "#03");
            nfi.PercentGroupSizes = new int[] {1, 2, 1, 2, 1, 2, 1};
            Assert.AreEqual("-2,1,4,74,8,36,4,80,0.0 %", int.MinValue.Fmt("P1", nfi), "#04");
            nfi.PercentGroupSizes = new int[] {1, 0};
            Assert.AreEqual("-21474836480,0.0 %", int.MinValue.Fmt("P1", nfi), "#05");
            nfi.PercentGroupSizes = new int[] {1, 2, 0};
            Assert.AreEqual("-214748364,80,0.0 %", int.MinValue.Fmt("P1", nfi), "#06");
            nfi.PercentGroupSizes = new int[] {1, 2, 3, 0};
            Assert.AreEqual("-214748,364,80,0.0 %", int.MinValue.Fmt("P1", nfi), "#07");
            nfi.PercentGroupSizes = new int[] {1, 2, 3, 4, 0};
            Assert.AreEqual("-21,4748,364,80,0.0 %", int.MinValue.Fmt("P1", nfi), "#08");
        }

        [Test]
        public void Test05031()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "1234567890";
            Assert.AreEqual("1234567890214,748,364,800.00 %", int.MinValue.Fmt("P", nfi), "#01");
        }

        [Test]
        public void Test05032()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.PercentNegativePattern = 0;
            Assert.AreEqual("-214,748,364,800.00 %", int.MinValue.Fmt("P", nfi), "#01");
            Assert.AreEqual("214,748,364,700.00 %", int.MaxValue.Fmt("P", nfi), "#02");
            Assert.AreEqual("0.00 %", 0.Fmt("P", nfi), "#03");
        }

        [Test]
        public void Test05033()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.PercentNegativePattern = 1;
            Assert.AreEqual("-214,748,364,800.00%", int.MinValue.Fmt("P", nfi), "#01");
            Assert.AreEqual("214,748,364,700.00 %", int.MaxValue.Fmt("P", nfi), "#02");
            Assert.AreEqual("0.00 %", 0.Fmt("P", nfi), "#03");
        }

        [Test]
        public void Test05034()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.PercentNegativePattern = 2;
            Assert.AreEqual("-%214,748,364,800.00", int.MinValue.Fmt("P", nfi), "#01");
            Assert.AreEqual("214,748,364,700.00 %", int.MaxValue.Fmt("P", nfi), "#02");
            Assert.AreEqual("0.00 %", 0.Fmt("P", nfi), "#03");
        }

        [Test]
        public void Test05035()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.PercentPositivePattern = 0;
            Assert.AreEqual("-214,748,364,800.00 %", int.MinValue.Fmt("P", nfi), "#01");
            Assert.AreEqual("214,748,364,700.00 %", int.MaxValue.Fmt("P", nfi), "#02");
            Assert.AreEqual("0.00 %", 0.Fmt("P", nfi), "#03");
        }

        [Test]
        public void Test05036()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.PercentPositivePattern = 1;
            Assert.AreEqual("-214,748,364,800.00 %", int.MinValue.Fmt("P", nfi), "#01");
            Assert.AreEqual("214,748,364,700.00%", int.MaxValue.Fmt("P", nfi), "#02");
            Assert.AreEqual("0.00%", 0.Fmt("P", nfi), "#03");
        }

        [Test]
        public void Test05037()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.PercentPositivePattern = 2;
            Assert.AreEqual("-214,748,364,800.00 %", int.MinValue.Fmt("P", nfi), "#01");
            Assert.AreEqual("%214,748,364,700.00", int.MaxValue.Fmt("P", nfi), "#02");
            Assert.AreEqual("%0.00", 0.Fmt("P", nfi), "#03");
        }

        // Test06000 - Int32 and R
        [Test]
        [ExpectedException(typeof (FormatException))]
        public void Test06000()
        {
            Assert.AreEqual("0", 0.Fmt("R", _nfi), "#01");
        }

        // Test07000- Int32 and X
        [Test]
        public void Test07000()
        {
            Assert.AreEqual("0", 0.Fmt("X", _nfi), "#01");
            Assert.AreEqual("0", 0.Fmt("x", _nfi), "#02");
            Assert.AreEqual("80000000", int.MinValue.Fmt("X", _nfi), "#03");
            Assert.AreEqual("80000000", int.MinValue.Fmt("x", _nfi), "#04");
            Assert.AreEqual("7FFFFFFF", int.MaxValue.Fmt("X", _nfi), "#05");
            Assert.AreEqual("7fffffff", int.MaxValue.Fmt("x", _nfi), "#06");
        }

        [Test]
        public void Test07001()
        {
            Assert.AreEqual("X ", 0.Fmt("X ", _nfi), "#01");
            Assert.AreEqual(" X", 0.Fmt(" X", _nfi), "#02");
            Assert.AreEqual(" X ", 0.Fmt(" X ", _nfi), "#03");
        }

        [Test]
        public void Test07002()
        {
            Assert.AreEqual("-X ", (-1).Fmt("X ", _nfi), "#01");
            Assert.AreEqual("- X", (-1).Fmt(" X", _nfi), "#02");
            Assert.AreEqual("- X ", (-1).Fmt(" X ", _nfi), "#03");
        }

        [Test]
        public void Test07003()
        {
            Assert.AreEqual("0", 0.Fmt("X0", _nfi), "#01");
            Assert.AreEqual("0000000000", 0.Fmt("X10", _nfi), "#02");
            Assert.AreEqual("00000000000", 0.Fmt("X11", _nfi), "#03");
            Assert.AreEqual(
                "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                0.Fmt("X99", _nfi), "#04");
            Assert.AreEqual("X100", 0.Fmt("X100", _nfi), "#05");
        }

        [Test]
        public void Test07004()
        {
            Assert.AreEqual("7FFFFFFF", int.MaxValue.Fmt("X0", _nfi), "#01");
            Assert.AreEqual("007FFFFFFF", int.MaxValue.Fmt("X10", _nfi), "#02");
            Assert.AreEqual("0007FFFFFFF", int.MaxValue.Fmt("X11", _nfi), "#03");
            Assert.AreEqual(
                "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFF",
                int.MaxValue.Fmt("X99", _nfi), "#04");
            Assert.AreEqual("X12147483647", int.MaxValue.Fmt("X100", _nfi), "#05");
        }

        [Test]
        public void Test07005()
        {
            Assert.AreEqual("80000000", int.MinValue.Fmt("X0", _nfi), "#01");
            Assert.AreEqual("0080000000", int.MinValue.Fmt("X10", _nfi), "#02");
            Assert.AreEqual("00080000000", int.MinValue.Fmt("X11", _nfi), "#03");
            Assert.AreEqual(
                "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000080000000",
                int.MinValue.Fmt("X99", _nfi), "#04");
            Assert.AreEqual("-X12147483648", int.MinValue.Fmt("X100", _nfi), "#05");
        }

        [Test]
        public void Test07006()
        {
            Assert.AreEqual("XF", 0.Fmt("XF", _nfi), "#01");
            Assert.AreEqual("X0F", 0.Fmt("X0F", _nfi), "#02");
            Assert.AreEqual("X0xF", 0.Fmt("X0xF", _nfi), "#03");
        }

        [Test]
        public void Test07007()
        {
            Assert.AreEqual("XF", int.MaxValue.Fmt("XF", _nfi), "#01");
            Assert.AreEqual("X2147483647F", int.MaxValue.Fmt("X0F", _nfi), "#02");
            Assert.AreEqual("X2147483647xF", int.MaxValue.Fmt("X0xF", _nfi), "#03");
        }

        [Test]
        public void Test07008()
        {
            Assert.AreEqual("-XF", int.MinValue.Fmt("XF", _nfi), "#01");
            Assert.AreEqual("-X2147483648F", int.MinValue.Fmt("X0F", _nfi), "#02");
            Assert.AreEqual("-X2147483648xF", int.MinValue.Fmt("X0xF", _nfi), "#03");
        }

        [Test]
        public void Test07009()
        {
            Assert.AreEqual("00000000000", 0.Fmt("X0000000000000000000000000000000000000011", _nfi), "#01");
            Assert.AreEqual("0007FFFFFFF", int.MaxValue.Fmt("X0000000000000000000000000000000000000011", _nfi), "#02");
            Assert.AreEqual("00080000000", int.MinValue.Fmt("X0000000000000000000000000000000000000011", _nfi), "#03");
        }

        [Test]
        public void Test07010()
        {
            Assert.AreEqual("+X", 0.Fmt("+X", _nfi), "#01");
            Assert.AreEqual("X+", 0.Fmt("X+", _nfi), "#02");
            Assert.AreEqual("+X+", 0.Fmt("+X+", _nfi), "#03");
        }

        [Test]
        public void Test07011()
        {
            Assert.AreEqual("+X", int.MaxValue.Fmt("+X", _nfi), "#01");
            Assert.AreEqual("X+", int.MaxValue.Fmt("X+", _nfi), "#02");
            Assert.AreEqual("+X+", int.MaxValue.Fmt("+X+", _nfi), "#03");
        }

        [Test]
        public void Test07012()
        {
            Assert.AreEqual("-+X", int.MinValue.Fmt("+X", _nfi), "#01");
            Assert.AreEqual("-X+", int.MinValue.Fmt("X+", _nfi), "#02");
            Assert.AreEqual("-+X+", int.MinValue.Fmt("+X+", _nfi), "#03");
        }

        [Test]
        public void Test07013()
        {
            Assert.AreEqual("-X", 0.Fmt("-X", _nfi), "#01");
            Assert.AreEqual("X-", 0.Fmt("X-", _nfi), "#02");
            Assert.AreEqual("-X-", 0.Fmt("-X-", _nfi), "#03");
        }

        [Test]
        public void Test07014()
        {
            Assert.AreEqual("-X", int.MaxValue.Fmt("-X", _nfi), "#01");
            Assert.AreEqual("X-", int.MaxValue.Fmt("X-", _nfi), "#02");
            Assert.AreEqual("-X-", int.MaxValue.Fmt("-X-", _nfi), "#03");
        }

        [Test]
        public void Test07015()
        {
            Assert.AreEqual("--X", int.MinValue.Fmt("-X", _nfi), "#01");
            Assert.AreEqual("-X-", int.MinValue.Fmt("X-", _nfi), "#02");
            Assert.AreEqual("--X-", int.MinValue.Fmt("-X-", _nfi), "#03");
        }

        [Test]
        public void Test07016()
        {
            Assert.AreEqual("X+0", 0.Fmt("X+0", _nfi), "#01");
            Assert.AreEqual("X+2147483647", int.MaxValue.Fmt("X+0", _nfi), "#02");
            Assert.AreEqual("-X+2147483648", int.MinValue.Fmt("X+0", _nfi), "#03");
        }

        [Test]
        public void Test07017()
        {
            Assert.AreEqual("X+9", 0.Fmt("X+9", _nfi), "#01");
            Assert.AreEqual("X+9", int.MaxValue.Fmt("X+9", _nfi), "#02");
            Assert.AreEqual("-X+9", int.MinValue.Fmt("X+9", _nfi), "#03");
        }

        [Test]
        public void Test07018()
        {
            Assert.AreEqual("X-9", 0.Fmt("X-9", _nfi), "#01");
            Assert.AreEqual("X-9", int.MaxValue.Fmt("X-9", _nfi), "#02");
            Assert.AreEqual("-X-9", int.MinValue.Fmt("X-9", _nfi), "#03");
        }

        [Test]
        public void Test07019()
        {
            Assert.AreEqual("X0", 0.Fmt("X0,", _nfi), "#01");
            Assert.AreEqual("X2147484", int.MaxValue.Fmt("X0,", _nfi), "#02");
            Assert.AreEqual("-X2147484", int.MinValue.Fmt("X0,", _nfi), "#03");
        }

        [Test]
        public void Test07020()
        {
            Assert.AreEqual("X0", 0.Fmt("X0.", _nfi), "#01");
            Assert.AreEqual("X2147483647", int.MaxValue.Fmt("X0.", _nfi), "#02");
            Assert.AreEqual("-X2147483648", int.MinValue.Fmt("X0.", _nfi), "#03");
        }

        [Test]
        public void Test07021()
        {
            Assert.AreEqual("X0.0", 0.Fmt("X0.0", _nfi), "#01");
            Assert.AreEqual("X2147483647.0", int.MaxValue.Fmt("X0.0", _nfi), "#02");
            Assert.AreEqual("-X2147483648.0", int.MinValue.Fmt("X0.0", _nfi), "#03");
        }

        [Test]
        public void Test07022()
        {
            Assert.AreEqual("X09", 0.Fmt("X0.9", _nfi), "#01");
            Assert.AreEqual("X21474836479", int.MaxValue.Fmt("X0.9", _nfi), "#02");
            Assert.AreEqual("-X21474836489", int.MinValue.Fmt("X0.9", _nfi), "#03");
        }

        [Test]
        public void Test08000()
        {
            Assert.AreEqual("0", 0.Fmt("0", _nfi), "#01");
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("0", _nfi), "#02");
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("0", _nfi), "#03");
        }

        // Test08000 - Int32 and Custom
        [Test]
        public void Test08001()
        {
            Assert.AreEqual("00000000000", 0.Fmt("00000000000", _nfi), "#01");
            Assert.AreEqual("02147483647", int.MaxValue.Fmt("00000000000", _nfi), "#02");
            Assert.AreEqual("-02147483648", int.MinValue.Fmt("00000000000", _nfi), "#03");
        }

        [Test]
        public void Test08002()
        {
            Assert.AreEqual(" 00000000000 ", 0.Fmt(" 00000000000 ", _nfi), "#01");
            Assert.AreEqual(" 02147483647 ", int.MaxValue.Fmt(" 00000000000 ", _nfi), "#02");
            Assert.AreEqual("- 02147483648 ", int.MinValue.Fmt(" 00000000000 ", _nfi), "#03");
        }

        [Test]
        public void Test08003()
        {
            Assert.AreEqual("", 0.Fmt("#", _nfi), "#01");
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("#", _nfi), "#02");
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("#", _nfi), "#03");
        }

        [Test]
        public void Test08004()
        {
            Assert.AreEqual("", 0.Fmt("##########", _nfi), "#01");
            Assert.AreEqual("2147483647", int.MaxValue.Fmt("##########", _nfi), "#02");
            Assert.AreEqual("-2147483648", int.MinValue.Fmt("##########", _nfi), "#03");
        }

        [Test]
        public void Test08005()
        {
            Assert.AreEqual("  ", 0.Fmt(" ########## ", _nfi), "#01");
            Assert.AreEqual(" 2147483647 ", int.MaxValue.Fmt(" ########## ", _nfi), "#02");
            Assert.AreEqual("- 2147483648 ", int.MinValue.Fmt(" ########## ", _nfi), "#03");
        }

        [Test]
        public void Test08006()
        {
            Assert.AreEqual("", 0.Fmt(".", _nfi), "#01");
            Assert.AreEqual("", int.MaxValue.Fmt(".", _nfi), "#02");
            Assert.AreEqual("-", int.MinValue.Fmt(".", _nfi), "#03");
        }

        [Test]
        public void Test08007()
        {
            Assert.AreEqual("00000000000", 0.Fmt("00000000000.", _nfi), "#01");
            Assert.AreEqual("02147483647", int.MaxValue.Fmt("00000000000.", _nfi), "#02");
            Assert.AreEqual("-02147483648", int.MinValue.Fmt("00000000000.", _nfi), "#03");
        }

        [Test]
        public void Test08008()
        {
            Assert.AreEqual(".00000000000", 0.Fmt(".00000000000", _nfi), "#01");
            Assert.AreEqual("2147483647.00000000000", int.MaxValue.Fmt(".00000000000", _nfi), "#02");
            Assert.AreEqual("-2147483648.00000000000", int.MinValue.Fmt(".00000000000", _nfi), "#03");
        }

        [Test]
        public void Test08009()
        {
            Assert.AreEqual("00000000000.00000000000", 0.Fmt("00000000000.00000000000", _nfi), "#01");
            Assert.AreEqual("02147483647.00000000000", int.MaxValue.Fmt("00000000000.00000000000", _nfi), "#02");
            Assert.AreEqual("-02147483648.00000000000", int.MinValue.Fmt("00000000000.00000000000", _nfi), "#03");
        }

        [Test]
        public void Test08010()
        {
            Assert.AreEqual("00.0000000000", 0.Fmt("00.0.00.000.0000", _nfi), "#01");
            Assert.AreEqual("01.0000000000", 1.Fmt("00.0.00.000.0000", _nfi), "#02");
            Assert.AreEqual("-01.0000000000", (-1).Fmt("00.0.00.000.0000", _nfi), "#03");
        }

        [Test]
        public void Test08011()
        {
            Assert.AreEqual("", 0.Fmt("##.#.##.###.####", _nfi), "#01");
            Assert.AreEqual("1", 1.Fmt("##.#.##.###.####", _nfi), "#02");
            Assert.AreEqual("-1", (-1).Fmt("##.#.##.###.####", _nfi), "#03");
        }

        [Test]
        public void Test08012()
        {
            Assert.AreEqual("00", 0.Fmt("0#.#.##.###.####", _nfi), "#01");
            Assert.AreEqual("01", 1.Fmt("0#.#.##.###.####", _nfi), "#02");
            Assert.AreEqual("-01", (-1).Fmt("0#.#.##.###.####", _nfi), "#03");
        }

        [Test]
        public void Test08013()
        {
            Assert.AreEqual("0", 0.Fmt("#0.#.##.###.####", _nfi), "#01");
            Assert.AreEqual("1", 1.Fmt("#0.#.##.###.####", _nfi), "#02");
            Assert.AreEqual("-1", (-1).Fmt("#0.#.##.###.####", _nfi), "#03");
        }

        [Test]
        public void Test08014()
        {
            Assert.AreEqual(".0000000000", 0.Fmt("##.#.##.###.###0", _nfi), "#01");
            Assert.AreEqual("1.0000000000", 1.Fmt("##.#.##.###.###0", _nfi), "#02");
            Assert.AreEqual("-1.0000000000", (-1).Fmt("##.#.##.###.###0", _nfi), "#03");
        }

        [Test]
        public void Test08015()
        {
            Assert.AreEqual(".000000000", 0.Fmt("##.#.##.###.##0#", _nfi), "#01");
            Assert.AreEqual("1.000000000", 1.Fmt("##.#.##.###.##0#", _nfi), "#02");
            Assert.AreEqual("-1.000000000", (-1).Fmt("##.#.##.###.##0#", _nfi), "#03");
        }

        [Test]
        public void Test08016()
        {
            Assert.AreEqual(".000000000", 0.Fmt("##.#.##.##0.##0#", _nfi), "#01");
            Assert.AreEqual("1.000000000", 1.Fmt("##.#.##.##0.##0#", _nfi), "#02");
            Assert.AreEqual("-1.000000000", (-1).Fmt("##.#.##.##0.##0#", _nfi), "#03");
        }

        [Test]
        public void Test08017()
        {
            Assert.AreEqual("0.000000000", 0.Fmt("#0.#.##.##0.##0#", _nfi), "#01");
            Assert.AreEqual("1.000000000", 1.Fmt("#0.#.##.##0.##0#", _nfi), "#02");
            Assert.AreEqual("-1.000000000", (-1).Fmt("#0.#.##.##0.##0#", _nfi), "#03");
        }

        [Test]
        public void Test08018()
        {
            Assert.AreEqual("-0002147484", int.MinValue.Fmt("0000000000,", _nfi), "#01");
            Assert.AreEqual("-0000002147", int.MinValue.Fmt("0000000000,,", _nfi), "#02");
            Assert.AreEqual("-0000000002", int.MinValue.Fmt("0000000000,,,", _nfi), "#03");
            Assert.AreEqual("0000000000", int.MinValue.Fmt("0000000000,,,,", _nfi), "#04");
            Assert.AreEqual("0000000000",
                int.MinValue.Fmt(
                    "0000000000,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,",
                    _nfi), "#05");
        }

        [Test]
        public void Test08019()
        {
            Assert.AreEqual("-2147483648", int.MinValue.Fmt(",0000000000", _nfi), "#01");
        }

        [Test]
        public void Test08020()
        {
            Assert.AreEqual("-0002147484", int.MinValue.Fmt(",0000000000,", _nfi), "#01");
        }

        [Test]
        public void Test08021()
        {
            Assert.AreEqual("-02,147,483,648", int.MinValue.Fmt("0,0000000000", _nfi), "#01");
        }

        [Test]
        public void Test08022()
        {
            Assert.AreEqual("-02,147,483,648", int.MinValue.Fmt("0000000000,0", _nfi), "#01");
        }

        [Test]
        public void Test08023()
        {
            Assert.AreEqual("-02,147,483,648", int.MinValue.Fmt("0,0,0,0,0,0,0,0,0,0,0", _nfi), "#01");
        }

        [Test]
        public void Test08024()
        {
            Assert.AreEqual("-02,147,483,648", int.MinValue.Fmt(",0,0,0,0,0,0,0,0,0,0,0", _nfi), "#01");
        }

        [Test]
        public void Test08025()
        {
            Assert.AreEqual("-00,002,147,484", int.MinValue.Fmt("0,0,0,0,0,0,0,0,0,0,0,", _nfi), "#01");
        }

        [Test]
        public void Test08026()
        {
            Assert.AreEqual("-00,002,147,484", int.MinValue.Fmt(",0,0,0,0,0,0,0,0,0,0,0,", _nfi), "#01");
        }

        [Test]
        public void Test08027()
        {
            Assert.AreEqual("-", int.MinValue.Fmt(",", _nfi), "#01");
        }

        [Test]
        public void Test08028()
        {
            Assert.AreEqual("-2147483648", int.MinValue.Fmt(",##########", _nfi), "#01");
        }

        [Test]
        public void Test08029()
        {
            Assert.AreEqual("-2147484", int.MinValue.Fmt(",##########,", _nfi), "#01");
        }

        [Test]
        public void Test08030()
        {
            Assert.AreEqual("-2,147,483,648", int.MinValue.Fmt("#,##########", _nfi), "#01");
        }

        [Test]
        public void Test08031()
        {
            Assert.AreEqual("-2,147,483,648", int.MinValue.Fmt("##########,#", _nfi), "#01");
        }

        [Test]
        public void Test08032()
        {
            Assert.AreEqual("-2,147,483,648", int.MinValue.Fmt("#,#,#,#,#,#,#,#,#,#,#", _nfi), "#01");
        }

        [Test]
        public void Test08033()
        {
            Assert.AreEqual("-2,147,483,648", int.MinValue.Fmt(",#,#,#,#,#,#,#,#,#,#,#", _nfi), "#01");
        }

        [Test]
        public void Test08034()
        {
            Assert.AreEqual("-2,147,484", int.MinValue.Fmt("#,#,#,#,#,#,#,#,#,#,#,", _nfi), "#01");
        }

        [Test]
        public void Test08035()
        {
            Assert.AreEqual("-2,147,484", int.MinValue.Fmt(",#,#,#,#,#,#,#,#,#,#,#,", _nfi), "#01");
        }

        [Test]
        public void Test08036()
        {
            Assert.AreEqual("-1", (-1000).Fmt("##########,", _nfi), "#01");
        }

        [Test]
        public void Test08037()
        {
            Assert.AreEqual("", (-100).Fmt("##########,", _nfi), "#01");
        }

        [Test]
        public void Test08038()
        {
            Assert.AreEqual("-%", int.MinValue.Fmt("%", _nfi), "#01");
        }

        [Test]
        public void Test08039()
        {
            Assert.AreEqual("-214748364800%", int.MinValue.Fmt("0%", _nfi), "#01");
        }

        [Test]
        public void Test08040()
        {
            Assert.AreEqual("-%214748364800", int.MinValue.Fmt("%0", _nfi), "#01");
        }

        [Test]
        public void Test08041()
        {
            Assert.AreEqual("-%21474836480000%", int.MinValue.Fmt("%0%", _nfi), "#01");
        }

        [Test]
        public void Test08042()
        {
            Assert.AreEqual("- % 21474836480000 % ", int.MinValue.Fmt(" % 0 % ", _nfi), "#01");
        }

        [Test]
        public void Test08043()
        {
            Assert.AreEqual("-214748365%", int.MinValue.Fmt("0%,", _nfi), "#01");
        }

        [Test]
        public void Test08044()
        {
            Assert.AreEqual("-214748365%", int.MinValue.Fmt("0,%", _nfi), "#01");
        }

        [Test]
        public void Test08045()
        {
            Assert.AreEqual("-%214748364800", int.MinValue.Fmt(",%0", _nfi), "#01");
        }

        [Test]
        public void Test08046()
        {
            Assert.AreEqual("-%214748364800", int.MinValue.Fmt("%,0", _nfi), "#01");
        }

        [Test]
        public void Test08047()
        {
            Assert.AreEqual("-2147483648%%%%%%", int.MinValue.Fmt("0,,,,%%%%%%", _nfi), "#01");
        }

        [Test]
        public void Test08048()
        {
            Assert.AreEqual("-2147483648%%%%%%", int.MinValue.Fmt("0%%%%%%,,,,", _nfi), "#01");
        }

        [Test]
        public void Test08049()
        {
            Assert.AreEqual("-%%%%%%2147483648", int.MinValue.Fmt("%%%%%%0,,,,", _nfi), "#01");
        }

        [Test]
        public void Test08050()
        {
            Assert.AreEqual("E+0", int.MinValue.Fmt("E+0", _nfi), "#01");
            Assert.AreEqual("e+0", int.MinValue.Fmt("e+0", _nfi), "#02");
            Assert.AreEqual("E0", int.MinValue.Fmt("E-0", _nfi), "#03");
            Assert.AreEqual("e0", int.MinValue.Fmt("e-0", _nfi), "#04");
        }

        [Test]
        public void Test08051()
        {
            Assert.AreEqual("-2E+9", int.MinValue.Fmt("0E+0", _nfi), "#01");
            Assert.AreEqual("-2e+9", int.MinValue.Fmt("0e+0", _nfi), "#02");
            Assert.AreEqual("-2E9", int.MinValue.Fmt("0E-0", _nfi), "#03");
            Assert.AreEqual("-2e9", int.MinValue.Fmt("0e-0", _nfi), "#04");
            Assert.AreEqual("-2E9", int.MinValue.Fmt("0E0", _nfi), "#05");
            Assert.AreEqual("-2e9", int.MinValue.Fmt("0e0", _nfi), "#06");
        }

        [Test]
        public void Test08052()
        {
            Assert.AreEqual("-2E+9", int.MinValue.Fmt("#E+0", _nfi), "#01");
            Assert.AreEqual("-2e+9", int.MinValue.Fmt("#e+0", _nfi), "#02");
            Assert.AreEqual("-2E9", int.MinValue.Fmt("#E-0", _nfi), "#03");
            Assert.AreEqual("-2e9", int.MinValue.Fmt("#e-0", _nfi), "#04");
            Assert.AreEqual("-2E9", int.MinValue.Fmt("#E0", _nfi), "#05");
            Assert.AreEqual("-2e9", int.MinValue.Fmt("#e0", _nfi), "#06");
        }

        [Test]
        public void Test08053()
        {
            Assert.AreEqual("-2147483648E+0", int.MinValue.Fmt("0000000000E+0", _nfi), "#01");
            Assert.AreEqual("-2147483648e+0", int.MinValue.Fmt("0000000000e+0", _nfi), "#02");
            Assert.AreEqual("-2147483648E0", int.MinValue.Fmt("0000000000E-0", _nfi), "#03");
            Assert.AreEqual("-2147483648e0", int.MinValue.Fmt("0000000000e-0", _nfi), "#04");
            Assert.AreEqual("-2147483648E0", int.MinValue.Fmt("0000000000E0", _nfi), "#05");
            Assert.AreEqual("-2147483648e0", int.MinValue.Fmt("0000000000e0", _nfi), "#06");
        }

        [Test]
        public void Test08054()
        {
            Assert.AreEqual("-21474836480E-1", int.MinValue.Fmt("00000000000E+0", _nfi), "#01");
            Assert.AreEqual("-21474836480e-1", int.MinValue.Fmt("00000000000e+0", _nfi), "#02");
            Assert.AreEqual("-21474836480E-1", int.MinValue.Fmt("00000000000E-0", _nfi), "#03");
            Assert.AreEqual("-21474836480e-1", int.MinValue.Fmt("00000000000e-0", _nfi), "#04");
            Assert.AreEqual("-21474836480E-1", int.MinValue.Fmt("00000000000E0", _nfi), "#05");
            Assert.AreEqual("-21474836480e-1", int.MinValue.Fmt("00000000000e0", _nfi), "#06");
        }

        [Test]
        public void Test08055()
        {
            Assert.AreEqual("-214748365E+1", int.MinValue.Fmt("000000000E+0", _nfi), "#01");
            Assert.AreEqual("-214748365e+1", int.MinValue.Fmt("000000000e+0", _nfi), "#02");
            Assert.AreEqual("-214748365E1", int.MinValue.Fmt("000000000E-0", _nfi), "#03");
            Assert.AreEqual("-214748365e1", int.MinValue.Fmt("000000000e-0", _nfi), "#04");
            Assert.AreEqual("-214748365E1", int.MinValue.Fmt("000000000E0", _nfi), "#05");
            Assert.AreEqual("-214748365e1", int.MinValue.Fmt("000000000e0", _nfi), "#06");
        }

        [Test]
        public void Test08056()
        {
            Assert.AreEqual("-21474836E+2", int.MinValue.Fmt("00000000E+0", _nfi), "#01");
            Assert.AreEqual("-21474836e+2", int.MinValue.Fmt("00000000e+0", _nfi), "#02");
            Assert.AreEqual("-21474836E2", int.MinValue.Fmt("00000000E-0", _nfi), "#03");
            Assert.AreEqual("-21474836e2", int.MinValue.Fmt("00000000e-0", _nfi), "#04");
            Assert.AreEqual("-21474836E2", int.MinValue.Fmt("00000000E0", _nfi), "#05");
            Assert.AreEqual("-21474836e2", int.MinValue.Fmt("00000000e0", _nfi), "#06");
        }

        [Test]
        public void Test08057()
        {
            Assert.AreEqual("-2147483648E+00", int.MinValue.Fmt("0000000000E+00", _nfi), "#01");
            Assert.AreEqual("-2147483648e+00", int.MinValue.Fmt("0000000000e+00", _nfi), "#02");
            Assert.AreEqual("-2147483648E00", int.MinValue.Fmt("0000000000E-00", _nfi), "#03");
            Assert.AreEqual("-2147483648e00", int.MinValue.Fmt("0000000000e-00", _nfi), "#04");
            Assert.AreEqual("-2147483648E00", int.MinValue.Fmt("0000000000E00", _nfi), "#05");
            Assert.AreEqual("-2147483648e00", int.MinValue.Fmt("0000000000e00", _nfi), "#06");
        }

        [Test]
        public void Test08058()
        {
            Assert.AreEqual("-2147483648E+02%", int.MinValue.Fmt("0000000000E+00%", _nfi), "#01");
            Assert.AreEqual("-2147483648e+02%", int.MinValue.Fmt("0000000000e+00%", _nfi), "#02");
            Assert.AreEqual("-2147483648E02%", int.MinValue.Fmt("0000000000E-00%", _nfi), "#03");
            Assert.AreEqual("-2147483648e02%", int.MinValue.Fmt("0000000000e-00%", _nfi), "#04");
            Assert.AreEqual("-2147483648E02%", int.MinValue.Fmt("0000000000E00%", _nfi), "#05");
            Assert.AreEqual("-2147483648e02%", int.MinValue.Fmt("0000000000e00%", _nfi), "#06");
        }

        [Test]
        public void Test08059()
        {
            Assert.AreEqual("-2147483648E+10%%%%%", int.MinValue.Fmt("0000000000E+00%%%%%", _nfi), "#01");
            Assert.AreEqual("-2147483648e+10%%%%%", int.MinValue.Fmt("0000000000e+00%%%%%", _nfi), "#02");
            Assert.AreEqual("-2147483648E10%%%%%", int.MinValue.Fmt("0000000000E-00%%%%%", _nfi), "#03");
            Assert.AreEqual("-2147483648e10%%%%%", int.MinValue.Fmt("0000000000e-00%%%%%", _nfi), "#04");
            Assert.AreEqual("-2147483648E10%%%%%", int.MinValue.Fmt("0000000000E00%%%%%", _nfi), "#05");
            Assert.AreEqual("-2147483648e10%%%%%", int.MinValue.Fmt("0000000000e00%%%%%", _nfi), "#06");
        }

        [Test]
        public void Test08060()
        {
            Assert.AreEqual("-2147483648E-03", int.MinValue.Fmt("0000000000E+00,", _nfi), "#01");
            Assert.AreEqual("-2147483648e-03", int.MinValue.Fmt("0000000000e+00,", _nfi), "#02");
            Assert.AreEqual("-2147483648E-03", int.MinValue.Fmt("0000000000E-00,", _nfi), "#03");
            Assert.AreEqual("-2147483648e-03", int.MinValue.Fmt("0000000000e-00,", _nfi), "#04");
            Assert.AreEqual("-2147483648E-03", int.MinValue.Fmt("0000000000E00,", _nfi), "#05");
            Assert.AreEqual("-2147483648e-03", int.MinValue.Fmt("0000000000e00,", _nfi), "#06");
        }

        [Test]
        public void Test08061()
        {
            Assert.AreEqual("-2147483648E-12", int.MinValue.Fmt("0000000000E+00,,,,", _nfi), "#01");
            Assert.AreEqual("-2147483648e-12", int.MinValue.Fmt("0000000000e+00,,,,", _nfi), "#02");
            Assert.AreEqual("-2147483648E-12", int.MinValue.Fmt("0000000000E-00,,,,", _nfi), "#03");
            Assert.AreEqual("-2147483648e-12", int.MinValue.Fmt("0000000000e-00,,,,", _nfi), "#04");
            Assert.AreEqual("-2147483648E-12", int.MinValue.Fmt("0000000000E00,,,,", _nfi), "#05");
            Assert.AreEqual("-2147483648e-12", int.MinValue.Fmt("0000000000e00,,,,", _nfi), "#06");
        }

        [Test]
        public void Test08062()
        {
            Assert.AreEqual("-2147483648E-04%%%%", int.MinValue.Fmt("0000000000E+00,,,,%%%%", _nfi), "#01");
            Assert.AreEqual("-2147483648e-04%%%%", int.MinValue.Fmt("0000000000e+00,,,,%%%%", _nfi), "#02");
            Assert.AreEqual("-2147483648E-04%%%%", int.MinValue.Fmt("0000000000E-00,,,,%%%%", _nfi), "#03");
            Assert.AreEqual("-2147483648e-04%%%%", int.MinValue.Fmt("0000000000e-00,,,,%%%%", _nfi), "#04");
            Assert.AreEqual("-2147483648E-04%%%%", int.MinValue.Fmt("0000000000E00,,,,%%%%", _nfi), "#05");
            Assert.AreEqual("-2147483648e-04%%%%", int.MinValue.Fmt("0000000000e00,,,,%%%%", _nfi), "#06");
        }

        [Test]
        public void Test08063()
        {
            Assert.AreEqual("-2147483648E-07%%%%", int.MinValue.Fmt("0000000000,E+00,,,,%%%%", _nfi), "#01");
            Assert.AreEqual("-2147483648e-07%%%%", int.MinValue.Fmt("0000000000,e+00,,,,%%%%", _nfi), "#02");
            Assert.AreEqual("-2147483648E-07%%%%", int.MinValue.Fmt("0000000000,E-00,,,,%%%%", _nfi), "#03");
            Assert.AreEqual("-2147483648e-07%%%%", int.MinValue.Fmt("0000000000,e-00,,,,%%%%", _nfi), "#04");
            Assert.AreEqual("-2147483648E-07%%%%", int.MinValue.Fmt("0000000000,E00,,,,%%%%", _nfi), "#05");
            Assert.AreEqual("-2147483648e-07%%%%", int.MinValue.Fmt("0000000000,e00,,,,%%%%", _nfi), "#06");
        }

        [Test]
        public void Test08064()
        {
            Assert.AreEqual("-000,000,214,7E+48%%%%", int.MinValue.Fmt("0000000000,E,+00,,,,%%%%", _nfi), "#01");
            Assert.AreEqual("-000,000,214,7e+48%%%%", int.MinValue.Fmt("0000000000,e,+00,,,,%%%%", _nfi), "#02");
            Assert.AreEqual("-000,000,214,7E-48%%%%", int.MinValue.Fmt("0000000000,E,-00,,,,%%%%", _nfi), "#03");
            Assert.AreEqual("-000,000,214,7e-48%%%%", int.MinValue.Fmt("0000000000,e,-00,,,,%%%%", _nfi), "#04");
            Assert.AreEqual("-000,000,214,7E48%%%%", int.MinValue.Fmt("0000000000,E,00,,,,%%%%", _nfi), "#05");
            Assert.AreEqual("-000,000,214,7e48%%%%", int.MinValue.Fmt("0000000000,e,00,,,,%%%%", _nfi), "#06");
        }

        [Test]
        public void Test08065()
        {
            Assert.AreEqual("-000,000,214,7E+48%%%%", int.MinValue.Fmt("0000000000,E+,00,,,,%%%%", _nfi), "#01");
            Assert.AreEqual("-000,000,214,7e+48%%%%", int.MinValue.Fmt("0000000000,e+,00,,,,%%%%", _nfi), "#02");
            Assert.AreEqual("-000,000,214,7E-48%%%%", int.MinValue.Fmt("0000000000,E-,00,,,,%%%%", _nfi), "#03");
            Assert.AreEqual("-000,000,214,7e-48%%%%", int.MinValue.Fmt("0000000000,e-,00,,,,%%%%", _nfi), "#04");
        }

        [Test]
        public void Test08066()
        {
            Assert.AreEqual("-21,474,836,48E-50%%%%", int.MinValue.Fmt("0000000000,E+0,0,,,,%%%%", _nfi), "#01");
            Assert.AreEqual("-21,474,836,48e-50%%%%", int.MinValue.Fmt("0000000000,e+0,0,,,,%%%%", _nfi), "#02");
            Assert.AreEqual("-21,474,836,48E-50%%%%", int.MinValue.Fmt("0000000000,E-0,0,,,,%%%%", _nfi), "#03");
            Assert.AreEqual("-21,474,836,48e-50%%%%", int.MinValue.Fmt("0000000000,e-0,0,,,,%%%%", _nfi), "#04");
            Assert.AreEqual("-21,474,836,48E-50%%%%", int.MinValue.Fmt("0000000000,E0,0,,,,%%%%", _nfi), "#05");
            Assert.AreEqual("-21,474,836,48e-50%%%%", int.MinValue.Fmt("0000000000,e0,0,,,,%%%%", _nfi), "#06");
        }

        [Test]
        public void Test08067()
        {
            Assert.AreEqual("-2147483648E-01,%%%%", int.MinValue.Fmt(@"0000000000E+00\,,,,%%%%", _nfi), "#01");
            Assert.AreEqual("-2147483648e-01,%%%%", int.MinValue.Fmt(@"0000000000e+00\,,,,%%%%", _nfi), "#02");
            Assert.AreEqual("-2147483648E-01,%%%%", int.MinValue.Fmt(@"0000000000E-00\,,,,%%%%", _nfi), "#03");
            Assert.AreEqual("-2147483648e-01,%%%%", int.MinValue.Fmt(@"0000000000e-00\,,,,%%%%", _nfi), "#04");
            Assert.AreEqual("-2147483648E-01,%%%%", int.MinValue.Fmt(@"0000000000E00\,,,,%%%%", _nfi), "#05");
            Assert.AreEqual("-2147483648e-01,%%%%", int.MinValue.Fmt(@"0000000000e00\,,,,%%%%", _nfi), "#06");
        }

        [Test]
        public void Test08068()
        {
            Assert.AreEqual("-2147483648E+02,,%%%%", int.MinValue.Fmt(@"0000000000E+00\,,,\,%%%%", _nfi), "#01");
            Assert.AreEqual("-2147483648e+02,,%%%%", int.MinValue.Fmt(@"0000000000e+00\,,,\,%%%%", _nfi), "#02");
            Assert.AreEqual("-2147483648E02,,%%%%", int.MinValue.Fmt(@"0000000000E-00\,,,\,%%%%", _nfi), "#03");
            Assert.AreEqual("-2147483648e02,,%%%%", int.MinValue.Fmt(@"0000000000e-00\,,,\,%%%%", _nfi), "#04");
            Assert.AreEqual("-2147483648E02,,%%%%", int.MinValue.Fmt(@"0000000000E00\,,,\,%%%%", _nfi), "#05");
            Assert.AreEqual("-2147483648e02,,%%%%", int.MinValue.Fmt(@"0000000000e00\,,,\,%%%%", _nfi), "#06");
        }

        [Test]
        public void Test08069()
        {
            Assert.AreEqual("-2147483648E+00,,%%%%", int.MinValue.Fmt(@"0000000000E+00\,,,\,\%%%%", _nfi), "#01");
            Assert.AreEqual("-2147483648e+00,,%%%%", int.MinValue.Fmt(@"0000000000e+00\,,,\,\%%%%", _nfi), "#02");
            Assert.AreEqual("-2147483648E00,,%%%%", int.MinValue.Fmt(@"0000000000E-00\,,,\,\%%%%", _nfi), "#03");
            Assert.AreEqual("-2147483648e00,,%%%%", int.MinValue.Fmt(@"0000000000e-00\,,,\,\%%%%", _nfi), "#04");
            Assert.AreEqual("-2147483648E00,,%%%%", int.MinValue.Fmt(@"0000000000E00\,,,\,\%%%%", _nfi), "#05");
            Assert.AreEqual("-2147483648e00,,%%%%", int.MinValue.Fmt(@"0000000000e00\,,,\,\%%%%", _nfi), "#06");
        }

        [Test]
        public void Test08070()
        {
            Assert.AreEqual("-2147483648E-02,,%%%%", int.MinValue.Fmt(@"0000000000E+00\,,,\,\%%%\%", _nfi), "#01");
            Assert.AreEqual("-2147483648e-02,,%%%%", int.MinValue.Fmt(@"0000000000e+00\,,,\,\%%%\%", _nfi), "#02");
            Assert.AreEqual("-2147483648E-02,,%%%%", int.MinValue.Fmt(@"0000000000E-00\,,,\,\%%%\%", _nfi), "#03");
            Assert.AreEqual("-2147483648e-02,,%%%%", int.MinValue.Fmt(@"0000000000e-00\,,,\,\%%%\%", _nfi), "#04");
            Assert.AreEqual("-2147483648E-02,,%%%%", int.MinValue.Fmt(@"0000000000E00\,,,\,\%%%\%", _nfi), "#05");
            Assert.AreEqual("-2147483648e-02,,%%%%", int.MinValue.Fmt(@"0000000000e00\,,,\,\%%%\%", _nfi), "#06");
        }

        [Test]
        public void Test08071()
        {
            Assert.AreEqual(@"-2147483648E-04\\\%%%\%", int.MinValue.Fmt(@"0000000000E+00\\,,,\\,\\%%%\\%", _nfi), "#01");
            Assert.AreEqual(@"-2147483648e-04\\\%%%\%", int.MinValue.Fmt(@"0000000000e+00\\,,,\\,\\%%%\\%", _nfi), "#02");
            Assert.AreEqual(@"-2147483648E-04\\\%%%\%", int.MinValue.Fmt(@"0000000000E-00\\,,,\\,\\%%%\\%", _nfi), "#03");
            Assert.AreEqual(@"-2147483648e-04\\\%%%\%", int.MinValue.Fmt(@"0000000000e-00\\,,,\\,\\%%%\\%", _nfi), "#04");
            Assert.AreEqual(@"-2147483648E-04\\\%%%\%", int.MinValue.Fmt(@"0000000000E00\\,,,\\,\\%%%\\%", _nfi), "#05");
            Assert.AreEqual(@"-2147483648e-04\\\%%%\%", int.MinValue.Fmt(@"0000000000e00\\,,,\\,\\%%%\\%", _nfi), "#06");
        }

        [Test]
        public void Test08072()
        {
            Assert.AreEqual(@"-2147483648E+00\,\,\%%%\%", int.MinValue.Fmt(@"0000000000E+00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#01");
            Assert.AreEqual(@"-2147483648e+00\,\,\%%%\%", int.MinValue.Fmt(@"0000000000e+00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#02");
            Assert.AreEqual(@"-2147483648E00\,\,\%%%\%", int.MinValue.Fmt(@"0000000000E-00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#03");
            Assert.AreEqual(@"-2147483648e00\,\,\%%%\%", int.MinValue.Fmt(@"0000000000e-00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#04");
            Assert.AreEqual(@"-2147483648E00\,\,\%%%\%", int.MinValue.Fmt(@"0000000000E00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#05");
            Assert.AreEqual(@"-2147483648e00\,\,\%%%\%", int.MinValue.Fmt(@"0000000000e00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#06");
        }

        [Test]
        public void Test08073()
        {
            Assert.AreEqual(@"-0021474836E+48\,\,\%%%\%", int.MinValue.Fmt(@"0000000000\E+00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#01");
            Assert.AreEqual(@"-0021474836e+48\,\,\%%%\%", int.MinValue.Fmt(@"0000000000\e+00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#02");
            Assert.AreEqual(@"-0021474836E-48\,\,\%%%\%", int.MinValue.Fmt(@"0000000000\E-00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#03");
            Assert.AreEqual(@"-0021474836e-48\,\,\%%%\%", int.MinValue.Fmt(@"0000000000\e-00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#04");
            Assert.AreEqual(@"-0021474836E48\,\,\%%%\%", int.MinValue.Fmt(@"0000000000\E00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#05");
            Assert.AreEqual(@"-0021474836e48\,\,\%%%\%", int.MinValue.Fmt(@"0000000000\e00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#06");
        }

        [Test]
        public void Test08074()
        {
            Assert.AreEqual(@"-0021474836E+48\,\,\%%%\%", int.MinValue.Fmt(@"0000000000E\+00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#01");
            Assert.AreEqual(@"-0021474836e+48\,\,\%%%\%", int.MinValue.Fmt(@"0000000000e\+00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#02");
            Assert.AreEqual(@"-0021474836E-48\,\,\%%%\%", int.MinValue.Fmt(@"0000000000E\-00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#03");
            Assert.AreEqual(@"-0021474836e-48\,\,\%%%\%", int.MinValue.Fmt(@"0000000000e\-00\\,\,,\\\,\\%%%\\\%", _nfi),
                "#04");
        }

        [Test]
        public void Test08075()
        {
            Assert.AreEqual("-2147483648E-03,%%%%", int.MinValue.Fmt("0000000000E+00,,,',%'%%%", _nfi), "#01");
            Assert.AreEqual("-2147483648e-03,%%%%", int.MinValue.Fmt("0000000000e+00,,,',%'%%%", _nfi), "#02");
            Assert.AreEqual("-2147483648E-03,%%%%", int.MinValue.Fmt("0000000000E-00,,,',%'%%%", _nfi), "#03");
            Assert.AreEqual("-2147483648e-03,%%%%", int.MinValue.Fmt("0000000000e-00,,,',%'%%%", _nfi), "#04");
            Assert.AreEqual("-2147483648E-03,%%%%", int.MinValue.Fmt("0000000000E00,,,',%'%%%", _nfi), "#05");
            Assert.AreEqual("-2147483648e-03,%%%%", int.MinValue.Fmt("0000000000e00,,,',%'%%%", _nfi), "#06");
        }

        [Test]
        public void Test08076()
        {
            Assert.AreEqual("-2147483648E-03,%%%%", int.MinValue.Fmt("0000000000E+00,,,\",%\"%%%", _nfi), "#01");
            Assert.AreEqual("-2147483648e-03,%%%%", int.MinValue.Fmt("0000000000e+00,,,\",%\"%%%", _nfi), "#02");
            Assert.AreEqual("-2147483648E-03,%%%%", int.MinValue.Fmt("0000000000E-00,,,\",%\"%%%", _nfi), "#03");
            Assert.AreEqual("-2147483648e-03,%%%%", int.MinValue.Fmt("0000000000e-00,,,\",%\"%%%", _nfi), "#04");
            Assert.AreEqual("-2147483648E-03,%%%%", int.MinValue.Fmt("0000000000E00,,,\",%\"%%%", _nfi), "#05");
            Assert.AreEqual("-2147483648e-03,%%%%", int.MinValue.Fmt("0000000000e00,,,\",%\"%%%", _nfi), "#06");
        }

        [Test]
        public void Test08077()
        {
            Assert.AreEqual("-", int.MinValue.Fmt(";", _nfi), "#01");
            Assert.AreEqual("", int.MaxValue.Fmt(";", _nfi), "#02");
            Assert.AreEqual("", 0.Fmt(";", _nfi), "#03");
        }

        [Test]
        public void Test08078()
        {
            Assert.AreEqual("-2,147,483,648", int.MinValue.Fmt("#,#;", _nfi), "#01");
            Assert.AreEqual("2,147,483,647", int.MaxValue.Fmt("#,#;", _nfi), "#02");
            Assert.AreEqual("", 0.Fmt("#,#;", _nfi), "#03");
        }

        [Test]
        public void Test08079()
        {
            Assert.AreEqual("2,147,483,648", int.MinValue.Fmt(";#,#", _nfi), "#01");
            Assert.AreEqual("", int.MaxValue.Fmt(";#,#", _nfi), "#02");
            Assert.AreEqual("", 0.Fmt(";#,#", _nfi), "#03");
        }

        [Test]
        public void Test08080()
        {
            Assert.AreEqual("2,147,483,648", int.MinValue.Fmt("0000000000,.0000000000;#,#", _nfi), "#01");
            Assert.AreEqual("0002147483.6470000000", int.MaxValue.Fmt("0000000000,.0000000000;#,#", _nfi), "#02");
            Assert.AreEqual("0000000000.0000000000", 0.Fmt("0000000000,.0000000000;#,#", _nfi), "#03");
        }

        [Test]
        public void Test08081()
        {
            Assert.AreEqual("-", int.MinValue.Fmt(";;", _nfi), "#01");
            Assert.AreEqual("", int.MaxValue.Fmt(";;", _nfi), "#02");
            Assert.AreEqual("", 0.Fmt(";;", _nfi), "#03");
        }

        [Test]
        public void Test08082()
        {
            Assert.AreEqual("-", int.MinValue.Fmt(";;0%", _nfi), "#01");
            Assert.AreEqual("", int.MaxValue.Fmt(";;0%", _nfi), "#02");
            Assert.AreEqual("0%", 0.Fmt(";;0%", _nfi), "#03");
        }

        [Test]
        public void Test08083()
        {
            Assert.AreEqual("2147484", int.MinValue.Fmt(";0,;0%", _nfi), "#01");
            Assert.AreEqual("", int.MaxValue.Fmt(";0,;0%", _nfi), "#02");
            Assert.AreEqual("0%", 0.Fmt(";0,;0%", _nfi), "#03");
        }

        [Test]
        public void Test08084()
        {
            Assert.AreEqual("2147484", int.MinValue.Fmt("0E+0;0,;0%", _nfi), "#01");
            Assert.AreEqual("2E+9", int.MaxValue.Fmt("0E+0;0,;0%", _nfi), "#02");
            Assert.AreEqual("0%", 0.Fmt("0E+0;0,;0%", _nfi), "#03");
        }

        [Test]
        public void Test08085()
        {
            Assert.AreEqual("214,748,364,80;0%", int.MinValue.Fmt(@"0E+0;0,\;0%", _nfi), "#01");
            Assert.AreEqual("2E+9", int.MaxValue.Fmt(@"0E+0;0,\;0%", _nfi), "#02");
            Assert.AreEqual("0E+0", 0.Fmt(@"0E+0;0,\;0%", _nfi), "#03");
        }

        [Test]
        public void Test08086()
        {
            Assert.AreEqual("214,748,364,80;0%", int.MinValue.Fmt("0E+0;0,\";\"0%", _nfi), "#01");
            Assert.AreEqual("2E+9", int.MaxValue.Fmt("0E+0;0,\";\"0%", _nfi), "#02");
            Assert.AreEqual("0E+0", 0.Fmt("0E+0;0,\";\"0%", _nfi), "#03");
        }

        [Test]
        public void Test08087()
        {
            // MS.NET bug?
            var nfi = NumberFormatInfo.InvariantInfo.Clone() as NumberFormatInfo;
            nfi.NumberDecimalSeparator = "$$$";
            Assert.AreEqual("-0000000000$$$2147483648", int.MinValue.Fmt("0000000000$$$0000000000", nfi), "#01");
        }

        [Test]
        public void Test08088()
        {
            // MS.NET bug?
            var nfi = NumberFormatInfo.InvariantInfo.Clone() as NumberFormatInfo;
            nfi.NumberGroupSeparator = "$$$";
            Assert.AreEqual("-0000000000$$$2147483648", int.MinValue.Fmt("0000000000$$$0000000000", nfi), "#01");
        }

        [Test]
        public void Test08089()
        {
            var nfi = NumberFormatInfo.InvariantInfo.Clone() as NumberFormatInfo;
            nfi.NumberGroupSizes = new int[] {3, 2, 1, 0};
            Assert.AreEqual("-00000000002147,4,83,648", int.MinValue.Fmt("0000000000,0000000000", nfi), "#01");
        }

        [Test]
        public void Test08090()
        {
            // MS.NET bug?
            var nfi = NumberFormatInfo.InvariantInfo.Clone() as NumberFormatInfo;
            nfi.PercentSymbol = "$$$";
            Assert.AreEqual("-0000000000$$$2147483648", int.MinValue.Fmt("0000000000$$$0000000000", nfi), "#01");
        }

        [Test]
        public void Test08091()
        {
            // MS.NET bug?
            Assert.AreEqual("B2147", int.MinValue.Fmt("A0,;B0,,;C0,,,;D0,,,,;E0,,,,,", _nfi), "#01");
            Assert.AreEqual("A2147484", int.MaxValue.Fmt("A0,;B0,,;C0,,,;D0,,,,;E0,,,,,", _nfi), "#02");
            Assert.AreEqual("C0", 0.Fmt("A0,;B0,,;C0,,,;D0,,,,;E0,,,,,", _nfi), "#03");
        }

        // Test10000- Double and D
        [Test]
        [ExpectedException(typeof (FormatException))]
        public void Test10000()
        {
            Assert.AreEqual("0", 0.0.Fmt("D", _nfi), "#01");
        }

        // Test11000- Double and E
        [Test]
        public void Test11000()
        {
            Assert.AreEqual("0.000000E+000", 0.0.Fmt("E", _nfi), "#01");
            Assert.AreEqual("0.000000e+000", 0.0.Fmt("e", _nfi), "#02");
            Assert.AreEqual("-1.797693E+308", double.MinValue.Fmt("E", _nfi), "#03");
            Assert.AreEqual("-1.797693e+308", double.MinValue.Fmt("e", _nfi), "#04");
            Assert.AreEqual("1.797693E+308", double.MaxValue.Fmt("E", _nfi), "#05");
            Assert.AreEqual("1.797693e+308", double.MaxValue.Fmt("e", _nfi), "#06");
        }

        [Test]
        public void Test11001()
        {
            Assert.AreEqual("E ", 0.0.Fmt("E ", _nfi), "#01");
            Assert.AreEqual(" E", 0.0.Fmt(" E", _nfi), "#02");
            Assert.AreEqual(" E ", 0.0.Fmt(" E ", _nfi), "#03");
        }

        [Test]
        public void Test11002()
        {
            Assert.AreEqual("-E ", (-1.0).Fmt("E ", _nfi), "#01");
            Assert.AreEqual("- E", (-1.0).Fmt(" E", _nfi), "#02");
            Assert.AreEqual("- E ", (-1.0).Fmt(" E ", _nfi), "#03");
        }

        [Test]
        public void Test11003()
        {
            Assert.AreEqual("0E+000", 0.0.Fmt("E0", _nfi), "#01");
            Assert.AreEqual("0.0000000000000000E+000", 0.0.Fmt("E16", _nfi), "#02");
            Assert.AreEqual("0.00000000000000000E+000", 0.0.Fmt("E17", _nfi), "#03");
            Assert.AreEqual(
                "0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000E+000",
                0.0.Fmt("E99", _nfi), "#04");
            Assert.AreEqual("E100", 0.0.Fmt("E100", _nfi), "#05");
        }

        [Test]
        public void Test11004()
        {
            Assert.AreEqual("2E+308", double.MaxValue.Fmt("E0", _nfi), "#01");
            Assert.AreEqual("1.7976931348623157E+308", double.MaxValue.Fmt("E16", _nfi), "#02");
            Assert.AreEqual("1.79769313486231570E+308", double.MaxValue.Fmt("E17", _nfi), "#03");
            Assert.AreEqual(
                "1.797693134862315700000000000000000000000000000000000000000000000000000000000000000000000000000000000E+308",
                double.MaxValue.Fmt("E99", _nfi), "#04");
            Assert.AreEqual(
                "E1179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("E100", _nfi), "#05");
        }

        [Test]
        public void Test11005()
        {
            Assert.AreEqual("-2E+308", double.MinValue.Fmt("E0", _nfi), "#01");
            Assert.AreEqual("-1.7976931348623157E+308", double.MinValue.Fmt("E16", _nfi), "#02");
            Assert.AreEqual("-1.79769313486231570E+308", double.MinValue.Fmt("E17", _nfi), "#03");
            Assert.AreEqual(
                "-1.797693134862315700000000000000000000000000000000000000000000000000000000000000000000000000000000000E+308",
                double.MinValue.Fmt("E99", _nfi), "#04");
            Assert.AreEqual(
                "-E1179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("E100", _nfi), "#05");
        }

        [Test]
        public void Test11006()
        {
            Assert.AreEqual("EF", 0.0.Fmt("EF", _nfi), "#01");
            Assert.AreEqual("E0F", 0.0.Fmt("E0F", _nfi), "#02");
            Assert.AreEqual("E0xF", 0.0.Fmt("E0xF", _nfi), "#03");
        }

        [Test]
        public void Test11007()
        {
            Assert.AreEqual("EF", double.MaxValue.Fmt("EF", _nfi), "#01");
            Assert.AreEqual("E0F", double.MaxValue.Fmt("E0F", _nfi), "#02");
            Assert.AreEqual("E0xF", double.MaxValue.Fmt("E0xF", _nfi), "#03");
        }

        [Test]
        public void Test11008()
        {
            Assert.AreEqual("-EF", double.MinValue.Fmt("EF", _nfi), "#01");
            Assert.AreEqual("E0F", double.MinValue.Fmt("E0F", _nfi), "#02");
            Assert.AreEqual("E0xF", double.MinValue.Fmt("E0xF", _nfi), "#03");
        }

        [Test]
        public void Test11009()
        {
            Assert.AreEqual("0.00000000000000000E+000", 0.0.Fmt("E0000000000000000000000000000000000000017", _nfi),
                "#01");
            Assert.AreEqual("1.79769313486231570E+308",
                double.MaxValue.Fmt("E0000000000000000000000000000000000000017", _nfi), "#02");
            Assert.AreEqual("-1.79769313486231570E+308",
                double.MinValue.Fmt("E0000000000000000000000000000000000000017", _nfi), "#03");
        }

        [Test]
        public void Test11010()
        {
            Assert.AreEqual("+E", 0.0.Fmt("+E", _nfi), "#01");
            Assert.AreEqual("E+", 0.0.Fmt("E+", _nfi), "#02");
            Assert.AreEqual("+E+", 0.0.Fmt("+E+", _nfi), "#03");
        }

        [Test]
        public void Test11011()
        {
            Assert.AreEqual("+E", double.MaxValue.Fmt("+E", _nfi), "#01");
            Assert.AreEqual("E+", double.MaxValue.Fmt("E+", _nfi), "#02");
            Assert.AreEqual("+E+", double.MaxValue.Fmt("+E+", _nfi), "#03");
        }

        [Test]
        public void Test11012()
        {
            Assert.AreEqual("-+E", double.MinValue.Fmt("+E", _nfi), "#01");
            Assert.AreEqual("-E+", double.MinValue.Fmt("E+", _nfi), "#02");
            Assert.AreEqual("-+E+", double.MinValue.Fmt("+E+", _nfi), "#03");
        }

        [Test]
        public void Test11013()
        {
            Assert.AreEqual("-E", 0.0.Fmt("-E", _nfi), "#01");
            Assert.AreEqual("E-", 0.0.Fmt("E-", _nfi), "#02");
            Assert.AreEqual("-E-", 0.0.Fmt("-E-", _nfi), "#03");
        }

        [Test]
        public void Test11014()
        {
            Assert.AreEqual("-E", double.MaxValue.Fmt("-E", _nfi), "#01");
            Assert.AreEqual("E-", double.MaxValue.Fmt("E-", _nfi), "#02");
            Assert.AreEqual("-E-", double.MaxValue.Fmt("-E-", _nfi), "#03");
        }

        [Test]
        public void Test11015()
        {
            Assert.AreEqual("--E", double.MinValue.Fmt("-E", _nfi), "#01");
            Assert.AreEqual("-E-", double.MinValue.Fmt("E-", _nfi), "#02");
            Assert.AreEqual("--E-", double.MinValue.Fmt("-E-", _nfi), "#03");
        }

        [Test]
        public void Test11016()
        {
            Assert.AreEqual("E+0", 0.0.Fmt("E+0", _nfi), "#01");
            Assert.AreEqual("E+0", double.MaxValue.Fmt("E+0", _nfi), "#02");
            Assert.AreEqual("E+0", double.MinValue.Fmt("E+0", _nfi), "#03");
        }

        [Test]
        public void Test11017()
        {
            Assert.AreEqual("E+9", 0.0.Fmt("E+9", _nfi), "#01");
            Assert.AreEqual("E+9", double.MaxValue.Fmt("E+9", _nfi), "#02");
            Assert.AreEqual("-E+9", double.MinValue.Fmt("E+9", _nfi), "#03");
        }

        [Test]
        public void Test11018()
        {
            Assert.AreEqual("E-9", 0.0.Fmt("E-9", _nfi), "#01");
            Assert.AreEqual("E-9", double.MaxValue.Fmt("E-9", _nfi), "#02");
            Assert.AreEqual("-E-9", double.MinValue.Fmt("E-9", _nfi), "#03");
        }

        [Test]
        public void Test11019()
        {
            Assert.AreEqual("E0", 0.0.Fmt("E0,", _nfi), "#01");
            Assert.AreEqual("E0", double.MaxValue.Fmt("E0,", _nfi), "#02");
            Assert.AreEqual("E0", double.MinValue.Fmt("E0,", _nfi), "#03");
        }

        [Test]
        public void Test11020()
        {
            Assert.AreEqual("E0", 0.0.Fmt("E0.", _nfi), "#01");
            Assert.AreEqual("E0", double.MaxValue.Fmt("E0.", _nfi), "#02");
            Assert.AreEqual("E0", double.MinValue.Fmt("E0.", _nfi), "#03");
        }

        [Test]
        public void Test11021()
        {
            Assert.AreEqual("E0.0", 0.0.Fmt("E0.0", _nfi), "#01");
            Assert.AreEqual("E309.2", double.MaxValue.Fmt("E0.0", _nfi), "#02");
            Assert.AreEqual("-E309.2", double.MinValue.Fmt("E0.0", _nfi), "#03");
        }

        [Test]
        public void Test11022()
        {
            Assert.AreEqual("E09", 0.0.Fmt("E0.9", _nfi), "#01");
            Assert.AreEqual("E09", double.MaxValue.Fmt("E0.9", _nfi), "#02");
            Assert.AreEqual("E09", double.MinValue.Fmt("E0.9", _nfi), "#03");
        }

        [Test]
        public void Test11023()
        {
            Assert.AreEqual("1.1E+000", 1.05.Fmt("E1", _nfi), "#01");
            Assert.AreEqual("1.2E+000", 1.15.Fmt("E1", _nfi), "#02");
            Assert.AreEqual("1.3E+000", 1.25.Fmt("E1", _nfi), "#03");
            Assert.AreEqual("1.4E+000", 1.35.Fmt("E1", _nfi), "#04");
            Assert.AreEqual("1.5E+000", 1.45.Fmt("E1", _nfi), "#05");
            Assert.AreEqual("1.6E+000", 1.55.Fmt("E1", _nfi), "#06");
            Assert.AreEqual("1.7E+000", 1.65.Fmt("E1", _nfi), "#07");
            Assert.AreEqual("1.8E+000", 1.75.Fmt("E1", _nfi), "#08");
            Assert.AreEqual("1.9E+000", 1.85.Fmt("E1", _nfi), "#09");
            Assert.AreEqual("2.0E+000", 1.95.Fmt("E1", _nfi), "#10");
        }

        [Test]
        public void Test11024()
        {
            Assert.AreEqual("1.01E+000", 1.005.Fmt("E2", _nfi), "#01");
            Assert.AreEqual("1.02E+000", 1.015.Fmt("E2", _nfi), "#02");
            Assert.AreEqual("1.03E+000", 1.025.Fmt("E2", _nfi), "#03");
            Assert.AreEqual("1.04E+000", 1.035.Fmt("E2", _nfi), "#04");
            Assert.AreEqual("1.05E+000", 1.045.Fmt("E2", _nfi), "#05");
            Assert.AreEqual("1.06E+000", 1.055.Fmt("E2", _nfi), "#06");
            Assert.AreEqual("1.07E+000", 1.065.Fmt("E2", _nfi), "#07");
            Assert.AreEqual("1.08E+000", 1.075.Fmt("E2", _nfi), "#08");
            Assert.AreEqual("1.09E+000", 1.085.Fmt("E2", _nfi), "#09");
            Assert.AreEqual("1.10E+000", 1.095.Fmt("E2", _nfi), "#10");
        }

        [Test]
        public void Test11025()
        {
            Assert.AreEqual("1.00000000000001E+000", 1.000000000000005.Fmt("E14", _nfi), "#01");
            Assert.AreEqual("1.00000000000002E+000", 1.000000000000015.Fmt("E14", _nfi), "#02");
            Assert.AreEqual("1.00000000000003E+000", 1.000000000000025.Fmt("E14", _nfi), "#03");
            Assert.AreEqual("1.00000000000004E+000", 1.000000000000035.Fmt("E14", _nfi), "#04");
            Assert.AreEqual("1.00000000000005E+000", 1.000000000000045.Fmt("E14", _nfi), "#05");
            Assert.AreEqual("1.00000000000006E+000", 1.000000000000055.Fmt("E14", _nfi), "#06");
            Assert.AreEqual("1.00000000000007E+000", 1.000000000000065.Fmt("E14", _nfi), "#07");
            Assert.AreEqual("1.00000000000008E+000", 1.000000000000075.Fmt("E14", _nfi), "#08");
            Assert.AreEqual("1.00000000000009E+000", 1.000000000000085.Fmt("E14", _nfi), "#09");
            Assert.AreEqual("1.00000000000010E+000", 1.000000000000095.Fmt("E14", _nfi), "#10");
        }

        [Test]
        public void Test11026()
        {
            Assert.AreEqual("1.000000000000000E+000", 1.0000000000000005.Fmt("E15", _nfi), "#01");
            Assert.AreEqual("1.000000000000002E+000", 1.0000000000000015.Fmt("E15", _nfi), "#02");
            Assert.AreEqual("1.000000000000002E+000", 1.0000000000000025.Fmt("E15", _nfi), "#03");
            Assert.AreEqual("1.000000000000004E+000", 1.0000000000000035.Fmt("E15", _nfi), "#04");
            Assert.AreEqual("1.000000000000004E+000", 1.0000000000000045.Fmt("E15", _nfi), "#05");
            Assert.AreEqual("1.000000000000006E+000", 1.0000000000000055.Fmt("E15", _nfi), "#06");
            Assert.AreEqual("1.000000000000006E+000", 1.0000000000000065.Fmt("E15", _nfi), "#07");
            Assert.AreEqual("1.000000000000008E+000", 1.0000000000000075.Fmt("E15", _nfi), "#08");
            Assert.AreEqual("1.000000000000008E+000", 1.0000000000000085.Fmt("E15", _nfi), "#09");
            Assert.AreEqual("1.000000000000010E+000", 1.0000000000000095.Fmt("E15", _nfi), "#10");
        }

        [Test]
        public void Test11027()
        {
            Assert.AreEqual("1.0000000000000000E+000", 1.00000000000000005.Fmt("E16", _nfi), "#01");
            Assert.AreEqual("1.0000000000000002E+000", 1.00000000000000015.Fmt("E16", _nfi), "#02");
            Assert.AreEqual("1.0000000000000002E+000", 1.00000000000000025.Fmt("E16", _nfi), "#03");
            Assert.AreEqual("1.0000000000000004E+000", 1.00000000000000035.Fmt("E16", _nfi), "#04");
            Assert.AreEqual("1.0000000000000004E+000", 1.00000000000000045.Fmt("E16", _nfi), "#05");
            Assert.AreEqual("1.0000000000000004E+000", 1.00000000000000055.Fmt("E16", _nfi), "#06");
            Assert.AreEqual("1.0000000000000007E+000", 1.00000000000000065.Fmt("E16", _nfi), "#07");
            Assert.AreEqual("1.0000000000000007E+000", 1.00000000000000075.Fmt("E16", _nfi), "#08");
            Assert.AreEqual("1.0000000000000009E+000", 1.00000000000000085.Fmt("E16", _nfi), "#09");
            Assert.AreEqual("1.0000000000000009E+000", 1.00000000000000095.Fmt("E16", _nfi), "#10");
        }

        [Test]
        public void Test11028()
        {
            Assert.AreEqual("1.00000000000000000E+000", 1.000000000000000005.Fmt("E17", _nfi), "#01");
            Assert.AreEqual("1.00000000000000000E+000", 1.000000000000000015.Fmt("E17", _nfi), "#02");
            Assert.AreEqual("1.00000000000000000E+000", 1.000000000000000025.Fmt("E17", _nfi), "#03");
            Assert.AreEqual("1.00000000000000000E+000", 1.000000000000000035.Fmt("E17", _nfi), "#04");
            Assert.AreEqual("1.00000000000000000E+000", 1.000000000000000045.Fmt("E17", _nfi), "#05");
            Assert.AreEqual("1.00000000000000000E+000", 1.000000000000000055.Fmt("E17", _nfi), "#06");
            Assert.AreEqual("1.00000000000000000E+000", 1.000000000000000065.Fmt("E17", _nfi), "#07");
            Assert.AreEqual("1.00000000000000000E+000", 1.000000000000000075.Fmt("E17", _nfi), "#08");
            Assert.AreEqual("1.00000000000000000E+000", 1.000000000000000085.Fmt("E17", _nfi), "#09");
            Assert.AreEqual("1.00000000000000000E+000", 1.000000000000000095.Fmt("E17", _nfi), "#10");
        }

        [Test]
        public void Test11029()
        {
            Assert.AreEqual("1E+000",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("E0"), "#01");
            Assert.AreEqual("1.2345678901234567E+000",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("E16"), "#02");
            Assert.AreEqual("1.23456789012345670E+000",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("E17"), "#03");
            Assert.AreEqual(
                "1.234567890123456700000000000000000000000000000000000000000000000000000000000000000000000000000000000E+000",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("E99"), "#04");
            Assert.AreEqual("E101",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("E100"), "#04");
        }

        [Test]
        public void Test11030()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberDecimalSeparator = "#";
            Assert.AreEqual("-1#000000E+008", (-99999999.9).Fmt("E", nfi), "#01");
        }

        [Test]
        public void Test11031()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "+";
            nfi.PositiveSign = "-";

            Assert.AreEqual("1.000000E-000", 1.0.Fmt("E", nfi), "#01");
            Assert.AreEqual("0.000000E-000", 0.0.Fmt("E", nfi), "#02");
            Assert.AreEqual("+1.000000E-000", (-1.0).Fmt("E", nfi), "#03");
        }

        [Test]
        public void TestNaNToString()
        {
            // Net 4 workaround:
            var nfi = NumberFormatInfo.GetInstance(null);
            var InfinityPos = nfi.PositiveInfinitySymbol;
            var InfinityNeg = nfi.NegativeInfinitySymbol;

            Assert.AreEqual(InfinityPos, double.PositiveInfinity.Fmt(), "#01");
            Assert.AreEqual(InfinityNeg, double.NegativeInfinity.Fmt(), "#02");
            Assert.AreEqual("NaN", double.NaN.Fmt(), "#03");
            Assert.AreEqual(InfinityPos, float.PositiveInfinity.Fmt(), "#04");
            Assert.AreEqual(InfinityNeg, float.NegativeInfinity.Fmt(), "#05");
            Assert.AreEqual("NaN", float.NaN.Fmt(), "#06");

            Assert.AreEqual(InfinityPos, double.PositiveInfinity.Fmt("R"), "#07");
            Assert.AreEqual(InfinityNeg, double.NegativeInfinity.Fmt("R"), "#08");
            Assert.AreEqual("NaN", double.NaN.Fmt("R"), "#09");
            Assert.AreEqual(InfinityPos, float.PositiveInfinity.Fmt("R"), "#10");
            Assert.AreEqual(InfinityNeg, float.NegativeInfinity.Fmt("R"), "#11");
            Assert.AreEqual("NaN", float.NaN.Fmt("R"), "#12");
        }

        [Test]
        public void Test11032()
        {
            Assert.AreEqual("Infinity", (double.MaxValue/0.0).Fmt("E99", _nfi), "#01");
            Assert.AreEqual("-Infinity", (double.MinValue/0.0).Fmt("E99", _nfi), "#02");
            Assert.AreEqual("NaN", (0.0/0.0).Fmt("E99", _nfi), "#03");
        }

        // Test12000- Double and F
        [Test]
        public void Test12000()
        {
            Assert.AreEqual("0.00", 0.0.Fmt("F", _nfi), "#01");
            Assert.AreEqual("0.00", 0.0.Fmt("f", _nfi), "#02");
            Assert.AreEqual(
                "-179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.00",
                double.MinValue.Fmt("F", _nfi), "#03");
            Assert.AreEqual(
                "-179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.00",
                double.MinValue.Fmt("f", _nfi), "#04");
            Assert.AreEqual(
                "179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.00",
                double.MaxValue.Fmt("F", _nfi), "#05");
            Assert.AreEqual(
                "179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.00",
                double.MaxValue.Fmt("f", _nfi), "#06");
        }

        [Test]
        public void Test12001()
        {
            Assert.AreEqual("F ", 0.0.Fmt("F ", _nfi), "#01");
            Assert.AreEqual(" F", 0.0.Fmt(" F", _nfi), "#02");
            Assert.AreEqual(" F ", 0.0.Fmt(" F ", _nfi), "#03");
        }

        [Test]
        public void Test12002()
        {
            Assert.AreEqual("-F ", (-1.0).Fmt("F ", _nfi), "#01");
            Assert.AreEqual("- F", (-1.0).Fmt(" F", _nfi), "#02");
            Assert.AreEqual("- F ", (-1.0).Fmt(" F ", _nfi), "#03");
        }

        [Test]
        public void Test12003()
        {
            Assert.AreEqual("0", 0.0.Fmt("F0", _nfi), "#01");
            Assert.AreEqual("0.0000000000000000", 0.0.Fmt("F16", _nfi), "#02");
            Assert.AreEqual("0.00000000000000000", 0.0.Fmt("F17", _nfi), "#03");
            Assert.AreEqual(
                "0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                0.0.Fmt("F99", _nfi), "#04");
            Assert.AreEqual("F100", 0.0.Fmt("F100", _nfi), "#05");
        }

        [Test]
        public void Test12004()
        {
            Assert.AreEqual(
                "179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("F0", _nfi), "#01");
            Assert.AreEqual(
                "179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0000000000000000",
                double.MaxValue.Fmt("F16", _nfi), "#02");
            Assert.AreEqual(
                "179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.00000000000000000",
                double.MaxValue.Fmt("F17", _nfi), "#03");
            Assert.AreEqual(
                "179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("F99", _nfi), "#04");
            Assert.AreEqual(
                "F1179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("F100", _nfi), "#05");
        }

        [Test]
        public void Test12005()
        {
            Assert.AreEqual(
                "-179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("F0", _nfi), "#01");
            Assert.AreEqual(
                "-179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0000000000000000",
                double.MinValue.Fmt("F16", _nfi), "#02");
            Assert.AreEqual(
                "-179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.00000000000000000",
                double.MinValue.Fmt("F17", _nfi), "#03");
            Assert.AreEqual(
                "-179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("F99", _nfi), "#04");
            Assert.AreEqual(
                "-F1179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("F100", _nfi), "#05");
        }

        [Test]
        public void Test12006()
        {
            Assert.AreEqual("FF", 0.0.Fmt("FF", _nfi), "#01");
            Assert.AreEqual("F0F", 0.0.Fmt("F0F", _nfi), "#02");
            Assert.AreEqual("F0xF", 0.0.Fmt("F0xF", _nfi), "#03");
        }

        [Test]
        public void Test12007()
        {
            Assert.AreEqual("FF", double.MaxValue.Fmt("FF", _nfi), "#01");
            Assert.AreEqual(
                "F179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F",
                double.MaxValue.Fmt("F0F", _nfi), "#02");
            Assert.AreEqual(
                "F179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000xF",
                double.MaxValue.Fmt("F0xF", _nfi), "#03");
        }

        [Test]
        public void Test12008()
        {
            Assert.AreEqual("-FF", double.MinValue.Fmt("FF", _nfi), "#01");
            Assert.AreEqual(
                "-F179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F",
                double.MinValue.Fmt("F0F", _nfi), "#02");
            Assert.AreEqual(
                "-F179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000xF",
                double.MinValue.Fmt("F0xF", _nfi), "#03");
        }

        [Test]
        public void Test12009()
        {
            Assert.AreEqual("0.00000000000000000", 0.0.Fmt("F0000000000000000000000000000000000000017", _nfi), "#01");
            Assert.AreEqual(
                "179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.00000000000000000",
                double.MaxValue.Fmt("F0000000000000000000000000000000000000017", _nfi), "#02");
            Assert.AreEqual(
                "-179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.00000000000000000",
                double.MinValue.Fmt("F0000000000000000000000000000000000000017", _nfi), "#03");
        }

        [Test]
        public void Test12010()
        {
            Assert.AreEqual("+F", 0.0.Fmt("+F", _nfi), "#01");
            Assert.AreEqual("F+", 0.0.Fmt("F+", _nfi), "#02");
            Assert.AreEqual("+F+", 0.0.Fmt("+F+", _nfi), "#03");
        }

        [Test]
        public void Test12011()
        {
            Assert.AreEqual("+F", double.MaxValue.Fmt("+F", _nfi), "#01");
            Assert.AreEqual("F+", double.MaxValue.Fmt("F+", _nfi), "#02");
            Assert.AreEqual("+F+", double.MaxValue.Fmt("+F+", _nfi), "#03");
        }

        [Test]
        public void Test12012()
        {
            Assert.AreEqual("-+F", double.MinValue.Fmt("+F", _nfi), "#01");
            Assert.AreEqual("-F+", double.MinValue.Fmt("F+", _nfi), "#02");
            Assert.AreEqual("-+F+", double.MinValue.Fmt("+F+", _nfi), "#03");
        }

        [Test]
        public void Test12013()
        {
            Assert.AreEqual("-F", 0.0.Fmt("-F", _nfi), "#01");
            Assert.AreEqual("F-", 0.0.Fmt("F-", _nfi), "#02");
            Assert.AreEqual("-F-", 0.0.Fmt("-F-", _nfi), "#03");
        }

        [Test]
        public void Test12014()
        {
            Assert.AreEqual("-F", double.MaxValue.Fmt("-F", _nfi), "#01");
            Assert.AreEqual("F-", double.MaxValue.Fmt("F-", _nfi), "#02");
            Assert.AreEqual("-F-", double.MaxValue.Fmt("-F-", _nfi), "#03");
        }

        [Test]
        public void Test12015()
        {
            Assert.AreEqual("--F", double.MinValue.Fmt("-F", _nfi), "#01");
            Assert.AreEqual("-F-", double.MinValue.Fmt("F-", _nfi), "#02");
            Assert.AreEqual("--F-", double.MinValue.Fmt("-F-", _nfi), "#03");
        }

        [Test]
        public void Test12016()
        {
            Assert.AreEqual("F+0", 0.0.Fmt("F+0", _nfi), "#01");
            Assert.AreEqual(
                "F+179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("F+0", _nfi), "#02");
            Assert.AreEqual(
                "-F+179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("F+0", _nfi), "#03");
        }

        [Test]
        public void Test12017()
        {
            Assert.AreEqual("F+9", 0.0.Fmt("F+9", _nfi), "#01");
            Assert.AreEqual("F+9", double.MaxValue.Fmt("F+9", _nfi), "#02");
            Assert.AreEqual("-F+9", double.MinValue.Fmt("F+9", _nfi), "#03");
        }

        [Test]
        public void Test12018()
        {
            Assert.AreEqual("F-9", 0.0.Fmt("F-9", _nfi), "#01");
            Assert.AreEqual("F-9", double.MaxValue.Fmt("F-9", _nfi), "#02");
            Assert.AreEqual("-F-9", double.MinValue.Fmt("F-9", _nfi), "#03");
        }

        [Test]
        public void Test12019()
        {
            Assert.AreEqual("F0", 0.0.Fmt("F0,", _nfi), "#01");
            Assert.AreEqual(
                "F179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("F0,", _nfi), "#02");
            Assert.AreEqual(
                "-F179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("F0,", _nfi), "#03");
        }

        [Test]
        public void Test12020()
        {
            Assert.AreEqual("F0", 0.0.Fmt("F0.", _nfi), "#01");
            Assert.AreEqual(
                "F179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("F0.", _nfi), "#02");
            Assert.AreEqual(
                "-F179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("F0.", _nfi), "#03");
        }

        [Test]
        public void Test12021()
        {
            Assert.AreEqual("F0.0", 0.0.Fmt("F0.0", _nfi), "#01");
            Assert.AreEqual(
                "F179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0",
                double.MaxValue.Fmt("F0.0", _nfi), "#02");
            Assert.AreEqual(
                "-F179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0",
                double.MinValue.Fmt("F0.0", _nfi), "#03");
        }

        [Test]
        public void Test12022()
        {
            Assert.AreEqual("F09", 0.0.Fmt("F0.9", _nfi), "#01");
            Assert.AreEqual(
                "F1797693134862320000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009",
                double.MaxValue.Fmt("F0.9", _nfi), "#02");
            Assert.AreEqual(
                "-F1797693134862320000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009",
                double.MinValue.Fmt("F0.9", _nfi), "#03");
        }

        [Test]
        public void Test12023()
        {
            Assert.AreEqual("1.1", 1.05.Fmt("F1", _nfi), "#01");
            Assert.AreEqual("1.2", 1.15.Fmt("F1", _nfi), "#02");
            Assert.AreEqual("1.3", 1.25.Fmt("F1", _nfi), "#03");
            Assert.AreEqual("1.4", 1.35.Fmt("F1", _nfi), "#04");
            Assert.AreEqual("1.5", 1.45.Fmt("F1", _nfi), "#05");
            Assert.AreEqual("1.6", 1.55.Fmt("F1", _nfi), "#06");
            Assert.AreEqual("1.7", 1.65.Fmt("F1", _nfi), "#07");
            Assert.AreEqual("1.8", 1.75.Fmt("F1", _nfi), "#08");
            Assert.AreEqual("1.9", 1.85.Fmt("F1", _nfi), "#09");
            Assert.AreEqual("2.0", 1.95.Fmt("F1", _nfi), "#10");
        }

        [Test]
        public void Test12024()
        {
            Assert.AreEqual("1.01", 1.005.Fmt("F2", _nfi), "#01");
            Assert.AreEqual("1.02", 1.015.Fmt("F2", _nfi), "#02");
            Assert.AreEqual("1.03", 1.025.Fmt("F2", _nfi), "#03");
            Assert.AreEqual("1.04", 1.035.Fmt("F2", _nfi), "#04");
            Assert.AreEqual("1.05", 1.045.Fmt("F2", _nfi), "#05");
            Assert.AreEqual("1.06", 1.055.Fmt("F2", _nfi), "#06");
            Assert.AreEqual("1.07", 1.065.Fmt("F2", _nfi), "#07");
            Assert.AreEqual("1.08", 1.075.Fmt("F2", _nfi), "#08");
            Assert.AreEqual("1.09", 1.085.Fmt("F2", _nfi), "#09");
            Assert.AreEqual("1.10", 1.095.Fmt("F2", _nfi), "#10");
        }

        [Test]
        public void Test12025()
        {
            Assert.AreEqual("1.00000000000001", 1.000000000000005.Fmt("F14", _nfi), "#01");
            Assert.AreEqual("1.00000000000002", 1.000000000000015.Fmt("F14", _nfi), "#02");
            Assert.AreEqual("1.00000000000003", 1.000000000000025.Fmt("F14", _nfi), "#03");
            Assert.AreEqual("1.00000000000004", 1.000000000000035.Fmt("F14", _nfi), "#04");
            Assert.AreEqual("1.00000000000005", 1.000000000000045.Fmt("F14", _nfi), "#05");
            Assert.AreEqual("1.00000000000006", 1.000000000000055.Fmt("F14", _nfi), "#06");
            Assert.AreEqual("1.00000000000007", 1.000000000000065.Fmt("F14", _nfi), "#07");
            Assert.AreEqual("1.00000000000008", 1.000000000000075.Fmt("F14", _nfi), "#08");
            Assert.AreEqual("1.00000000000009", 1.000000000000085.Fmt("F14", _nfi), "#09");
            Assert.AreEqual("1.00000000000010", 1.000000000000095.Fmt("F14", _nfi), "#10");
        }

        [Test]
        public void Test12026()
        {
            Assert.AreEqual("1.000000000000000", 1.0000000000000005.Fmt("F15", _nfi), "#01");
            Assert.AreEqual("1.000000000000000", 1.0000000000000015.Fmt("F15", _nfi), "#02");
            Assert.AreEqual("1.000000000000000", 1.0000000000000025.Fmt("F15", _nfi), "#03");
            Assert.AreEqual("1.000000000000000", 1.0000000000000035.Fmt("F15", _nfi), "#04");
            Assert.AreEqual("1.000000000000000", 1.0000000000000045.Fmt("F15", _nfi), "#05");
            Assert.AreEqual("1.000000000000010", 1.0000000000000055.Fmt("F15", _nfi), "#06");
            Assert.AreEqual("1.000000000000010", 1.0000000000000065.Fmt("F15", _nfi), "#07");
            Assert.AreEqual("1.000000000000010", 1.0000000000000075.Fmt("F15", _nfi), "#08");
            Assert.AreEqual("1.000000000000010", 1.0000000000000085.Fmt("F15", _nfi), "#09");
            Assert.AreEqual("1.000000000000010", 1.0000000000000095.Fmt("F15", _nfi), "#10");
        }

        [Test]
        public void Test12027()
        {
            Assert.AreEqual("1.0000000000000000", 1.00000000000000005.Fmt("F16", _nfi), "#01");
            Assert.AreEqual("1.0000000000000000", 1.00000000000000015.Fmt("F16", _nfi), "#02");
            Assert.AreEqual("1.0000000000000000", 1.00000000000000025.Fmt("F16", _nfi), "#03");
            Assert.AreEqual("1.0000000000000000", 1.00000000000000035.Fmt("F16", _nfi), "#04");
            Assert.AreEqual("1.0000000000000000", 1.00000000000000045.Fmt("F16", _nfi), "#05");
            Assert.AreEqual("1.0000000000000000", 1.00000000000000055.Fmt("F16", _nfi), "#06");
            Assert.AreEqual("1.0000000000000000", 1.00000000000000065.Fmt("F16", _nfi), "#07");
            Assert.AreEqual("1.0000000000000000", 1.00000000000000075.Fmt("F16", _nfi), "#08");
            Assert.AreEqual("1.0000000000000000", 1.00000000000000085.Fmt("F16", _nfi), "#09");
            Assert.AreEqual("1.0000000000000000", 1.00000000000000095.Fmt("F16", _nfi), "#10");
        }

        [Test]
        public void Test12028()
        {
            Assert.AreEqual("1",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("F0", _nfi), "#01");
            Assert.AreEqual("1.234567890123",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("F12", _nfi), "#02");
            Assert.AreEqual("1.2345678901235",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("F13", _nfi), "#03");
            Assert.AreEqual("1.23456789012346",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("F14", _nfi), "#04");
            Assert.AreEqual("1.234567890123460",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("F15", _nfi), "#05");
            Assert.AreEqual(
                "1.234567890123460000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("F99", _nfi), "#06");
            Assert.AreEqual("F101",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("F100", _nfi), "#07");
        }

        [Test]
        public void Test12029()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberDecimalSeparator = "#";
            Assert.AreEqual("-99999999#90", (-99999999.9).Fmt("F", nfi), "#01");
        }

        [Test]
        public void Test12030()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "+";
            nfi.PositiveSign = "-";

            Assert.AreEqual("1.00", 1.0.Fmt("F", nfi), "#01");
            Assert.AreEqual("0.00", 0.0.Fmt("F", nfi), "#02");
            Assert.AreEqual("+1.00", (-1.0).Fmt("F", nfi), "#03");
        }

        [Test]
        public void Test12031()
        {
            Assert.AreEqual("Infinity", (double.MaxValue/0.0).Fmt("F99", _nfi), "#01");
            Assert.AreEqual("-Infinity", (double.MinValue/0.0).Fmt("F99", _nfi), "#02");
            Assert.AreEqual("NaN", (0.0/0.0).Fmt("F99", _nfi), "#03");
        }

        // Test13000- Double and G
        [Test]
        public void Test13000()
        {
            Assert.AreEqual("0", 0.0.Fmt("G", _nfi), "#01");
            Assert.AreEqual("0", (-0.0).Fmt("G", _nfi), "#01.1");
            Assert.AreEqual("0", 0.0.Fmt("g", _nfi), "#02");
            Assert.AreEqual("-1.79769313486232E+308", double.MinValue.Fmt("G", _nfi), "#03");
            Assert.AreEqual("-1.79769313486232e+308", double.MinValue.Fmt("g", _nfi), "#04");
            Assert.AreEqual("1.79769313486232E+308", double.MaxValue.Fmt("G", _nfi), "#05");
            Assert.AreEqual("1.79769313486232e+308", double.MaxValue.Fmt("g", _nfi), "#06");
        }

        [Test]
        public void Test13001()
        {
            Assert.AreEqual("G ", 0.0.Fmt("G ", _nfi), "#01");
            Assert.AreEqual(" G", 0.0.Fmt(" G", _nfi), "#02");
            Assert.AreEqual(" G ", 0.0.Fmt(" G ", _nfi), "#03");
        }

        [Test]
        public void Test13002()
        {
            Assert.AreEqual("-G ", (-1.0).Fmt("G ", _nfi), "#01");
            Assert.AreEqual("- G", (-1.0).Fmt(" G", _nfi), "#02");
            Assert.AreEqual("- G ", (-1.0).Fmt(" G ", _nfi), "#03");
        }

        [Test]
        public void Test13003()
        {
            Assert.AreEqual("0", 0.0.Fmt("G0", _nfi), "#01");
            Assert.AreEqual("0", 0.0.Fmt("G16", _nfi), "#02");
            Assert.AreEqual("0", 0.0.Fmt("G17", _nfi), "#03");
            Assert.AreEqual("0", 0.0.Fmt("G99", _nfi), "#04");
            Assert.AreEqual("G100", 0.0.Fmt("G100", _nfi), "#05");
        }

        [Test]
        public void Test13004()
        {
            Assert.AreEqual("1.79769313486232E+308", double.MaxValue.Fmt("G0", _nfi), "#01");
            Assert.AreEqual("1.797693134862316E+308", double.MaxValue.Fmt("G16", _nfi), "#02");
            Assert.AreEqual("1.7976931348623157E+308", double.MaxValue.Fmt("G17", _nfi), "#03");
            Assert.AreEqual("1.7976931348623157E+308", double.MaxValue.Fmt("G99", _nfi), "#04");
            Assert.AreEqual(
                "G1179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("G100", _nfi), "#05");
        }

        [Test]
        public void Test13005()
        {
            Assert.AreEqual("-1.79769313486232E+308", double.MinValue.Fmt("G0", _nfi), "#01");
            Assert.AreEqual("-1.797693134862316E+308", double.MinValue.Fmt("G16", _nfi), "#02");
            Assert.AreEqual("-1.7976931348623157E+308", double.MinValue.Fmt("G17", _nfi), "#03");
            Assert.AreEqual("-1.7976931348623157E+308", double.MinValue.Fmt("G99", _nfi), "#04");
            Assert.AreEqual(
                "-G1179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("G100", _nfi), "#05");
        }

        [Test]
        public void Test13006()
        {
            Assert.AreEqual("GF", 0.0.Fmt("GF", _nfi), "#01");
            Assert.AreEqual("G0F", 0.0.Fmt("G0F", _nfi), "#02");
            Assert.AreEqual("G0xF", 0.0.Fmt("G0xF", _nfi), "#03");
        }

        [Test]
        public void Test13007()
        {
            Assert.AreEqual("GF", double.MaxValue.Fmt("GF", _nfi), "#01");
            Assert.AreEqual(
                "G179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F",
                double.MaxValue.Fmt("G0F", _nfi), "#02");
            Assert.AreEqual(
                "G179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000xF",
                double.MaxValue.Fmt("G0xF", _nfi), "#03");
        }

        [Test]
        public void Test13008()
        {
            Assert.AreEqual("-GF", double.MinValue.Fmt("GF", _nfi), "#01");
            Assert.AreEqual(
                "-G179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F",
                double.MinValue.Fmt("G0F", _nfi), "#02");
            Assert.AreEqual(
                "-G179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000xF",
                double.MinValue.Fmt("G0xF", _nfi), "#03");
        }

        [Test]
        public void Test13009()
        {
            Assert.AreEqual("0", 0.0.Fmt("G0000000000000000000000000000000000000017", _nfi), "#01");
            Assert.AreEqual("1.7976931348623157E+308",
                double.MaxValue.Fmt("G0000000000000000000000000000000000000017", _nfi), "#02");
            Assert.AreEqual("-1.7976931348623157E+308",
                double.MinValue.Fmt("G0000000000000000000000000000000000000017", _nfi), "#03");
        }

        [Test]
        public void Test13010()
        {
            Assert.AreEqual("+G", 0.0.Fmt("+G", _nfi), "#01");
            Assert.AreEqual("G+", 0.0.Fmt("G+", _nfi), "#02");
            Assert.AreEqual("+G+", 0.0.Fmt("+G+", _nfi), "#03");
        }

        [Test]
        public void Test13011()
        {
            Assert.AreEqual("+G", double.MaxValue.Fmt("+G", _nfi), "#01");
            Assert.AreEqual("G+", double.MaxValue.Fmt("G+", _nfi), "#02");
            Assert.AreEqual("+G+", double.MaxValue.Fmt("+G+", _nfi), "#03");
        }

        [Test]
        public void Test13012()
        {
            Assert.AreEqual("-+G", double.MinValue.Fmt("+G", _nfi), "#01");
            Assert.AreEqual("-G+", double.MinValue.Fmt("G+", _nfi), "#02");
            Assert.AreEqual("-+G+", double.MinValue.Fmt("+G+", _nfi), "#03");
        }

        [Test]
        public void Test13013()
        {
            Assert.AreEqual("-G", 0.0.Fmt("-G", _nfi), "#01");
            Assert.AreEqual("G-", 0.0.Fmt("G-", _nfi), "#02");
            Assert.AreEqual("-G-", 0.0.Fmt("-G-", _nfi), "#03");
        }

        [Test]
        public void Test13014()
        {
            Assert.AreEqual("-G", double.MaxValue.Fmt("-G", _nfi), "#01");
            Assert.AreEqual("G-", double.MaxValue.Fmt("G-", _nfi), "#02");
            Assert.AreEqual("-G-", double.MaxValue.Fmt("-G-", _nfi), "#03");
        }

        [Test]
        public void Test13015()
        {
            Assert.AreEqual("--G", double.MinValue.Fmt("-G", _nfi), "#01");
            Assert.AreEqual("-G-", double.MinValue.Fmt("G-", _nfi), "#02");
            Assert.AreEqual("--G-", double.MinValue.Fmt("-G-", _nfi), "#03");
        }

        [Test]
        public void Test13016()
        {
            Assert.AreEqual("G+0", 0.0.Fmt("G+0", _nfi), "#01");
            Assert.AreEqual(
                "G+179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("G+0", _nfi), "#02");
            Assert.AreEqual(
                "-G+179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("G+0", _nfi), "#03");
        }

        [Test]
        public void Test13017()
        {
            Assert.AreEqual("G+9", 0.0.Fmt("G+9", _nfi), "#01");
            Assert.AreEqual("G+9", double.MaxValue.Fmt("G+9", _nfi), "#02");
            Assert.AreEqual("-G+9", double.MinValue.Fmt("G+9", _nfi), "#03");
        }

        [Test]
        public void Test13018()
        {
            Assert.AreEqual("G-9", 0.0.Fmt("G-9", _nfi), "#01");
            Assert.AreEqual("G-9", double.MaxValue.Fmt("G-9", _nfi), "#02");
            Assert.AreEqual("-G-9", double.MinValue.Fmt("G-9", _nfi), "#03");
        }

        [Test]
        public void Test13019()
        {
            Assert.AreEqual("G0", 0.0.Fmt("G0,", _nfi), "#01");
            Assert.AreEqual(
                "G179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("G0,", _nfi), "#02");
            Assert.AreEqual(
                "-G179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("G0,", _nfi), "#03");
        }

        [Test]
        public void Test13020()
        {
            Assert.AreEqual("G0", 0.0.Fmt("G0.", _nfi), "#01");
            Assert.AreEqual(
                "G179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("G0.", _nfi), "#02");
            Assert.AreEqual(
                "-G179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("G0.", _nfi), "#03");
        }

        [Test]
        public void Test13021()
        {
            Assert.AreEqual("G0.0", 0.0.Fmt("G0.0", _nfi), "#01");
            Assert.AreEqual(
                "G179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0",
                double.MaxValue.Fmt("G0.0", _nfi), "#02");
            Assert.AreEqual(
                "-G179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0",
                double.MinValue.Fmt("G0.0", _nfi), "#03");
        }

        [Test]
        public void Test13022()
        {
            Assert.AreEqual("G09", 0.0.Fmt("G0.9", _nfi), "#01");
            Assert.AreEqual(
                "G1797693134862320000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009",
                double.MaxValue.Fmt("G0.9", _nfi), "#02");
            Assert.AreEqual(
                "-G1797693134862320000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009",
                double.MinValue.Fmt("G0.9", _nfi), "#03");
        }

        [Test]
        public void Test13023()
        {
            Assert.AreEqual("0.5", 0.5.Fmt("G1", _nfi), "#01");
            Assert.AreEqual("2", 1.5.Fmt("G1", _nfi), "#02");
            Assert.AreEqual("3", 2.5.Fmt("G1", _nfi), "#03");
            Assert.AreEqual("4", 3.5.Fmt("G1", _nfi), "#04");
            Assert.AreEqual("5", 4.5.Fmt("G1", _nfi), "#05");
            Assert.AreEqual("6", 5.5.Fmt("G1", _nfi), "#06");
            Assert.AreEqual("7", 6.5.Fmt("G1", _nfi), "#07");
            Assert.AreEqual("8", 7.5.Fmt("G1", _nfi), "#08");
            Assert.AreEqual("9", 8.5.Fmt("G1", _nfi), "#09");
            Assert.AreEqual("1E+01", 9.5.Fmt("G1", _nfi), "#10");
        }

        [Test]
        public void Test13024_CarryPropagation()
        {
            var d = 1.15;
            Assert.AreEqual("1", d.Fmt("G1", _nfi), "#01");
            // NumberStore converts 1.15 into 1.14999...91 (1 in index 17)
            // so the call to NumberToString doesn't result in 1.2 but in 1.1
            // which seems "somewhat" normal considering the #17 results,
            Assert.AreEqual("1.2", d.Fmt("G2", _nfi), "#02");
            Assert.AreEqual("1.15", d.Fmt("G3", _nfi), "#03");
            Assert.AreEqual("1.15", d.Fmt("G4", _nfi), "#04");
            Assert.AreEqual("1.15", d.Fmt("G5", _nfi), "#05");
            Assert.AreEqual("1.15", d.Fmt("G6", _nfi), "#06");
            Assert.AreEqual("1.15", d.Fmt("G7", _nfi), "#07");
            Assert.AreEqual("1.15", d.Fmt("G8", _nfi), "#08");
            Assert.AreEqual("1.15", d.Fmt("G9", _nfi), "#09");
            Assert.AreEqual("1.15", d.Fmt("G10", _nfi), "#10");
            Assert.AreEqual("1.15", d.Fmt("G11", _nfi), "#11");
            Assert.AreEqual("1.15", d.Fmt("G12", _nfi), "#12");
            Assert.AreEqual("1.15", d.Fmt("G13", _nfi), "#13");
            Assert.AreEqual("1.15", d.Fmt("G14", _nfi), "#14");
            Assert.AreEqual("1.15", d.Fmt("G15", _nfi), "#15");
            Assert.AreEqual("1.15", d.Fmt("G16", _nfi), "#16");
            Assert.AreEqual("1.1499999999999999", d.Fmt("G17", _nfi), "#17");
        }

        [Test]
        public void Test13024()
        {
            Assert.AreEqual("1.1", 1.05.Fmt("G2", _nfi), "#01");
            Assert.AreEqual("1.2", 1.15.Fmt("G2", _nfi), "#02");
            Assert.AreEqual("1.3", 1.25.Fmt("G2", _nfi), "#03");
            Assert.AreEqual("1.4", 1.35.Fmt("G2", _nfi), "#04");
            Assert.AreEqual("1.5", 1.45.Fmt("G2", _nfi), "#05");
            Assert.AreEqual("1.6", 1.55.Fmt("G2", _nfi), "#06");
            Assert.AreEqual("1.7", 1.65.Fmt("G2", _nfi), "#07");
            Assert.AreEqual("1.8", 1.75.Fmt("G2", _nfi), "#08");
            Assert.AreEqual("1.9", 1.85.Fmt("G2", _nfi), "#09");
            Assert.AreEqual("2", 1.95.Fmt("G2", _nfi), "#10");
        }

        [Test]
        public void Test13025()
        {
            Assert.AreEqual("10", 10.05.Fmt("G2", _nfi), "#01");
            Assert.AreEqual("10", 10.15.Fmt("G2", _nfi), "#02");
            Assert.AreEqual("10", 10.25.Fmt("G2", _nfi), "#03");
            Assert.AreEqual("10", 10.35.Fmt("G2", _nfi), "#04");
            Assert.AreEqual("10", 10.45.Fmt("G2", _nfi), "#05");
            Assert.AreEqual("11", 10.55.Fmt("G2", _nfi), "#06");
            Assert.AreEqual("11", 10.65.Fmt("G2", _nfi), "#07");
            Assert.AreEqual("11", 10.75.Fmt("G2", _nfi), "#08");
            Assert.AreEqual("11", 10.85.Fmt("G2", _nfi), "#09");
            Assert.AreEqual("11", 10.95.Fmt("G2", _nfi), "#10");
        }

        [Test]
        public void Test13026()
        {
            Assert.AreEqual("1.00000000000001", 1.000000000000005.Fmt("G15", _nfi), "#01");
            Assert.AreEqual("1.00000000000002", 1.000000000000015.Fmt("G15", _nfi), "#02");
            Assert.AreEqual("1.00000000000003", 1.000000000000025.Fmt("G15", _nfi), "#03");
            Assert.AreEqual("1.00000000000004", 1.000000000000035.Fmt("G15", _nfi), "#04");
            Assert.AreEqual("1.00000000000005", 1.000000000000045.Fmt("G15", _nfi), "#05");
            Assert.AreEqual("1.00000000000006", 1.000000000000055.Fmt("G15", _nfi), "#06");
            Assert.AreEqual("1.00000000000007", 1.000000000000065.Fmt("G15", _nfi), "#07");
            Assert.AreEqual("1.00000000000008", 1.000000000000075.Fmt("G15", _nfi), "#08");
            Assert.AreEqual("1.00000000000009", 1.000000000000085.Fmt("G15", _nfi), "#09");
            Assert.AreEqual("1.0000000000001", 1.000000000000095.Fmt("G15", _nfi), "#10");
        }

        [Test]
        public void Test13027()
        {
            Assert.AreEqual("1", 1.0000000000000005.Fmt("G16", _nfi), "#01");
            Assert.AreEqual("1.000000000000002", 1.0000000000000015.Fmt("G16", _nfi), "#02");
            Assert.AreEqual("1.000000000000002", 1.0000000000000025.Fmt("G16", _nfi), "#03");
            Assert.AreEqual("1.000000000000004", 1.0000000000000035.Fmt("G16", _nfi), "#04");
            Assert.AreEqual("1.000000000000004", 1.0000000000000045.Fmt("G16", _nfi), "#05");
            Assert.AreEqual("1.000000000000006", 1.0000000000000055.Fmt("G16", _nfi), "#06");
            Assert.AreEqual("1.000000000000006", 1.0000000000000065.Fmt("G16", _nfi), "#07");
            Assert.AreEqual("1.000000000000008", 1.0000000000000075.Fmt("G16", _nfi), "#08");
            Assert.AreEqual("1.000000000000008", 1.0000000000000085.Fmt("G16", _nfi), "#09");
            Assert.AreEqual("1.00000000000001", 1.0000000000000095.Fmt("G16", _nfi), "#10");
        }

        [Test]
        public void Test13028()
        {
            Assert.AreEqual("1", 1.00000000000000005.Fmt("G17", _nfi), "#01");
            Assert.AreEqual("1.0000000000000002", 1.00000000000000015.Fmt("G17", _nfi), "#02");
            Assert.AreEqual("1.0000000000000002", 1.00000000000000025.Fmt("G17", _nfi), "#03");
            Assert.AreEqual("1.0000000000000004", 1.00000000000000035.Fmt("G17", _nfi), "#04");
            Assert.AreEqual("1.0000000000000004", 1.00000000000000045.Fmt("G17", _nfi), "#05");
            Assert.AreEqual("1.0000000000000004", 1.00000000000000055.Fmt("G17", _nfi), "#06");
            Assert.AreEqual("1.0000000000000007", 1.00000000000000065.Fmt("G17", _nfi), "#07");
            Assert.AreEqual("1.0000000000000007", 1.00000000000000075.Fmt("G17", _nfi), "#08");
            Assert.AreEqual("1.0000000000000009", 1.00000000000000085.Fmt("G17", _nfi), "#09");
            Assert.AreEqual("1.0000000000000009", 1.00000000000000095.Fmt("G17", _nfi), "#10");
        }

        [Test]
        public void Test13029()
        {
            Assert.AreEqual("1", 1.000000000000000005.Fmt("G18", _nfi), "#01");
            Assert.AreEqual("1", 1.000000000000000015.Fmt("G18", _nfi), "#02");
            Assert.AreEqual("1", 1.000000000000000025.Fmt("G18", _nfi), "#03");
            Assert.AreEqual("1", 1.000000000000000035.Fmt("G18", _nfi), "#04");
            Assert.AreEqual("1", 1.000000000000000045.Fmt("G18", _nfi), "#05");
            Assert.AreEqual("1", 1.000000000000000055.Fmt("G18", _nfi), "#06");
            Assert.AreEqual("1", 1.000000000000000065.Fmt("G18", _nfi), "#07");
            Assert.AreEqual("1", 1.000000000000000075.Fmt("G18", _nfi), "#08");
            Assert.AreEqual("1", 1.000000000000000085.Fmt("G18", _nfi), "#09");
            Assert.AreEqual("1", 1.000000000000000095.Fmt("G18", _nfi), "#10");
        }

        [Test]
        public void Test13030()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberDecimalSeparator = "#";
            Assert.AreEqual("-99999999#9", (-99999999.9).Fmt("G", nfi), "#01");
        }

        [Test]
        public void Test13031()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "+";
            nfi.PositiveSign = "-";

            Assert.AreEqual("1", 1.0.Fmt("G", nfi), "#01");
            Assert.AreEqual("0", 0.0.Fmt("G", nfi), "#02");
            Assert.AreEqual("+1", (-1.0).Fmt("G", nfi), "#03");
        }

        [Test]
        public void Test13032()
        {
            Assert.AreEqual("Infinity", (double.MaxValue/0.0).Fmt("G99", _nfi), "#01");
            Assert.AreEqual("-Infinity", (double.MinValue/0.0).Fmt("G99", _nfi), "#02");
            Assert.AreEqual("NaN", (0.0/0.0).Fmt("G99", _nfi), "#03");
        }

        [Test]
        public void Test13033()
        {
            Assert.AreEqual("0.0001", 0.0001.Fmt("G", _nfi), "#01");
            Assert.AreEqual("1E-05", 0.00001.Fmt("G", _nfi), "#02");
            Assert.AreEqual("0.0001", 0.0001.Fmt("G0", _nfi), "#03");
            Assert.AreEqual("1E-05", 0.00001.Fmt("G0", _nfi), "#04");
            Assert.AreEqual("100000000000000", 100000000000000.0.Fmt("G", _nfi), "#05");
            Assert.AreEqual("1E+15", 1000000000000000.0.Fmt("G", _nfi), "#06");
            Assert.AreEqual("1000000000000000", 1000000000000000.0.Fmt("G16", _nfi), "#07");
        }

        // Test14000- Double and N
        [Test]
        public void Test14000()
        {
            Assert.AreEqual("0.00", 0.0.Fmt("N", _nfi), "#01");
            Assert.AreEqual("0.00", 0.0.Fmt("n", _nfi), "#02");
            Assert.AreEqual(
                "-179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00",
                double.MinValue.Fmt("N", _nfi), "#03");
            Assert.AreEqual(
                "-179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00",
                double.MinValue.Fmt("n", _nfi), "#04");
            Assert.AreEqual(
                "179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00",
                double.MaxValue.Fmt("N", _nfi), "#05");
            Assert.AreEqual(
                "179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00",
                double.MaxValue.Fmt("n", _nfi), "#06");
        }

        [Test]
        public void Test14001()
        {
            Assert.AreEqual("N ", 0.0.Fmt("N ", _nfi), "#01");
            Assert.AreEqual(" N", 0.0.Fmt(" N", _nfi), "#02");
            Assert.AreEqual(" N ", 0.0.Fmt(" N ", _nfi), "#03");
        }

        [Test]
        public void Test14002()
        {
            Assert.AreEqual("-N ", (-1.0).Fmt("N ", _nfi), "#01");
            Assert.AreEqual("- N", (-1.0).Fmt(" N", _nfi), "#02");
            Assert.AreEqual("- N ", (-1.0).Fmt(" N ", _nfi), "#03");
        }

        [Test]
        public void Test14003()
        {
            Assert.AreEqual("0", 0.0.Fmt("N0", _nfi), "#01");
            Assert.AreEqual("0.0000000000000000", 0.0.Fmt("N16", _nfi), "#02");
            Assert.AreEqual("0.00000000000000000", 0.0.Fmt("N17", _nfi), "#03");
            Assert.AreEqual(
                "0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                0.0.Fmt("N99", _nfi), "#04");
            Assert.AreEqual("N100", 0.0.Fmt("N100", _nfi), "#05");
        }

        [Test]
        public void Test14004()
        {
            Assert.AreEqual(
                "179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000",
                double.MaxValue.Fmt("N0", _nfi), "#01");
            Assert.AreEqual(
                "179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.0000000000000000",
                double.MaxValue.Fmt("N16", _nfi), "#02");
            Assert.AreEqual(
                "179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00000000000000000",
                double.MaxValue.Fmt("N17", _nfi), "#03");
            Assert.AreEqual(
                "179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("N99", _nfi), "#04");
            Assert.AreEqual(
                "N1179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("N100", _nfi), "#05");
        }

        [Test]
        public void Test14005()
        {
            Assert.AreEqual(
                "-179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000",
                double.MinValue.Fmt("N0", _nfi), "#01");
            Assert.AreEqual(
                "-179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.0000000000000000",
                double.MinValue.Fmt("N16", _nfi), "#02");
            Assert.AreEqual(
                "-179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00000000000000000",
                double.MinValue.Fmt("N17", _nfi), "#03");
            Assert.AreEqual(
                "-179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("N99", _nfi), "#04");
            Assert.AreEqual(
                "-N1179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("N100", _nfi), "#05");
        }

        [Test]
        public void Test14006()
        {
            Assert.AreEqual("NF", 0.0.Fmt("NF", _nfi), "#01");
            Assert.AreEqual("N0F", 0.0.Fmt("N0F", _nfi), "#02");
            Assert.AreEqual("N0xF", 0.0.Fmt("N0xF", _nfi), "#03");
        }

        [Test]
        public void Test14007()
        {
            Assert.AreEqual("NF", double.MaxValue.Fmt("NF", _nfi), "#01");
            Assert.AreEqual(
                "N179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F",
                double.MaxValue.Fmt("N0F", _nfi), "#02");
            Assert.AreEqual(
                "N179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000xF",
                double.MaxValue.Fmt("N0xF", _nfi), "#03");
        }

        [Test]
        public void Test14008()
        {
            Assert.AreEqual("-NF", double.MinValue.Fmt("NF", _nfi), "#01");
            Assert.AreEqual(
                "-N179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F",
                double.MinValue.Fmt("N0F", _nfi), "#02");
            Assert.AreEqual(
                "-N179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000xF",
                double.MinValue.Fmt("N0xF", _nfi), "#03");
        }

        [Test]
        public void Test14009()
        {
            Assert.AreEqual("0.00000000000000000", 0.0.Fmt("N0000000000000000000000000000000000000017", _nfi), "#01");
            Assert.AreEqual(
                "179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00000000000000000",
                double.MaxValue.Fmt("N0000000000000000000000000000000000000017", _nfi), "#02");
            Assert.AreEqual(
                "-179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00000000000000000",
                double.MinValue.Fmt("N0000000000000000000000000000000000000017", _nfi), "#03");
        }

        [Test]
        public void Test14010()
        {
            Assert.AreEqual("+N", 0.0.Fmt("+N", _nfi), "#01");
            Assert.AreEqual("N+", 0.0.Fmt("N+", _nfi), "#02");
            Assert.AreEqual("+N+", 0.0.Fmt("+N+", _nfi), "#03");
        }

        [Test]
        public void Test14011()
        {
            Assert.AreEqual("+N", double.MaxValue.Fmt("+N", _nfi), "#01");
            Assert.AreEqual("N+", double.MaxValue.Fmt("N+", _nfi), "#02");
            Assert.AreEqual("+N+", double.MaxValue.Fmt("+N+", _nfi), "#03");
        }

        [Test]
        public void Test14012()
        {
            Assert.AreEqual("-+N", double.MinValue.Fmt("+N", _nfi), "#01");
            Assert.AreEqual("-N+", double.MinValue.Fmt("N+", _nfi), "#02");
            Assert.AreEqual("-+N+", double.MinValue.Fmt("+N+", _nfi), "#03");
        }

        [Test]
        public void Test14013()
        {
            Assert.AreEqual("-N", 0.0.Fmt("-N", _nfi), "#01");
            Assert.AreEqual("N-", 0.0.Fmt("N-", _nfi), "#02");
            Assert.AreEqual("-N-", 0.0.Fmt("-N-", _nfi), "#03");
        }

        [Test]
        public void Test14014()
        {
            Assert.AreEqual("-N", double.MaxValue.Fmt("-N", _nfi), "#01");
            Assert.AreEqual("N-", double.MaxValue.Fmt("N-", _nfi), "#02");
            Assert.AreEqual("-N-", double.MaxValue.Fmt("-N-", _nfi), "#03");
        }

        [Test]
        public void Test14015()
        {
            Assert.AreEqual("--N", double.MinValue.Fmt("-N", _nfi), "#01");
            Assert.AreEqual("-N-", double.MinValue.Fmt("N-", _nfi), "#02");
            Assert.AreEqual("--N-", double.MinValue.Fmt("-N-", _nfi), "#03");
        }

        [Test]
        public void Test14016()
        {
            Assert.AreEqual("N+0", 0.0.Fmt("N+0", _nfi), "#01");
            Assert.AreEqual(
                "N+179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("N+0", _nfi), "#02");
            Assert.AreEqual(
                "-N+179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("N+0", _nfi), "#03");
        }

        [Test]
        public void Test14017()
        {
            Assert.AreEqual("N+9", 0.0.Fmt("N+9", _nfi), "#01");
            Assert.AreEqual("N+9", double.MaxValue.Fmt("N+9", _nfi), "#02");
            Assert.AreEqual("-N+9", double.MinValue.Fmt("N+9", _nfi), "#03");
        }

        [Test]
        public void Test14018()
        {
            Assert.AreEqual("N-9", 0.0.Fmt("N-9", _nfi), "#01");
            Assert.AreEqual("N-9", double.MaxValue.Fmt("N-9", _nfi), "#02");
            Assert.AreEqual("-N-9", double.MinValue.Fmt("N-9", _nfi), "#03");
        }

        [Test]
        public void Test14019()
        {
            Assert.AreEqual("N0", 0.0.Fmt("N0,", _nfi), "#01");
            Assert.AreEqual(
                "N179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("N0,", _nfi), "#02");
            Assert.AreEqual(
                "-N179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("N0,", _nfi), "#03");
        }

        [Test]
        public void Test14020()
        {
            Assert.AreEqual("N0", 0.0.Fmt("N0.", _nfi), "#01");
            Assert.AreEqual(
                "N179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("N0.", _nfi), "#02");
            Assert.AreEqual(
                "-N179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("N0.", _nfi), "#03");
        }

        [Test]
        public void Test14021()
        {
            Assert.AreEqual("N0.0", 0.0.Fmt("N0.0", _nfi), "#01");
            Assert.AreEqual(
                "N179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0",
                double.MaxValue.Fmt("N0.0", _nfi), "#02");
            Assert.AreEqual(
                "-N179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0",
                double.MinValue.Fmt("N0.0", _nfi), "#03");
        }

        [Test]
        public void Test14022()
        {
            Assert.AreEqual("N09", 0.0.Fmt("N0.9", _nfi), "#01");
            Assert.AreEqual(
                "N1797693134862320000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009",
                double.MaxValue.Fmt("N0.9", _nfi), "#02");
            Assert.AreEqual(
                "-N1797693134862320000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009",
                double.MinValue.Fmt("N0.9", _nfi), "#03");
        }

        [Test]
        public void Test14023()
        {
            Assert.AreEqual("999.1", 999.05.Fmt("N1", _nfi), "#01");
            Assert.AreEqual("999.2", 999.15.Fmt("N1", _nfi), "#02");
            Assert.AreEqual("999.3", 999.25.Fmt("N1", _nfi), "#03");
            Assert.AreEqual("999.4", 999.35.Fmt("N1", _nfi), "#04");
            Assert.AreEqual("999.5", 999.45.Fmt("N1", _nfi), "#05");
            Assert.AreEqual("999.6", 999.55.Fmt("N1", _nfi), "#06");
            Assert.AreEqual("999.7", 999.65.Fmt("N1", _nfi), "#07");
            Assert.AreEqual("999.8", 999.75.Fmt("N1", _nfi), "#08");
            Assert.AreEqual("999.9", 999.85.Fmt("N1", _nfi), "#09");
            Assert.AreEqual("1,000.0", 999.95.Fmt("N1", _nfi), "#10");
        }

        [Test]
        public void Test14024()
        {
            Assert.AreEqual("999.91", 999.905.Fmt("N2", _nfi), "#01");
            Assert.AreEqual("999.92", 999.915.Fmt("N2", _nfi), "#02");
            Assert.AreEqual("999.93", 999.925.Fmt("N2", _nfi), "#03");
            Assert.AreEqual("999.94", 999.935.Fmt("N2", _nfi), "#04");
            Assert.AreEqual("999.95", 999.945.Fmt("N2", _nfi), "#05");
            Assert.AreEqual("999.96", 999.955.Fmt("N2", _nfi), "#06");
            Assert.AreEqual("999.97", 999.965.Fmt("N2", _nfi), "#07");
            Assert.AreEqual("999.98", 999.975.Fmt("N2", _nfi), "#08");
            Assert.AreEqual("999.99", 999.985.Fmt("N2", _nfi), "#09");
            Assert.AreEqual("1,000.00", 999.995.Fmt("N2", _nfi), "#10");
        }

        [Test]
        public void Test14025()
        {
            Assert.AreEqual("999.99999999991", 999.999999999905.Fmt("N11", _nfi), "#01");
            Assert.AreEqual("999.99999999992", 999.999999999915.Fmt("N11", _nfi), "#02");
            Assert.AreEqual("999.99999999993", 999.999999999925.Fmt("N11", _nfi), "#03");
            Assert.AreEqual("999.99999999994", 999.999999999935.Fmt("N11", _nfi), "#04");
            Assert.AreEqual("999.99999999995", 999.999999999945.Fmt("N11", _nfi), "#05");
            Assert.AreEqual("999.99999999996", 999.999999999955.Fmt("N11", _nfi), "#06");
            Assert.AreEqual("999.99999999997", 999.999999999965.Fmt("N11", _nfi), "#07");
            Assert.AreEqual("999.99999999998", 999.999999999975.Fmt("N11", _nfi), "#08");
            Assert.AreEqual("999.99999999999", 999.999999999985.Fmt("N11", _nfi), "#09");
            Assert.AreEqual("1,000.00000000000", 999.999999999995.Fmt("N11", _nfi), "#10");
        }

        [Test]
        public void Test14026()
        {
            Assert.AreEqual("999.999999999990", 999.9999999999905.Fmt("N12", _nfi), "#01");
            Assert.AreEqual("999.999999999991", 999.9999999999915.Fmt("N12", _nfi), "#02");
            Assert.AreEqual("999.999999999993", 999.9999999999925.Fmt("N12", _nfi), "#03");
            Assert.AreEqual("999.999999999994", 999.9999999999935.Fmt("N12", _nfi), "#04");
            Assert.AreEqual("999.999999999995", 999.9999999999945.Fmt("N12", _nfi), "#05");
            Assert.AreEqual("999.999999999995", 999.9999999999955.Fmt("N12", _nfi), "#06");
            Assert.AreEqual("999.999999999996", 999.9999999999965.Fmt("N12", _nfi), "#07");
            Assert.AreEqual("999.999999999998", 999.9999999999975.Fmt("N12", _nfi), "#08");
            Assert.AreEqual("999.999999999999", 999.9999999999985.Fmt("N12", _nfi), "#09");
            Assert.AreEqual("1,000.000000000000", 999.9999999999995.Fmt("N12", _nfi), "#10");
        }

        [Test]
        public void Test14027()
        {
            Assert.AreEqual("999.9999999999990", 999.99999999999905.Fmt("N13", _nfi), "#01");
            Assert.AreEqual("999.9999999999990", 999.99999999999915.Fmt("N13", _nfi), "#02");
            Assert.AreEqual("999.9999999999990", 999.99999999999925.Fmt("N13", _nfi), "#03");
            Assert.AreEqual("999.9999999999990", 999.99999999999935.Fmt("N13", _nfi), "#04");
            Assert.AreEqual("999.9999999999990", 999.99999999999945.Fmt("N13", _nfi), "#05");
            Assert.AreEqual("1,000.0000000000000", 999.99999999999955.Fmt("N13", _nfi), "#06");
            Assert.AreEqual("1,000.0000000000000", 999.99999999999965.Fmt("N13", _nfi), "#07");
            Assert.AreEqual("1,000.0000000000000", 999.99999999999975.Fmt("N13", _nfi), "#08");
            Assert.AreEqual("1,000.0000000000000", 999.99999999999985.Fmt("N13", _nfi), "#09");
            Assert.AreEqual("1,000.0000000000000", 999.99999999999995.Fmt("N13", _nfi), "#10");
        }

        [Test]
        public void Test14028()
        {
            Assert.AreEqual("1",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N0", _nfi), "#01");
            Assert.AreEqual("1.234567890123",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N12", _nfi), "#02");
            Assert.AreEqual("1.2345678901235",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N13", _nfi), "#03");
            Assert.AreEqual("1.23456789012346",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N14", _nfi), "#04");
            Assert.AreEqual("1.234567890123460",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N15", _nfi), "#05");
            Assert.AreEqual(
                "1.234567890123460000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N99", _nfi), "#06");
            Assert.AreEqual("N101",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N100", _nfi), "#07");
        }

        [Test]
        public void Test14029()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NumberDecimalSeparator = "#";
            Assert.AreEqual("-99,999,999#90", (-99999999.9).Fmt("N", nfi), "#01");
        }

        [Test]
        public void Test14030()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "+";
            nfi.PositiveSign = "-";

            Assert.AreEqual("1,000.00", 1000.0.Fmt("N", nfi), "#01");
            Assert.AreEqual("0.00", 0.0.Fmt("N", nfi), "#02");
            Assert.AreEqual("+1,000.00", (-1000.0).Fmt("N", nfi), "#03");
        }

        [Test]
        public void Test14031()
        {
            Assert.AreEqual("Infinity", (double.MaxValue/0.0).Fmt("N99", _nfi), "#01");
            Assert.AreEqual("-Infinity", (double.MinValue/0.0).Fmt("N99", _nfi), "#02");
            Assert.AreEqual("NaN", (0.0/0.0).Fmt("N99", _nfi), "#03");
        }

        // Test15000- Double and P
        [Test]
        public void Test15000()
        {
            Assert.AreEqual("0.00 %", 0.0.Fmt("P", _nfi), "#01");
            Assert.AreEqual("0.00 %", 0.0.Fmt("p", _nfi), "#02");
            Assert.AreEqual(
                "-17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00 %",
                double.MinValue.Fmt("P", _nfi), "#03");
            Assert.AreEqual(
                "-17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00 %",
                double.MinValue.Fmt("p", _nfi), "#04");
            Assert.AreEqual(
                "17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00 %",
                double.MaxValue.Fmt("P", _nfi), "#05");
            Assert.AreEqual(
                "17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00 %",
                double.MaxValue.Fmt("p", _nfi), "#06");
        }

        [Test]
        public void Test15001()
        {
            Assert.AreEqual("P ", 0.0.Fmt("P ", _nfi), "#01");
            Assert.AreEqual(" P", 0.0.Fmt(" P", _nfi), "#02");
            Assert.AreEqual(" P ", 0.0.Fmt(" P ", _nfi), "#03");
        }

        [Test]
        public void Test15002()
        {
            Assert.AreEqual("-P ", (-1.0).Fmt("P ", _nfi), "#01");
            Assert.AreEqual("- P", (-1.0).Fmt(" P", _nfi), "#02");
            Assert.AreEqual("- P ", (-1.0).Fmt(" P ", _nfi), "#03");
        }

        [Test]
        public void Test15003()
        {
            Assert.AreEqual("0 %", 0.0.Fmt("P0", _nfi), "#01");
            Assert.AreEqual("0.0000000000000000 %", 0.0.Fmt("P16", _nfi), "#02");
            Assert.AreEqual("0.00000000000000000 %", 0.0.Fmt("P17", _nfi), "#03");
            Assert.AreEqual(
                "0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 %",
                0.0.Fmt("P99", _nfi), "#04");
            Assert.AreEqual("P100", 0.0.Fmt("P100", _nfi), "#05");
        }

        [Test]
        public void Test15004()
        {
            Assert.AreEqual(
                "17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000 %",
                double.MaxValue.Fmt("P0", _nfi), "#01");
            Assert.AreEqual(
                "17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.0000000000000000 %",
                double.MaxValue.Fmt("P16", _nfi), "#02");
            Assert.AreEqual(
                "17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00000000000000000 %",
                double.MaxValue.Fmt("P17", _nfi), "#03");
            Assert.AreEqual(
                "17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 %",
                double.MaxValue.Fmt("P99", _nfi), "#04");
            Assert.AreEqual(
                "P1179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("P100", _nfi), "#05");
        }

        [Test]
        public void Test15005()
        {
            Assert.AreEqual(
                "-17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000 %",
                double.MinValue.Fmt("P0", _nfi), "#01");
            Assert.AreEqual(
                "-17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.0000000000000000 %",
                double.MinValue.Fmt("P16", _nfi), "#02");
            Assert.AreEqual(
                "-17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00000000000000000 %",
                double.MinValue.Fmt("P17", _nfi), "#03");
            Assert.AreEqual(
                "-17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 %",
                double.MinValue.Fmt("P99", _nfi), "#04");
            Assert.AreEqual(
                "P1179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("P100", _nfi), "#05");
        }

        [Test]
        public void Test15006()
        {
            Assert.AreEqual("PF", 0.0.Fmt("PF", _nfi), "#01");
            Assert.AreEqual("P0F", 0.0.Fmt("P0F", _nfi), "#02");
            Assert.AreEqual("P0xF", 0.0.Fmt("P0xF", _nfi), "#03");
        }

        [Test]
        public void Test15007()
        {
            Assert.AreEqual("PF", double.MaxValue.Fmt("PF", _nfi), "#01");
            Assert.AreEqual(
                "P179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F",
                double.MaxValue.Fmt("P0F", _nfi), "#02");
            Assert.AreEqual(
                "P179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000xF",
                double.MaxValue.Fmt("P0xF", _nfi), "#03");
        }

        [Test]
        public void Test15008()
        {
            Assert.AreEqual("-PF", double.MinValue.Fmt("PF", _nfi), "#01");
            Assert.AreEqual(
                "-P179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F",
                double.MinValue.Fmt("P0F", _nfi), "#02");
            Assert.AreEqual(
                "-P179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000xF",
                double.MinValue.Fmt("P0xF", _nfi), "#03");
        }

        [Test]
        public void Test15009()
        {
            Assert.AreEqual("0.00000000000000000 %", 0.0.Fmt("P0000000000000000000000000000000000000017", _nfi), "#01");
            Assert.AreEqual(
                "17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00000000000000000 %",
                double.MaxValue.Fmt("P0000000000000000000000000000000000000017", _nfi), "#02");
            Assert.AreEqual(
                "-17,976,931,348,623,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000.00000000000000000 %",
                double.MinValue.Fmt("P0000000000000000000000000000000000000017", _nfi), "#03");
        }

        [Test]
        public void Test15010()
        {
            Assert.AreEqual("+P", 0.0.Fmt("+P", _nfi), "#01");
            Assert.AreEqual("P+", 0.0.Fmt("P+", _nfi), "#02");
            Assert.AreEqual("+P+", 0.0.Fmt("+P+", _nfi), "#03");
        }

        [Test]
        public void Test15011()
        {
            Assert.AreEqual("+P", double.MaxValue.Fmt("+P", _nfi), "#01");
            Assert.AreEqual("P+", double.MaxValue.Fmt("P+", _nfi), "#02");
            Assert.AreEqual("+P+", double.MaxValue.Fmt("+P+", _nfi), "#03");
        }

        [Test]
        public void Test15012()
        {
            Assert.AreEqual("-+P", double.MinValue.Fmt("+P", _nfi), "#01");
            Assert.AreEqual("-P+", double.MinValue.Fmt("P+", _nfi), "#02");
            Assert.AreEqual("-+P+", double.MinValue.Fmt("+P+", _nfi), "#03");
        }

        [Test]
        public void Test15013()
        {
            Assert.AreEqual("-P", 0.0.Fmt("-P", _nfi), "#01");
            Assert.AreEqual("P-", 0.0.Fmt("P-", _nfi), "#02");
            Assert.AreEqual("-P-", 0.0.Fmt("-P-", _nfi), "#03");
        }

        [Test]
        public void Test15014()
        {
            Assert.AreEqual("-P", double.MaxValue.Fmt("-P", _nfi), "#01");
            Assert.AreEqual("P-", double.MaxValue.Fmt("P-", _nfi), "#02");
            Assert.AreEqual("-P-", double.MaxValue.Fmt("-P-", _nfi), "#03");
        }

        [Test]
        public void Test15015()
        {
            Assert.AreEqual("--P", double.MinValue.Fmt("-P", _nfi), "#01");
            Assert.AreEqual("-P-", double.MinValue.Fmt("P-", _nfi), "#02");
            Assert.AreEqual("--P-", double.MinValue.Fmt("-P-", _nfi), "#03");
        }

        [Test]
        public void Test15016()
        {
            Assert.AreEqual("P+0", 0.0.Fmt("P+0", _nfi), "#01");
            Assert.AreEqual(
                "P+179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("P+0", _nfi), "#02");
            Assert.AreEqual(
                "-P+179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("P+0", _nfi), "#03");
        }

        [Test]
        public void Test15017()
        {
            Assert.AreEqual("P+9", 0.0.Fmt("P+9", _nfi), "#01");
            Assert.AreEqual("P+9", double.MaxValue.Fmt("P+9", _nfi), "#02");
            Assert.AreEqual("-P+9", double.MinValue.Fmt("P+9", _nfi), "#03");
        }

        [Test]
        public void Test15018()
        {
            Assert.AreEqual("P-9", 0.0.Fmt("P-9", _nfi), "#01");
            Assert.AreEqual("P-9", double.MaxValue.Fmt("P-9", _nfi), "#02");
            Assert.AreEqual("-P-9", double.MinValue.Fmt("P-9", _nfi), "#03");
        }

        [Test]
        public void Test15019()
        {
            Assert.AreEqual("P0", 0.0.Fmt("P0,", _nfi), "#01");
            Assert.AreEqual(
                "P179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("P0,", _nfi), "#02");
            Assert.AreEqual(
                "-P179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("P0,", _nfi), "#03");
        }

        [Test]
        public void Test15020()
        {
            Assert.AreEqual("P0", 0.0.Fmt("P0.", _nfi), "#01");
            Assert.AreEqual(
                "P179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MaxValue.Fmt("P0.", _nfi), "#02");
            Assert.AreEqual(
                "-P179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                double.MinValue.Fmt("P0.", _nfi), "#03");
        }

        [Test]
        public void Test15021()
        {
            Assert.AreEqual("P0.0", 0.0.Fmt("P0.0", _nfi), "#01");
            Assert.AreEqual(
                "P179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0",
                double.MaxValue.Fmt("P0.0", _nfi), "#02");
            Assert.AreEqual(
                "-P179769313486232000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0",
                double.MinValue.Fmt("P0.0", _nfi), "#03");
        }

        [Test]
        public void Test15022()
        {
            Assert.AreEqual("P09", 0.0.Fmt("P0.9", _nfi), "#01");
            Assert.AreEqual(
                "P1797693134862320000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009",
                double.MaxValue.Fmt("P0.9", _nfi), "#02");
            Assert.AreEqual(
                "-P1797693134862320000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009",
                double.MinValue.Fmt("P0.9", _nfi), "#03");
        }

        [Test]
        public void Test15023()
        {
            Assert.AreEqual("999.1 %", 9.9905.Fmt("P1", _nfi), "#01");
            Assert.AreEqual("999.2 %", 9.9915.Fmt("P1", _nfi), "#02");
            Assert.AreEqual("999.3 %", 9.9925.Fmt("P1", _nfi), "#03");
            Assert.AreEqual("999.4 %", 9.9935.Fmt("P1", _nfi), "#04");
            Assert.AreEqual("999.5 %", 9.9945.Fmt("P1", _nfi), "#05");
            Assert.AreEqual("999.6 %", 9.9955.Fmt("P1", _nfi), "#06");
            Assert.AreEqual("999.7 %", 9.9965.Fmt("P1", _nfi), "#07");
            Assert.AreEqual("999.8 %", 9.9975.Fmt("P1", _nfi), "#08");
            Assert.AreEqual("999.9 %", 9.9985.Fmt("P1", _nfi), "#09");
            Assert.AreEqual("1,000.0 %", 9.9995.Fmt("P1", _nfi), "#10");
        }

        [Test]
        public void Test15024()
        {
            Assert.AreEqual("999.91 %", 9.99905.Fmt("P2", _nfi), "#01");
            Assert.AreEqual("999.92 %", 9.99915.Fmt("P2", _nfi), "#02");
            Assert.AreEqual("999.93 %", 9.99925.Fmt("P2", _nfi), "#03");
            Assert.AreEqual("999.94 %", 9.99935.Fmt("P2", _nfi), "#04");
            Assert.AreEqual("999.95 %", 9.99945.Fmt("P2", _nfi), "#05");
            Assert.AreEqual("999.96 %", 9.99955.Fmt("P2", _nfi), "#06");
            Assert.AreEqual("999.97 %", 9.99965.Fmt("P2", _nfi), "#07");
            Assert.AreEqual("999.98 %", 9.99975.Fmt("P2", _nfi), "#08");
            Assert.AreEqual("999.99 %", 9.99985.Fmt("P2", _nfi), "#09");
            Assert.AreEqual("1,000.00 %", 9.99995.Fmt("P2", _nfi), "#10");
        }

        [Test]
        public void Test15025()
        {
            Assert.AreEqual("999.99999999991 %", 9.99999999999905.Fmt("P11", _nfi), "#01");
            Assert.AreEqual("999.99999999992 %", 9.99999999999915.Fmt("P11", _nfi), "#02");
            Assert.AreEqual("999.99999999993 %", 9.99999999999925.Fmt("P11", _nfi), "#03");
            Assert.AreEqual("999.99999999994 %", 9.99999999999935.Fmt("P11", _nfi), "#04");
            Assert.AreEqual("999.99999999995 %", 9.99999999999945.Fmt("P11", _nfi), "#05");
            Assert.AreEqual("999.99999999996 %", 9.99999999999955.Fmt("P11", _nfi), "#06");
            Assert.AreEqual("999.99999999997 %", 9.99999999999965.Fmt("P11", _nfi), "#07");
            Assert.AreEqual("999.99999999998 %", 9.99999999999975.Fmt("P11", _nfi), "#08");
            Assert.AreEqual("999.99999999999 %", 9.99999999999985.Fmt("P11", _nfi), "#09");
            Assert.AreEqual("1,000.00000000000 %", 9.99999999999995.Fmt("P11", _nfi), "#10");
        }

        [Test]
        public void Test15026()
        {
            Assert.AreEqual("999.999999999991 %", 9.999999999999905.Fmt("P12", _nfi), "#01");
            Assert.AreEqual("999.999999999991 %", 9.999999999999915.Fmt("P12", _nfi), "#02");
            Assert.AreEqual("999.999999999993 %", 9.999999999999925.Fmt("P12", _nfi), "#03");
            Assert.AreEqual("999.999999999993 %", 9.999999999999935.Fmt("P12", _nfi), "#04");
            Assert.AreEqual("999.999999999994 %", 9.999999999999945.Fmt("P12", _nfi), "#05");
            Assert.AreEqual("999.999999999996 %", 9.999999999999955.Fmt("P12", _nfi), "#06");
            Assert.AreEqual("999.999999999996 %", 9.999999999999965.Fmt("P12", _nfi), "#07");
            Assert.AreEqual("999.999999999998 %", 9.999999999999975.Fmt("P12", _nfi), "#08");
            Assert.AreEqual("999.999999999999 %", 9.999999999999985.Fmt("P12", _nfi), "#09");
            Assert.AreEqual("999.999999999999 %", 9.999999999999995.Fmt("P12", _nfi), "#10");
        }

        [Test]
        public void Test15027()
        {
            Assert.AreEqual("999.9999999999990 %", 9.9999999999999905.Fmt("P13", _nfi), "#01");
            Assert.AreEqual("999.9999999999990 %", 9.9999999999999915.Fmt("P13", _nfi), "#02");
            Assert.AreEqual("999.9999999999990 %", 9.9999999999999925.Fmt("P13", _nfi), "#03");
            Assert.AreEqual("999.9999999999990 %", 9.9999999999999935.Fmt("P13", _nfi), "#04");
            Assert.AreEqual("999.9999999999990 %", 9.9999999999999945.Fmt("P13", _nfi), "#05");
            Assert.AreEqual("999.9999999999990 %", 9.9999999999999955.Fmt("P13", _nfi), "#06");
            Assert.AreEqual("1,000.0000000000000 %", 9.9999999999999965.Fmt("P13", _nfi), "#07");
            Assert.AreEqual("1,000.0000000000000 %", 9.9999999999999975.Fmt("P13", _nfi), "#08");
            Assert.AreEqual("1,000.0000000000000 %", 9.9999999999999985.Fmt("P13", _nfi), "#09");
            Assert.AreEqual("1,000.0000000000000 %", 9.9999999999999995.Fmt("P13", _nfi), "#10");
        }

        [Test]
        public void Test15028()
        {
            Assert.AreEqual("1",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N0", _nfi), "#01");
            Assert.AreEqual("1.234567890123",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N12", _nfi), "#02");
            Assert.AreEqual("1.2345678901235",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N13", _nfi), "#03");
            Assert.AreEqual("1.23456789012346",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N14", _nfi), "#04");
            Assert.AreEqual("1.234567890123460",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N15", _nfi), "#05");
            Assert.AreEqual(
                "1.234567890123460000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N99", _nfi), "#06");
            Assert.AreEqual("N101",
                1.234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                    .Fmt("N100"), "#07");
        }

        [Test]
        public void Test15029()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.PercentDecimalSeparator = "#";
            Assert.AreEqual("-9,999,999,990#00 %", (-99999999.9).Fmt("P", nfi), "#01");
        }

        [Test]
        public void Test15030()
        {
            var nfi = _nfi.Clone() as NumberFormatInfo;
            nfi.NegativeSign = "+";
            nfi.PositiveSign = "-";

            Assert.AreEqual("1,000.00 %", 10.0.Fmt("P", nfi), "#01");
            Assert.AreEqual("0.00 %", 0.0.Fmt("P", nfi), "#02");
            Assert.AreEqual("+1,000.00 %", (-10.0).Fmt("P", nfi), "#03");
        }

        [Test]
        public void Test15031()
        {
            Assert.AreEqual("Infinity", (double.MaxValue/0.0).Fmt("N99", _nfi), "#01");
            Assert.AreEqual("-Infinity", (double.MinValue/0.0).Fmt("N99", _nfi), "#02");
            Assert.AreEqual("NaN", (0.0/0.0).Fmt("N99", _nfi), "#03");
        }

        // TestRoundtrip for double and single
        [Test]
        public void TestRoundtrip()
        {
            Assert.AreEqual("1.2345678901234567", 1.2345678901234567890.Fmt("R", _nfi), "#01");
            Assert.AreEqual("1.2345678901234567", 1.2345678901234567890.Fmt("r", _nfi), "#02");
            Assert.AreEqual("1.2345678901234567", 1.2345678901234567890.Fmt("R0", _nfi), "#03");
            Assert.AreEqual("1.2345678901234567", 1.2345678901234567890.Fmt("r0", _nfi), "#04");
            Assert.AreEqual("1.2345678901234567", 1.2345678901234567890.Fmt("R99", _nfi), "#05");
            Assert.AreEqual("1.2345678901234567", 1.2345678901234567890.Fmt("r99", _nfi), "#06");
            Assert.AreEqual("-1.7976931348623157E+308", double.MinValue.Fmt("R"), "#07");
            Assert.AreEqual("1.7976931348623157E+308", double.MaxValue.Fmt("R"), "#08");
            Assert.AreEqual("-1.7976931348623147E+308", (-1.7976931348623147E+308).Fmt("R"), "#09");
            Assert.AreEqual("-3.40282347E+38", float.MinValue.Fmt("R"), "#10");
            Assert.AreEqual("3.40282347E+38", float.MaxValue.Fmt("R"), "#11");
        }

        // Tests arithmetic overflow in double.Fmt exposed by Bug #383531
        [Test]
        public void TestToStringOverflow()
        {
            // Test all the possible double exponents with the maximal mantissa
            var dblPattern = 0xfffffffffffff; // all 1s significand

            for (long exp = 0; exp < 4096; exp++)
            {
                var val = BitConverter.Int64BitsToDouble((long) (dblPattern | (exp << 52)));
                var strRes = val.Fmt("R", NumberFormatInfo.InvariantInfo);
                var rndTripVal = double.Parse(strRes);
                Assert.AreEqual(val, rndTripVal, "Iter#" + exp);
            }
        }

        // Test17000 - Double and X
        [Test]
        [ExpectedException(typeof (FormatException))]
        public void Test17000()
        {
            Assert.AreEqual("", 0.0.Fmt("X99", _nfi), "#01");
        }
    }
}
