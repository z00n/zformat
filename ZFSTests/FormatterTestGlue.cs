#region Usings

using System;
using System.Globalization;

#endregion

namespace ZFormat
{
    public static class FormatterTestGlue
    {
        private static readonly ZString _zs = new ZString(1024);

        private static string ResetWithFormatter(ZNumberFormatter nf)
        {
            return _zs.Reset(nf.Chars, nf.Count).ToString();
        }

        private static ZNumberFormatInfo Convert(IFormatProvider provider)
        {
            if (provider == null)
            {
                return null;
            }
            var znfi = provider as ZNumberFormatInfo;
            if (znfi != null)
            {
                return znfi;
            }
            var nfi = provider as NumberFormatInfo;
            if (nfi != null)
            {
                return new ZNumberFormatInfo(nfi);
            }
            var culture = provider as CultureInfo;
            if (culture != null)
            {
                return new ZNumberFormatInfo(culture.NumberFormat);
            }
            throw new ArgumentException("provider");
        }

        #region Int32

        public static string Fmt(this int d, string format, IFormatProvider provider = null)
        {
            ZNumberFormatter.Instance.NumberToChars(format, d, Convert(provider));
            return ResetWithFormatter(ZNumberFormatter.Instance);
        }

        public static string Fmt(this int d, IFormatProvider provider = null)
        {
            ZNumberFormatter.Instance.NumberToChars(d, Convert(provider));
            return ResetWithFormatter(ZNumberFormatter.Instance);
        }

        #endregion

        #region Uint32

        public static string Fmt(this uint d, string format, IFormatProvider provider = null)
        {
            ZNumberFormatter.Instance.NumberToChars(format, d, Convert(provider));
            return ResetWithFormatter(ZNumberFormatter.Instance);
        }

        public static string Fmt(this uint d, IFormatProvider provider = null)
        {
            ZNumberFormatter.Instance.NumberToChars(d, Convert(provider));
            return ResetWithFormatter(ZNumberFormatter.Instance);
        }

        #endregion

        #region Double

        public static string Fmt(this double d, string format, IFormatProvider provider = null)
        {
            ZNumberFormatter.Instance.NumberToChars(format, d, Convert(provider));
            return ResetWithFormatter(ZNumberFormatter.Instance);
        }

        public static string Fmt(this double d, IFormatProvider provider = null)
        {
            ZNumberFormatter.Instance.NumberToChars(d, Convert(provider));
            return ResetWithFormatter(ZNumberFormatter.Instance);
        }

        #endregion

        #region Float

        public static string Fmt(this float d, string format, IFormatProvider provider = null)
        {
            ZNumberFormatter.Instance.NumberToChars(format, d, Convert(provider));
            return ResetWithFormatter(ZNumberFormatter.Instance);
        }

        public static string Fmt(this float d, IFormatProvider provider = null)
        {
            ZNumberFormatter.Instance.NumberToChars(d, Convert(provider));
            return ResetWithFormatter(ZNumberFormatter.Instance);
        }

        #endregion

        #region Int64

        public static string Fmt(this long d, string format, IFormatProvider provider = null)
        {
            ZNumberFormatter.Instance.NumberToChars(format, d, Convert(provider));
            return ResetWithFormatter(ZNumberFormatter.Instance);
        }

        public static string Fmt(this long d, IFormatProvider provider = null)
        {
            ZNumberFormatter.Instance.NumberToChars(d, Convert(provider));
            return ResetWithFormatter(ZNumberFormatter.Instance);
        }

        #endregion

        #region Uint64

        public static string Fmt(this ulong d, string format, IFormatProvider provider = null)
        {
            ZNumberFormatter.Instance.NumberToChars(format, d, Convert(provider));
            return ResetWithFormatter(ZNumberFormatter.Instance);
        }

        public static string Fmt(this ulong d, IFormatProvider provider = null)
        {
            ZNumberFormatter.Instance.NumberToChars(d, Convert(provider));
            return ResetWithFormatter(ZNumberFormatter.Instance);
        }

        #endregion

        #region Decimal

        public static string Fmt(this decimal d, string format, IFormatProvider provider = null)
        {
            ZNumberFormatter.Instance.NumberToChars(format, d, Convert(provider));
            return ResetWithFormatter(ZNumberFormatter.Instance);
        }

        #endregion
    }
}
